# M++ Player

M++ Player is designed as a lightweight application that provides users with a cool and convenient means for listening to their music, or in short - a music player. M++ Player supports a variety of audio formats - mp3, wma, wav, ogg, mp4, m4a and flac and provides library and playlist management.

M++ Player is based off the previous audio player called [M-Player], which was released for Windows-based systems. M++ Player was built primarily for GNU/Linux community using the Qt framework (C++). This project will only build and run on GNU/Linux based systems. 

![Image of M++ Player 0.4.7](https://gitlab.com/mjsware/m-player/raw/master/images/M++PlayerLibrary.png)

#### Current version

Version Number: 0.4.7

Dated : 14-06-2020 

#### Features in the current version

* Playlist and library track search
* Playlist management (including drag and drop functionality)
* Library management and generation
* Theming for both player modes
* Supports a variety of audio formats - mp3, wma, wav, ogg, mp4, m4a and flac
* Album art
* Application customisation - setting a theme (dark/no theme), library location, audio driver, start-up playlist and hiding of the application
* Viewing and editing of track properties
* Viewable web content for each track (lyrics, youtube videos and artist information)
* Core application - volume control, player, repeat and shuffle options (playlist and library tracks)
* Can be run on the Raspberry Pi 3+

#### Licence

This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.

>Copyright Matthew James 
 This file is part of "M++ Player".  
 "M++ Player" is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free Software Foundation,
 either version 3 of the License, or (at your option) any later version.
 "M++ Player" is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with "M++ Player".
 If not, see http://www.gnu.org/licenses/.

#### Contribute

To contribute, simply folk, clone or patch and send a pull request. The project has been built using Qt Creator.

Use Qt Creator with Qt Framework (>= 5.11) to build the project. 
 
#### Support Libraries
 
 - [libav](https://www.libav.org)
 - [taglibc](http://taglib.github.io)
 - [libao](https://www.xiph.org/ao) 
