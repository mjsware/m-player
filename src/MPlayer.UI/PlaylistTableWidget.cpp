// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "PlaylistTableWidget.hpp"

PlaylistTableWidget::PlaylistTableWidget(QWidget *parent) : QTableWidget(parent)
{
    this->playlistTrackReader = std::shared_ptr<IPlaylistTrackReader>(new PlaylistTrackReader());
    this->playerStatusReader = std::shared_ptr<IPlayerStatusReader>(new PlayerStatusReader(this->libraryTrackReader, this->playlistTrackReader));
    this->trackGridReader = std::shared_ptr<ITrackGridReader>(new TrackGridReader());
    this->trackGridWriter = std::shared_ptr<ITrackGridWriter>(new TrackGridWriter());
    this->playlistTrackWriter = std::shared_ptr<IPlaylistTrackWriter>(new PlaylistTrackWriter());
    this->applicationSettingsProvider = std::shared_ptr<IApplicationSettingsProvider>(new ApplicationSettingsProvider());
    this->addTrackAction = false;
    this->defaultAlbumDirectory = QDir::homePath();

    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(ShowPlaylistContextMenu(const QPoint&)));

    SetPlaylistAttributes();
    SetPlaylistHeaderItems();
}

void PlaylistTableWidget::SetPlaylistAttributes()
{
    this->setRowCount(0);
    this->setColumnCount(10);
    this->setAcceptDrops(true);
    this->viewport()->setAcceptDrops(true);
    this->setEditTriggers(QAbstractItemView::NoEditTriggers); // Don't allow editing of the playlist track container
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    this->setSelectionMode(QAbstractItemView::ExtendedSelection);
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->setDragDropOverwriteMode(false);
    this->setDragEnabled(true);
    this->setDragDropMode(DragDropMode::DragDrop);
    this->setDefaultDropAction(Qt::CopyAction);
    this->verticalHeader()->setVisible(false);
}

void PlaylistTableWidget::SetPlaylistHeaderItems()
{
    this->setHorizontalHeaderItem(TRACK_NUMBER, new QTableWidgetItem("N"));
    this->setHorizontalHeaderItem(TRACK_PLAY_STATUS, new QTableWidgetItem("P"));
    this->setHorizontalHeaderItem(TRACK_TITLE, new QTableWidgetItem("Title"));
    this->setHorizontalHeaderItem(TRACK_ARTIST, new QTableWidgetItem("Artist"));
    this->setHorizontalHeaderItem(TRACK_ALBUM, new QTableWidgetItem("Album"));
    this->setHorizontalHeaderItem(TRACK_GENRE, new QTableWidgetItem("Genre"));
    this->setHorizontalHeaderItem(TRACK_DURATION, new QTableWidgetItem("Duration"));
    this->setHorizontalHeaderItem(TRACK_YEAR, new QTableWidgetItem("Year"));
    this->setHorizontalHeaderItem(TRACK_BITRATE, new QTableWidgetItem("Bitrate"));
    this->setHorizontalHeaderItem(TRACK_FILEPATH, new QTableWidgetItem("File Path"));
}

void PlaylistTableWidget::SetPlaylistControls(std::shared_ptr<QLabel> statusLabel, QGroupBox *ctrlPlaylistTracksGroup)
{
    this->statusLabel = statusLabel;
    this->ctrlPlaylistTracksGroup = ctrlPlaylistTracksGroup;
}

void PlaylistTableWidget::SetPlaylistDependencies(std::shared_ptr<IPlayerService> playerService,
                                                  std::shared_ptr<IWindowManager> windowManager,
                                                  std::shared_ptr<ILibraryTrackReader> libraryTrackReader,
                                                  std::shared_ptr<ILibraryTrackWriter> libraryTrackWriter)
{
    this->playerService = playerService;
    this->windowManager = windowManager;
    this->libraryTrackReader = libraryTrackReader;
    this->libraryTrackWriter = libraryTrackWriter;
}

void PlaylistTableWidget::CalculatePlaylistTrackStatistics()
{
    QStringList currentTrackFilepaths = this->trackGridReader->GetTrackFilepathsAsList(this);
    this->statusLabel->setText(this->playerStatusReader->GetTabStatusStringForPlayerTab(currentTrackFilepaths, PLAYLIST));
}

void PlaylistTableWidget::ClearPlaylist()
{
    while (this->rowCount() > 0)
    {
        this->removeRow(0);
    }

    this->ctrlPlaylistTracksGroup->setTitle("  Playlist (" + QString::number(this->rowCount()) + ")");
    CalculatePlaylistTrackStatistics();
}

void PlaylistTableWidget::AddPlaylistTracks(const QStringList &selectedPlaylistFilepaths)
{
    this->addTrackAction = true;
    QVector<PlayerTrack> playlistTracks = this->playlistTrackReader->GetPlaylistTracks(selectedPlaylistFilepaths);

    for(int i = 0; i < playlistTracks.length(); i++)
    {
        this->trackGridWriter->AddTrackToTableGrid(i, false, playlistTracks[i], this);

        if(!this->addTrackAction)
        {
            break;
        }

        this->ctrlPlaylistTracksGroup->setTitle("  Playlist (" + QString::number(this->rowCount()) + ")");
        QApplication::processEvents();
    }

    this->resizeColumnsToContents();
    this->addTrackAction = false;
}

void PlaylistTableWidget::OpenPlaylist(const bool &appendToPlaylist)
{
    std::unique_ptr<QFileDialog> openPlaylistFileDialog = std::unique_ptr<QFileDialog>(new QFileDialog(this));
    QString playlistFilepath = openPlaylistFileDialog->getOpenFileName(this, tr("Open Playlists"), this->applicationSettingsProvider->GetPlaylistsDirectory(), tr("Playlist Files (*.xml)"));
    QStringList foundPlaylistFilepaths = this->playlistTrackReader->GetFilelistOfPlaylistDirectory(playlistFilepath);

    if(foundPlaylistFilepaths.count() > 0)
    {
        if(!appendToPlaylist) // If the append option is not selected
        {
            ClearPlaylist();
        }

        AddPlaylistTracks(foundPlaylistFilepaths);
        CalculatePlaylistTrackStatistics();
    }
}

void PlaylistTableWidget::SavePlaylist()
{
    if(this->rowCount() > 0)
    {
        std::unique_ptr<QFileDialog> savePlaylistFileDialog = std::unique_ptr<QFileDialog>(new QFileDialog(this));
        QString playlistFilePath = savePlaylistFileDialog->getSaveFileName(this, "Save Playlist", this->applicationSettingsProvider->GetPlaylistsDirectory(), "*.xml");

        if(!playlistFilePath.contains(".xml"))
        {
            playlistFilePath += ".xml";
        }

        QStringList playlistFiles = this->trackGridReader->GetTrackFilepathsAsList(this);
        this->playlistTrackWriter->SavePlaylist(playlistFilePath, playlistFiles);
    }
}

void PlaylistTableWidget::AutoLoadPlaylistTracks()
{
    if(this->applicationSettingsProvider->GetAutoLoadPlaylist() && !this->applicationSettingsProvider->GetPlaylistPath().isEmpty())
    {
        QStringList trackFilepaths = this->playlistTrackReader->GetFilelistOfPlaylistDirectory(this->applicationSettingsProvider->GetPlaylistPath());

        if(trackFilepaths.count() > 0)
        {
            AddPlaylistTracks(trackFilepaths);
        }
        else
        {
            QMessageBox::critical(this, "Open Playlist", "Couldn't read " + this->applicationSettingsProvider->GetPlaylistPath());
        }
    }
}

void PlaylistTableWidget::AddSelectedPlaylistTracksByAlbum()
{
    std::unique_ptr<QFileDialog> addPlaylistTracksDialog = std::unique_ptr<QFileDialog>(new QFileDialog(this));
    addPlaylistTracksDialog->setFileMode(QFileDialog::Directory);
    addPlaylistTracksDialog->setOption(QFileDialog::ShowDirsOnly);
    addPlaylistTracksDialog->setDirectory(this->defaultAlbumDirectory);

    if(addPlaylistTracksDialog->exec())
    {
        QStringList selectedAlbums = addPlaylistTracksDialog->selectedFiles();
        QStringList allPlaylistTrackFilepaths = this->playlistTrackReader->GetAvailablePlaylistTracksInDirectory(selectedAlbums.at(0));
        this->defaultAlbumDirectory = selectedAlbums.at(0);

        if(allPlaylistTrackFilepaths.count() > 0)
        {
            AddPlaylistTracks(allPlaylistTrackFilepaths);
        }
    }

    CalculatePlaylistTrackStatistics();
}

void PlaylistTableWidget::AddSelectedPlaylistTracks()
{
    std::unique_ptr<QFileDialog> addPlaylistTracksDialog = std::unique_ptr<QFileDialog>(new QFileDialog(this));
    addPlaylistTracksDialog->setDirectory(this->defaultAlbumDirectory);

    QStringList selectedPlaylistTrackFilepaths = addPlaylistTracksDialog->getOpenFileNames(this, tr("Open Music Tracks"), this->defaultAlbumDirectory, tr("Audio Files (*.mp3 *.wma *.wav *.ogg *.mp4 *.m4a *.flac)"));

    if(selectedPlaylistTrackFilepaths.count() > 0)
    {
        this->defaultAlbumDirectory = QFileInfo(selectedPlaylistTrackFilepaths.at(0)).absolutePath();
        AddPlaylistTracks(selectedPlaylistTrackFilepaths);
    }

    CalculatePlaylistTrackStatistics();
}

void PlaylistTableWidget::SelectPlaylistPlayingTrack()
{
    if(!this->addTrackAction)
    {
        for(int i = 0; i < this->rowCount(); i++)
        {
            if(this->item(i, TRACK_PLAY_STATUS)->text() == "*")
            {
                this->setFocus();
                this->selectRow(i);
            }
        }
    }
}

void PlaylistTableWidget::ShowPlaylistContextMenu(const QPoint &pos) // Slot for playlist context menu
{
    int rowId = this->currentRow();

    if(rowId == -1)
    {
        return;
    }

    QPoint globalPos = this->mapToGlobal(pos);
    QMenu playlistMenu;

    if(this->rowCount() == 0)
    {
        playlistMenu.setEnabled(false);
    }
    else if(this->currentRow() < 0)
    {
        this->selectRow(0);
    }

    QString selectedPlaylistTrackFilepath = this->item(rowId, TRACK_FILEPATH)->text();

    playlistMenu.addAction("Play Track");

    if(this->playerService->IsPlayerServiceRunning())
    {
        playlistMenu.addAction("Select Playing", this, SLOT(SelectPlaylistPlayingTrack()));
    }

    playlistMenu.addSeparator();
    playlistMenu.addAction("Remove Track(s)");
    playlistMenu.addAction("Clear Playlist", this, SLOT(ClearPlaylist()));
    playlistMenu.addAction("Add Track(s) to Library");
    playlistMenu.addSeparator();
    playlistMenu.addAction("View Track Properties");

    QAction* selectedMenuAction = playlistMenu.exec(globalPos);

    if (selectedMenuAction) // Menu selected
    {
        if(selectedMenuAction->text() == "Play Track")
        {
            this->playerService->OpenTrack(selectedPlaylistTrackFilepath);
            SessionSettings::LastPlaylistTrackPathRequested = selectedPlaylistTrackFilepath;
        }
        else if(selectedMenuAction->text() == "Remove Track(s)")
        {
            while(this->selectionModel()->selectedRows().count() > 0)
            {
                this->removeRow(this->selectedItems().at(0)->row());
            }

            CalculatePlaylistTrackStatistics();

            this->ctrlPlaylistTracksGroup->setTitle("  Playlist (" + QString::number(this->rowCount()) + ")");
            this->resizeColumnsToContents();
        }
        else if (selectedMenuAction->text() == "Add Track(s) to Library")
        {
            QStringList playlistTracksToAdd;
            QStringList selectedPlaylistTrackList = this->trackGridReader->GetSelectedTrackFilepathsAsList(this);

            for(int i = 0; i < selectedPlaylistTrackList.count(); i++)
            {
                if(!this->libraryTrackReader->IsLibraryTrackByFilepathCached(selectedPlaylistTrackList[i]))
                {
                    playlistTracksToAdd.append(selectedPlaylistTrackFilepath);
                }
            }

            QVector<PlayerTrack> playlistTracks = this->playlistTrackReader->GetPlaylistTracks(playlistTracksToAdd);
            this->libraryTrackWriter->AddSelectedTracksToLibrary(playlistTracks);
        }
        else if(selectedMenuAction->text() == "View Track Properties")
        {
            ViewPropertiesOfSelectedPlaylistTracks();
        }
    }
}

void PlaylistTableWidget::ViewPropertiesOfSelectedPlaylistTracks()
{
    QStringList selectedPlaylistTrackList = this->trackGridReader->GetSelectedTrackFilepathsAsList(this);

    this->windowManager->ShowProgramWindow(new FrmTrackProperties(selectedPlaylistTrackList, this->playerService));
}

void PlaylistTableWidget::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

void PlaylistTableWidget::dragMoveEvent(QDragMoveEvent *event)
{
    event->acceptProposedAction();
}

void PlaylistTableWidget::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasUrls())
    {
        QStringList droppedFilePaths;
        QString droppedFilePath = "";

        for(int i = 0; i < event->mimeData()->urls().count(); i++)
        {
            QUrl url(event->mimeData()->urls().at(i).url());
            droppedFilePath = url.toLocalFile();

            if(this->playlistTrackReader->ValidateDroppedPlaylistFile(droppedFilePath))
            {
                droppedFilePaths.append(droppedFilePath);
            }
        }

        SessionSettings::HasDraggedPlaylistTracks = true;
        SessionSettings::DraggedPlaylistFilepaths = droppedFilePaths;
    }
}
