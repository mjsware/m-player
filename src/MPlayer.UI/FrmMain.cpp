// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "FrmMain.hpp"

FrmMain::FrmMain(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::FrmMain)
{
    ui->setupUi(this);

    SetupFormDependencies();
    LoadWindowSettings();
    SetupFormControlSlots();
    SetupFormControls();
    ListArtists();
    StartTimersForLoad();
}

void FrmMain::SetupFormDependencies()
{
    this->statusLabel = std::shared_ptr<QLabel>(new QLabel(""));
    this->windowManager = std::shared_ptr<IWindowManager>(new WindowManager());
    this->trackGridReader = std::shared_ptr<ITrackGridReader>(new TrackGridReader());
    this->trackGridWriter = std::shared_ptr<ITrackGridWriter>(new TrackGridWriter());
    this->trackShuffler = std::shared_ptr<ITrackShuffler>(new TrackShuffler());
    this->playlistTrackReader = std::shared_ptr<IPlaylistTrackReader>(new PlaylistTrackReader());
    this->applicationSettingsProvider = std::shared_ptr<IApplicationSettingsProvider>(new ApplicationSettingsProvider());
    this->albumArtReader = std::shared_ptr<IAlbumArtReader>(new AlbumArtReader());
    this->libraryGenerator = std::shared_ptr<ILibraryGenerator>(new LibraryGenerator());
    this->libraryTrackReader = std::shared_ptr<ILibraryTrackReader>(new LibraryTrackReader());
    this->libraryTrackWriter = std::shared_ptr<ILibraryTrackWriter>(new LibraryTrackWriter(this->libraryTrackReader));
    this->playerStatusReader = std::shared_ptr<IPlayerStatusReader>(new PlayerStatusReader(this->libraryTrackReader, this->playlistTrackReader));
    this->playerService = std::shared_ptr<IPlayerService>(new PlayerService(this->applicationSettingsProvider->GetDefaultVolume(), this->applicationSettingsProvider->GetDefaultAudioDriverName()));
    this->trayIcon = std::shared_ptr<QSystemTrayIcon>(new QSystemTrayIcon(QIcon(this->applicationSettingsProvider->GetProgramIconFilePath()), this));

    ui->ctrlPlaylistTracks->SetPlaylistDependencies(this->playerService, this->windowManager, this->libraryTrackReader, this->libraryTrackWriter);
    ui->ctrlPlaylistTracks->SetPlaylistControls(this->statusLabel, ui->ctrlPlaylistTracksGroup);
}

void FrmMain::SetupFormControlSlots()
{
    // Status controls
    connect(ui->ctrlLibraryTracks, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(ShowLibraryContextMenu(const QPoint&)));
    connect(ui->ctrlVolume, SIGNAL(valueChanged(int)), this, SLOT(UpdateVolumeValue(int))); // Connect up volume slider to public slot
    connect(ui->ctrlPosition, SIGNAL(sliderReleased()), this, SLOT(UpdateDurationValue())); // Connect up position slider to public slot

    // Library QListWidgets and QTableWidget
    connect(ui->lstArtists, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(OnLibraryArtistItemChange()));
    connect(ui->lstArtists, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(OnLibraryArtistItemDoubleClick()));
    connect(ui->lstArtists, SIGNAL(currentRowChanged(int)), this, SLOT(OnLibraryArtistItemChange()));
    connect(ui->lstAlbums, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(OnLibraryAlbumItemChange()));
    connect(ui->lstAlbums, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(OnLibraryAlbumItemDoubleClick()));
    connect(ui->lstAlbums, SIGNAL(currentRowChanged(int)), this, SLOT(OnLibraryAlbumItemChange()));
    connect(ui->lstAlbums, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ShowAlbumContextMenu(QPoint)));
    connect(ui->ctrlLibraryTracks, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(DoubleClickList(QTableWidgetItem*)));
    connect(ui->ctrlPlaylistTracks, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(DoubleClickList(QTableWidgetItem*)));

    // Button events
    connect(ui->btnMoveUp, SIGNAL(clicked()), this, SLOT(MovePlaylistTrackUp()));
    connect(ui->btnMoveDown, SIGNAL(clicked()), this, SLOT(MovePlaylistTrackDown()));
    connect(ui->btnAddTracks, SIGNAL(clicked()), this, SLOT(AddSelectedPlaylistTracks()));
    connect(ui->btnAddAlbums, SIGNAL(clicked()), this, SLOT(AddSelectedPlaylistTracksByAlbum()));
    connect(ui->btnPlayPause, SIGNAL(clicked()), this, SLOT(PlayPauseTrack()));
    connect(ui->btnStop, SIGNAL(clicked()), this, SLOT(BtnStop_Click()));
    connect(ui->btnNext, SIGNAL(clicked()), this, SLOT(BtnNext_Click()));
    connect(ui->btnPrevious, SIGNAL(clicked()), this, SLOT(BtnPrevious_Click()));
    connect(ui->btnClearPlaylist, SIGNAL(clicked()), this, SLOT(BtnClearPlaylist_Click()));
    connect(ui->btnPlaylistSelectPlaying, SIGNAL(clicked()), this, SLOT(BtnPlaylistSelectPlaying_Click()));
    connect(ui->btnLibrarySelectPlaying, SIGNAL(clicked()), this, SLOT(BtnLibrarySelectPlaying()));
    connect(ui->btnOpenPlaylist, SIGNAL(clicked()), this, SLOT(BtnOpenPlaylist_Click()));
    connect(ui->btnSavePlaylist, SIGNAL(clicked()), this, SLOT(BtnSavePlaylist_Click()));
    connect(ui->btnPlaylistSearch, SIGNAL(clicked()), this, SLOT(BtnPlaylistSearch_Click()));
    connect(ui->btnLibrarySearch, SIGNAL(clicked()), this, SLOT(BtnLibrarySearch_Click()));

    // Menu events
    connect(ui->itemAddPlaylistTracks, SIGNAL(triggered()), this, SLOT(AddSelectedPlaylistTracks()));
    connect(ui->itemAddPlaylistAlbumTracks, SIGNAL(triggered()), this, SLOT(AddSelectedPlaylistTracksByAlbum()));
    connect(ui->itemExitApplication, SIGNAL(triggered()), this, SLOT(CloseApplication())); // Menu click
    connect(ui->itemPlayPause, SIGNAL(triggered()), this, SLOT(PlayPauseTrack()));
    connect(ui->itemNextTrack, SIGNAL(triggered()), this, SLOT(BtnNext_Click()));
    connect(ui->itemPreviousTrack, SIGNAL(triggered()), this, SLOT(BtnPrevious_Click()));
    connect(ui->itemStop, SIGNAL(triggered()), this, SLOT(StopTrack()));
    connect(ui->itemRepeat, SIGNAL(triggered()), this, SLOT(ItemRepeat_Click()));
    connect(ui->itemShowStatus, SIGNAL(triggered()), this, SLOT(ShowStatusInterface()));
    connect(ui->itemShowLibraryPlaylist, SIGNAL(triggered()), this, SLOT(ShowLibraryPlaylistInterface()));
    connect(ui->itemShowAllControls, SIGNAL(triggered()), this, SLOT(ResetPlayerInterface()));
    connect(ui->itemHide, SIGNAL(triggered()), this, SLOT(ItemShowHide_Click()));
    connect(ui->itemShuffle, SIGNAL(triggered()), this, SLOT(ItemShuffle_Click()));
    connect(ui->itemShuffleAllLibraryTracks, SIGNAL(triggered()), this, SLOT(ItemShuffleAllLibraryTracks_Click()));
    connect(ui->itemWebsite, SIGNAL(triggered()), this, SLOT(ItemViewWebsite_Click()));
    connect(ui->itemAboutMPlayer, SIGNAL(triggered()), this, SLOT(ItemAboutMPlayer_Click()));
    connect(ui->itemViewTrackProperties, SIGNAL(triggered()), this, SLOT(ItemViewTrackProperties_Click()));
    connect(ui->itemChangeSettings, SIGNAL(triggered()), this, SLOT(ItemChangeSettings_Click()));
    connect(ui->itemGenerateLibrary, SIGNAL(triggered()), this, SLOT(ItemGenerateLibrary_Click()));

    // Timers
    connect(&durationTimer, SIGNAL(timeout()), this, SLOT(DurationChecker()));
    connect(&searchChecker, SIGNAL(timeout()), this, SLOT(PlaySelectedSearchTrack()));
    connect(&fileChangeChecker, SIGNAL(timeout()), this, SLOT(ShowUpdatedTracks()));
    connect(&settingsChecker, SIGNAL(timeout()), this, SLOT(SetUpdatedApplicationSettings()));
    connect(&libraryGenerationChecker, SIGNAL(timeout()), this, SLOT(UpdateLibraryDisplay()));
    connect(&playlistChecker, SIGNAL(timeout()), this, SLOT(ShowPlayingTrackFromPlaylist()));

    // Tab change
    connect(ui->ctrlTab, SIGNAL(currentChanged(int)), this, SLOT(TabChange()));

    // Tray Icon
    connect(this->trayIcon.get(), SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
}

void FrmMain::SetupFormControls()
{
    std::unique_ptr<IThemeProvider> themeProvider = std::unique_ptr<IThemeProvider>(new ThemeProvider());
    themeProvider->ApplyThemingToForm(this, "FrmMain");

    ui->lstAlbums->setContextMenuPolicy(Qt::CustomContextMenu);

    this->statusLabel->setAlignment(Qt::AlignLeft);
    ui->ctrlStatus->addWidget(this->statusLabel.get(), 1);

    ui->ctrlAlbumArt->installEventFilter(this);

    ShowApplicationTrayIcon();
    ShowAlbumArt();

    QMainWindow::setWindowIcon(QIcon(this->applicationSettingsProvider->GetProgramIconFilePath()));

    ui->ctrlLibraryTracks->setRowCount(0);
    ui->ctrlLibraryTracks->setEditTriggers(QAbstractItemView::NoEditTriggers); // Don't allow editing of the library track container

    ui->ctrlVolume->setValue(static_cast<int>(this->applicationSettingsProvider->GetDefaultVolume()));

    ui->ctrlPlaylistTracks->AutoLoadPlaylistTracks();
}

void FrmMain::ShowApplicationTrayIcon()
{
    this->trayIcon->setToolTip("M++ Player (Double click to hide)");
    this->trayIcon->setVisible(true);
    this->trayIcon->show();
}

void FrmMain::StartTimersForLoad()
{
    this->fileChangeChecker.start(200);
    this->playlistChecker.start(200);
    this->searchChecker.start(200);
    this->settingsChecker.start(200);
}

void FrmMain::ListArtists()
{
    ui->lstArtists->clear();
    ui->lstArtists->addItems(this->libraryTrackReader->GetCachedLibraryArtistList());
    ui->lstArtists->sortItems(Qt::AscendingOrder);
    ui->lstArtists->insertItem(0, "All Artists"); // Add the 'All Artists' option to listbox
    ui->lstArtists->item(0)->setFont(QFont("Sans", 9, QFont::Bold));
    ui->ctrlArtistGroup->setTitle("  Artists (" + QString::number(ui->lstArtists->count() - 1) + ")");
}

void FrmMain::ListAlbums()
{
    ui->lstAlbums->clear();

    if(ui->lstArtists->selectionModel()->selectedRows().count() > 0)
    {
        int currentRowId = ui->lstArtists->selectionModel()->currentIndex().row();
        QString selectedArtist = ui->lstArtists->item(currentRowId)->text();

        ui->lstAlbums->addItems(this->libraryTrackReader->GetCachedLibraryAlbumListForArtist(selectedArtist));
        ui->lstAlbums->sortItems(Qt::AscendingOrder);
        ui->lstAlbums->insertItem(0, "All Albums");
        ui->lstAlbums->item(0)->setFont(QFont("Sans", 9, QFont::Bold));

        if(ui->lstAlbums->count() > 1 && !this->selectLibraryTrack) // Set 1st album to selected
        {
            ui->lstAlbums->item(1)->setSelected(true); // Select first entry after "All Albums" entry
        }

        ui->ctrlAlbumGroup->setTitle("  Albums (" + QString::number(ui->lstAlbums->count() - 1) + ")");
    }
}

void FrmMain::ListTracks(const bool &selectLibraryTrack)
{
    if(ui->lstArtists->selectedItems().count() > 0 && ui->lstAlbums->selectedItems().count() > 0)
    {
        if((QString::compare(ui->lstArtists->selectedItems().at(0)->text(), this->selectedLibraryArtist) != 0
            || QString::compare(ui->lstAlbums->selectedItems().at(0)->text(), this->selectedLibraryAlbum) != 0)
                || selectLibraryTrack)
        {
            ui->ctrlLibraryTracks->setSortingEnabled(false);
            ui->ctrlLibraryTracks->clearContents();
            ui->ctrlLibraryTracks->setRowCount(0);

            this->selectedLibraryArtist = ui->lstArtists->selectedItems().at(0)->text();
            this->selectedLibraryAlbum = ui->lstAlbums->selectedItems().at(0)->text();

            bool isPlayingTrack = false;
            QVector<PlayerTrack> cachedArtistTracks = this->libraryTrackReader->GetCachedLibraryTrackListForAlbum(this->selectedLibraryArtist, this->selectedLibraryAlbum);

            for(int i = 0; i < cachedArtistTracks.count(); i++)
            {
                if(this->trackGridReader->GetRowIdOfSelectedTrackWidgetByFilename(ui->ctrlLibraryTracks, cachedArtistTracks[i].GetTrackFilepath()) == -1)
                {
                    isPlayingTrack = QString::compare(this->playerService->GetTrackFilepath(), cachedArtistTracks[i].GetTrackFilepath(), Qt::CaseSensitive) == 0;
                    this->trackGridWriter->AddTrackToTableGrid(i, isPlayingTrack, cachedArtistTracks[i], ui->ctrlLibraryTracks);
                }

                ui->ctrlLibraryTracksGroup->setTitle("   Tracks (" + QString::number(ui->ctrlLibraryTracks->rowCount()) + ")");
                QApplication::processEvents();
            }
        }
    }

    ui->ctrlLibraryTracks->setSortingEnabled(true);
    ui->ctrlLibraryTracks->resizeColumnsToContents();

    CalculateTrackStatistics();
}

void FrmMain::UpdatePlayerToPlayingStatus(QString &selectedTrackFilepath)
{
    ui->btnPlayPause->setText("Pause");
    ui->btnPlayPause->setEnabled(true);
    ui->btnStop->setEnabled(true);
    ui->ctrlPosition->setEnabled(true);
    ui->lblDuration->setVisible(true);
    ui->btnNext->setEnabled(true);
    ui->btnPrevious->setEnabled(true);
    ui->itemNextTrack->setEnabled(true);
    ui->itemPreviousTrack->setEnabled(true);
    ui->itemPlayPause->setEnabled(true);
    ui->itemPlayPause->setText("Pause");
    ui->itemStop->setEnabled(true);
    ui->itemViewTrackProperties->setEnabled(true);

    this->trackGridWriter->ResetPlayingStatusOfTrackForTableWidget(ui->ctrlLibraryTracks);
    this->trackGridWriter->ResetPlayingStatusOfTrackForTableWidget(ui->ctrlPlaylistTracks);

    PlayerTrack currentTrack = GetAvailablePlayerTrack(selectedTrackFilepath);
    ui->lblStatus->setText(QString("Playing %1 By %2").arg(currentTrack.GetTrackTitle(), currentTrack.GetTrackArtist()));

    ShowTrackIconBalloon(currentTrack);
    ShowAlbumArt(currentTrack.GetTrackFilepath());

    this->durationTimer.start(800);

    this->setWindowTitle(this->playerMode == LIBRARY ? "M++ Player [Library Mode]" : "M++ Player [Playlist Mode]");
}

PlayerTrack FrmMain::GetAvailablePlayerTrack(const QString &selectedTrackFilepath)
{
    PlayerTrack currentTrack = this->libraryTrackReader->GetCachedLibraryTrackByFilepath(selectedTrackFilepath);

    if(currentTrack.GetTrackFilepath().isEmpty())
    {
        return this->playlistTrackReader->GetPlaylistTrack(selectedTrackFilepath);
    }

    return currentTrack;
}

void FrmMain::ShowAlbumArt(QString selectedTrackFilepath)
{
    // Get height/width of QGraphicsView for image
    int albumArtWidth = ui->ctrlAlbumArt->geometry().width();
    int albumArtHeight = ui->ctrlAlbumArt->geometry().height();

    this->albumArtScene = std::shared_ptr<QGraphicsScene>(this->albumArtReader->GetAlbumArt(selectedTrackFilepath, this->applicationSettingsProvider->GetMusicNoteFilePath(), albumArtWidth, albumArtHeight));

    if(!selectedTrackFilepath.isEmpty())
    {
        ui->ctrlAlbumArt->fitInView(QRectF(0, 0, albumArtWidth, albumArtHeight), Qt::KeepAspectRatio);
    }
    else
    {
        ui->ctrlAlbumArt->scale(1, 1);
    }

    ui->ctrlAlbumArt->setScene(this->albumArtScene.get());
    ui->ctrlAlbumArt->show();
}

bool FrmMain::OpenTrack(QString &selectedTrackFilepath)
{
    if(selectedTrackFilepath.length() == 0)
    {
        StopTrack();
        return false;
    }

    this->playerService->OpenTrack(selectedTrackFilepath);

    UpdatePlayerToPlayingStatus(selectedTrackFilepath);

    return true;
}

void FrmMain::NextTrack()
{
    QTableWidget *selectedTableWidget = GetSelectedTableWidgetByPlayerMode();
    selectedTableWidget->clearSelection();

    QString selectedTrackFilepath = !this->repeatFlag ? this->trackShuffler->GetNextTrackFilepath()
                                                      : this->trackShuffler->GetCurrentTrackFilepath();

    if(!selectedTrackFilepath.isEmpty())
    {
        if(!OpenTrack(selectedTrackFilepath))
        {
            return;
        }

        SelectShuffledPlayingTrack(selectedTrackFilepath, selectedTableWidget);

        this->playingLibraryAlbum = this->playerMode == LIBRARY ? ui->lstAlbums->selectedItems().at(0)->text() : "";

        int currentRowId = this->trackGridReader->GetRowIdOfSelectedTrackWidgetByFilename(selectedTableWidget, selectedTrackFilepath);
        this->trackGridWriter->SetPlayingStatusOfTrackForTableWidget(currentRowId, selectedTableWidget);
    }
    else
    {
        StopTrack();
    }
}

void FrmMain::PreviousTrack(const bool &restartTrack = false)
{
    QTableWidget *selectedTableWidget = GetSelectedTableWidgetByPlayerMode();
    selectedTableWidget->clearSelection();

    QString selectedTrackFilepath = restartTrack || this->repeatFlag ? this->trackShuffler->GetCurrentTrackFilepath()
                                                                     : this->trackShuffler->GetPreviousTrackFilepath();
    if(!selectedTrackFilepath.isEmpty())
    {
        if(!OpenTrack(selectedTrackFilepath))
        {
            return;
        }

        SelectShuffledPlayingTrack(selectedTrackFilepath, selectedTableWidget);

        int currentRowId = this->trackGridReader->GetRowIdOfSelectedTrackWidgetByFilename(selectedTableWidget, selectedTrackFilepath);
        this->trackGridWriter->SetPlayingStatusOfTrackForTableWidget(currentRowId, selectedTableWidget);
    }
    else
    {
        StopTrack();
    }
}

int FrmMain::SetPlayingStatusOfTrackForTableWidgetRow(const QString &selectedTrackFilepath, QTableWidget *selectedTableWidget)
{
    int selectedRowId = this->trackGridReader->GetRowIdOfSelectedTrackWidgetByFilename(selectedTableWidget, selectedTrackFilepath);
    this->trackGridWriter->SetPlayingStatusOfTrackForTableWidget(selectedRowId, selectedTableWidget);
    return selectedRowId;
}

void FrmMain::SelectShuffledPlayingTrack(const QString &selectedTrackFilepath, QTableWidget *selectedTableWidget)
{
    if(this->playerMode == LIBRARY && ui->itemShuffleAllLibraryTracks->isChecked())
    {
        ShowPlayingTrackFromLibrary();
    }
    else if(this->playerMode == PLAYLIST && SetPlayingStatusOfTrackForTableWidgetRow(selectedTrackFilepath, selectedTableWidget) == -1) // Finished Shuffling - Clear list
    {
        this->trackShuffler->ClearShuffleList();
        StopTrack();
    }
}

void FrmMain::OpenSearchForm(const PlayerTab &playerTab, const QString &searchQuery)
{
    if(searchQuery.isEmpty())
    {
        return;
    }

    QVector<PlayerTrack> queriedTracks = playerTab == LIBRARY ? this->libraryTrackReader->GetLibraryTracksByQuery(searchQuery)
                                                              : this->playlistTrackReader->GetSearchedPlaylistTracks(searchQuery, ui->ctrlPlaylistTracks);
    if(queriedTracks.count() > 0)
    {
        this->windowManager->ShowProgramWindow(new FrmTrackSearch(this->windowManager, queriedTracks, playerTab, searchQuery, this->playerService, ui->ctrlPlaylistTracks, ui->ctrlPlaylistTracksGroup));
    }
    else
    {
        QMessageBox::information(this, "Search Results", "Nothing could be found");
    }
}

void FrmMain::ShuffleTracks(ShuffleMode shuffleMode)
{
    this->trackShuffler->ClearShuffleList();

    if(!this->trackShuffler->GetRandomisedState())
    {
        ui->itemRepeat->setChecked(false);
        ui->itemShuffle->setChecked(shuffleMode == SHUFFLE);
        ui->itemShuffleAllLibraryTracks->setChecked(shuffleMode == SHUFFLE_ALL_LIBRARY_TRACKS);

        this->repeatFlag = false;
        this->shuffleFlag = true;

        switch (shuffleMode)
        {
            case SHUFFLE:
                this->trackShuffler->AddTrackFilepathsForShuffleMode(this->trackGridReader->GetTrackFilepathsAsList(GetSelectedTableWidgetByPlayerMode()), this->playerService->GetTrackFilepath());
                break;
            case SHUFFLE_ALL_LIBRARY_TRACKS:
                this->trackShuffler->AddTrackFilepathsForShuffleMode(this->libraryTrackReader->GetCachedLibraryTrackFilepaths(), this->playerService->GetTrackFilepath());
                break;
        }

        this->trackShuffler->RandomiseList();
    }
}

void FrmMain::AddTracksToPlaylistFromLibrary(const QStringList &trackFilepaths)
{
    ui->btnPlaylistSelectPlaying->setText("Stop");
    ui->ctrlPlaylistTracks->AddPlaylistTracks(trackFilepaths);
    ui->btnPlaylistSelectPlaying->setText("Select Playing");
}

void FrmMain::ViewPropertiesOfSelectedLibraryTracks()
{
    QStringList selectedLibraryTrackList = this->trackGridReader->GetSelectedTrackFilepathsAsList(ui->ctrlLibraryTracks);

    if(selectedLibraryTrackList.count() == 0) // Must be from album art
    {
        selectedLibraryTrackList.append(this->playerService->GetTrackFilepath());
    }

    this->windowManager->ShowProgramWindow(new FrmTrackProperties(selectedLibraryTrackList, this->playerService));
}

void FrmMain::TrayIconContextMenu(QPoint point)
{
    QMenu trayIconMenu;
    trayIconMenu.setContextMenuPolicy(Qt::CustomContextMenu);

    QAction *playAction = new QAction("Play");
    connect(playAction, SIGNAL(triggered()), this, SLOT(PlayPauseTrack()));
    playAction->setEnabled(this->playerService->IsPlayerServiceRunning() && this->playerService->IsPaused());
    trayIconMenu.addAction(playAction);

    QAction *pauseAction = new QAction("Pause");
    connect(pauseAction, SIGNAL(triggered()), this, SLOT(PlayPauseTrack()));
    pauseAction->setEnabled(this->playerService->IsPlayerServiceRunning() && !this->playerService->IsPaused());
    trayIconMenu.addAction(pauseAction);

    QAction *stopAction = new QAction("Stop");
    connect(stopAction, SIGNAL(triggered()), this, SLOT(StopTrack()));
    stopAction->setEnabled(this->playerService->IsPlayerServiceRunning());
    trayIconMenu.addAction(stopAction);
    trayIconMenu.addSeparator();

    QAction *viewTrackPropertiesAction = new QAction("View Track Properties");
    connect(viewTrackPropertiesAction, SIGNAL(triggered()), this, SLOT(ItemViewTrackProperties_Click()));
    viewTrackPropertiesAction->setEnabled(this->playerService->IsPlayerServiceRunning());
    trayIconMenu.addAction(viewTrackPropertiesAction);
    trayIconMenu.addSeparator();

    QAction *nextTrackAction = new QAction("Next Track");
    connect(nextTrackAction, SIGNAL(triggered()), this, SLOT(BtnNext_Click()));
    nextTrackAction->setEnabled(this->playerService->IsPlayerServiceRunning());
    trayIconMenu.addAction(nextTrackAction);

    QAction *previousTrackAction = new QAction("Previous Track");
    connect(previousTrackAction, SIGNAL(triggered()), this, SLOT(BtnPrevious_Click()));
    previousTrackAction->setEnabled(this->playerService->IsPlayerServiceRunning());
    trayIconMenu.addAction(previousTrackAction);

    trayIconMenu.addSeparator();
    trayIconMenu.addAction("Show/Hide", this, SLOT(ItemShowHide_Click()));
    trayIconMenu.addAction("Exit", this, SLOT(CloseApplication()));

    trayIconMenu.exec(point); // Show menu
}

void FrmMain::UpdateSavedLibraryTrack()
{
    this->libraryTrackReader->GetLatestLibraryTracks();
    QString selectedTrackFilepath = SessionSettings::LastTrackPathEdited;

    if(!selectedTrackFilepath.isEmpty())
    {
        ListArtists();
        ShowPlayingTrackFromLibrary(selectedTrackFilepath);
    }
}

void FrmMain::ShowPlayingTrackFromLibrary(QString selectedTrackFilepath)
{
    if(selectedTrackFilepath.isEmpty())
    {
        selectedTrackFilepath = this->playerService->GetTrackFilepath();
    }

    this->selectLibraryTrack = true;

    if (ui->lstArtists->count() > 1)
    {
        ui->lstArtists->clearSelection();
        PlayerTrack currentTrack = GetAvailablePlayerTrack(selectedTrackFilepath);

        this->trackGridWriter->SelectTrackListBoxItemByValue(ui->lstArtists, currentTrack.GetTrackArtist()); // Select artist through list

        if (ui->lstAlbums->count() > 1)
        {
            QString trackAlbumName = this->playingLibraryAlbum != "All Albums" ? currentTrack.GetTrackAlbum() : "All Albums";
            this->trackGridWriter->SelectTrackListBoxItemByValue(ui->lstAlbums, trackAlbumName); // Select album through list

            int trackRowId = this->trackGridReader->GetRowIdOfSelectedTrackWidgetByFilename(ui->ctrlLibraryTracks, selectedTrackFilepath);

            if(selectedTrackFilepath == this->playerService->GetTrackFilepath())
            {
                this->trackGridWriter->SetPlayingStatusOfTrackForTableWidget(trackRowId, ui->ctrlLibraryTracks);
            }
        }
    }

    if(!SessionSettings::LastTrackPathEdited.isEmpty())
    {
        ListTracks(true);
    }

    this->playingLibraryAlbum = "";
    this->selectLibraryTrack = false;
}

void FrmMain::SetPlayerInterface(const PlayerInterfaceStatus &interfaceStatus,
                                 const int &mininumHeight,
                                 const int &mininumWidth,
                                 const bool &showStatusGroup,
                                 const bool &showTabControl)
{
    this->interfaceStatus = interfaceStatus;

    if(this->interfaceStatus != SHOW_STATUS)
    {
        setMinimumHeight(862);
        setMaximumHeight(16777215);
        setMaximumWidth(16777215);
    }

    resize(mininumWidth, mininumHeight);

    ui->ctrlStatusGroup->setVisible(showStatusGroup);
    ui->ctrlTab->setVisible(showTabControl);
}

void FrmMain::AddHistoryToSearchComboBox(QComboBox *comboBox, const QString &searchQuery)
{
    if(comboBox->findText(searchQuery) == -1)
    {
        comboBox->addItem(searchQuery);
    }
}

void FrmMain::InitiateTrackSearch(QPushButton *tabSearchButton, const QString &searchQuery, const PlayerTab &playerTab)
{
    tabSearchButton->setEnabled(false);
    OpenSearchForm(playerTab, searchQuery);
    tabSearchButton->setEnabled(true);
}

int FrmMain::GetCurrentRowIdOfSelectedTableWidget()
{
    QTableWidget *selectedTableWidget = GetSelectedTableWidgetByPlayerMode();
    return this->trackGridReader->GetCurrentTrackRowIdOfSelectedTrackWidget(selectedTableWidget);
}

QTableWidget* FrmMain::GetSelectedTableWidgetByPlayerMode()
{
    return this->playerMode == LIBRARY ? ui->ctrlLibraryTracks : ui->ctrlPlaylistTracks;
}

void FrmMain::ResetPlayModeStatus()
{
    this->shuffleFlag = false;
    this->repeatFlag = false;
    ui->itemRepeat->setChecked(false);
    ui->itemShuffle->setChecked(false);
    ui->itemShuffleAllLibraryTracks->setChecked(false);

    QTableWidget *selectedTableWidget = GetSelectedTableWidgetByPlayerMode();
    this->trackShuffler->ClearShuffleList();
    this->trackShuffler->AddTrackFilepathsForDefaultMode(this->trackGridReader->GetTrackFilepathsAsList(selectedTableWidget), this->playerService->GetTrackFilepath());
}

void FrmMain::SetupPlayerTrackListForTrackShuffler(QTableWidget *selectedTableWidget, const QString &selectedTrackFilepath)
{
    if(this->shuffleFlag)
    {
        if(ui->itemShuffleAllLibraryTracks->isChecked() && this->playerMode == PLAYLIST)
        {
            ui->itemShuffle->setChecked(this->shuffleFlag);
            ui->itemShuffleAllLibraryTracks->setChecked(false);
        }

        ShuffleMode shuffleMode = ui->itemShuffleAllLibraryTracks->isChecked() ? SHUFFLE_ALL_LIBRARY_TRACKS : SHUFFLE;
        ShuffleTracks(shuffleMode);
    }
    else
    {
        this->trackShuffler->ClearShuffleList();
        this->trackShuffler->AddTrackFilepathsForDefaultMode(this->trackGridReader->GetTrackFilepathsAsList(selectedTableWidget), selectedTrackFilepath);
    }
}

void FrmMain::SetupPlaylistTrackListForTrackShuffler()
{
    if(!this->shuffleFlag)
    {
        this->trackShuffler->ClearShuffleList();
        this->trackShuffler->AddTrackFilepathsForDefaultMode(this->trackGridReader->GetTrackFilepathsAsList(ui->ctrlPlaylistTracks), this->playerService->GetTrackFilepath());
    }
}

void FrmMain::SaveWindowSettings()
{
    if(this->interfaceStatus != SHOW_STATUS)
    {
        this->previousFormHeight = size().height();
        this->previousFormWidth = size().width();
    }

    QSize currentFormSize = QSize(this->previousFormWidth, this->previousFormHeight);
    WindowSettings windowSettings = WindowSettings(saveGeometry(), saveState(), isMaximized(), this->interfaceStatus, pos(), currentFormSize);

    this->applicationSettingsProvider->SaveWindowSettings("frmMain", windowSettings);
    this->applicationSettingsProvider->SaveDefaultVolume(ui->ctrlVolume->value()); // Save default volume
}

void FrmMain::LoadWindowSettings()
{
    if(!this->applicationSettingsProvider->HasSavedWindowSettingsForForm("frmMain"))
    {
        return;
    }

    WindowSettings windowSettings = this->applicationSettingsProvider->GetWindowSettings("frmMain");

    restoreGeometry(windowSettings.GetFormGeometry());
    restoreState(windowSettings.GetFormState());
    move(windowSettings.GetFormPosition());
    resize(windowSettings.GetFormSize());

    if (windowSettings.IsFormMaximised())
    {
        showMaximized();
    }

    this->interfaceStatus = windowSettings.GetInterfaceStatus();
    this->previousFormHeight = windowSettings.GetFormSize().height();
    this->previousFormWidth = windowSettings.GetFormSize().width();

    switch(this->interfaceStatus)
    {
        case SHOW_STATUS:
            ShowStatusInterface();
            break;
        case SHOW_LIBRARY_PLAYLIST:
            ShowLibraryPlaylistInterface();
            break;
        default:
            ResetPlayerInterface();
            break;
    }
}

void FrmMain::ShowTrackIconBalloon(PlayerTrack &currentTrack)
{
    if(!this->isActiveWindow() && this->playerService->IsPlaying())
    {
        this->trayIcon.get()->showMessage("Next Track", QString("%1 by %2").arg(currentTrack.GetTrackTitle(), currentTrack.GetTrackArtist()), QSystemTrayIcon::Information, 7000);
    }
}

void FrmMain::UpdateSavedPlaylistTrack() // Changes have been made to track via tag editing
{
    int rowId = this->trackGridReader->GetRowIdOfTrackByFilepath(ui->ctrlPlaylistTracks, SessionSettings::LastTrackPathEdited);

    if(rowId != -1)
    {
        PlayerTrack currentTrack = this->playlistTrackReader->GetPlaylistTrack(SessionSettings::LastTrackPathEdited);
        this->trackGridWriter->UpdateTrackInTableWidget(rowId, currentTrack, ui->ctrlPlaylistTracks);
        ui->ctrlPlaylistTracks->resizeColumnsToContents();
    }
}
