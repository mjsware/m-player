// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "FrmTrackProperties.hpp"

FrmTrackProperties::FrmTrackProperties(const QStringList &selectedTrackFilepaths,
                                       std::shared_ptr<IPlayerService> playerService,
                                       QWidget *parent) : QWidget(parent),
    ui(new Ui::FrmTrackProperties)
{
    ui->setupUi(this); // Set up widget ui

    this->albumArtReader = std::shared_ptr<IAlbumArtReader>(new AlbumArtReader());
    this->playlistTrackReader = std::shared_ptr<IPlaylistTrackReader>(new PlaylistTrackReader());
    this->playlistTrackWriter = std::shared_ptr<IPlaylistTrackWriter>(new PlaylistTrackWriter());
    this->applicationSettingsProvider = std::shared_ptr<IApplicationSettingsProvider>(new ApplicationSettingsProvider());
    this->libraryTrackReader = std::shared_ptr<ILibraryTrackReader>(new LibraryTrackReader());
    this->libraryTrackWriter = std::shared_ptr<ILibraryTrackWriter>(new LibraryTrackWriter(this->libraryTrackReader));
    this->playerService = playerService;

    this->selectedTrackFilepaths = selectedTrackFilepaths;

    if(selectedTrackFilepaths.count() > 0)
    {
        for(int i = 0; i < selectedTrackFilepaths.count(); i++)
        {
            QFileInfo trackFileInfo(selectedTrackFilepaths[i]);
            ui->cboTrackSelection->addItem(trackFileInfo.fileName());
        }

        this->currentSelectedTrackIndex = 0;
        this->selectedTrackFilepath = selectedTrackFilepaths[0];

        ui->btnPrevious->setEnabled(false);

        if(selectedTrackFilepaths.count() == 1)
        {
            ui->cboTrackSelection->setEnabled(false);
            ui->btnNext->setVisible(false);
            ui->btnPrevious->setVisible(false);
        }

        SetupFormControlSlots();
        SetupFormControls();
        SetSelectedTrackFilePath(this->selectedTrackFilepath);
    }
    else
    {
        this->close();
    }
}

void FrmTrackProperties::SetupFormControlSlots()
{
    connect(ui->cboWebContent, SIGNAL(currentIndexChanged(int)), this, SLOT(ShowWebContent()));
    connect(ui->btnEdit, SIGNAL(clicked()), this, SLOT(EditTrackDetails()));
    connect(ui->btnSave, SIGNAL(clicked()), this, SLOT(SaveTrackDetails()));
    connect(ui->btnViewTrack, SIGNAL(clicked()), this, SLOT(ViewTrackInFileManager()));
    connect(ui->btnAddToLibrary, SIGNAL(clicked()), this, SLOT(AddTrackToLibrary()));
    connect(ui->btnRemoveFromLibrary, SIGNAL(clicked()), this, SLOT(RemoveTrackFromLibrary()));
    connect(ui->btnRescanTrack, SIGNAL(clicked()), this, SLOT(RescanTrackDetails()));
    connect(ui->ctrlTrackActions, SIGNAL(currentChanged(int)), this, SLOT(TabChange(int)));
    connect(ui->cboTrackSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(SelectTrackFromTrackSelectionDropdown()));
    connect(ui->btnPrevious, SIGNAL(clicked()), this, SLOT(SelectPreviousTrack()));
    connect(ui->btnNext, SIGNAL(clicked()), this, SLOT(SelectNextTrack()));
    connect(ui->chkViewPlayingTrack, SIGNAL(stateChanged(int)), SLOT(ChkViewPlayingTrack_StateChange()));
}

void FrmTrackProperties::SetupFormControls()
{
    std::unique_ptr<IThemeProvider> themeProvider = std::unique_ptr<IThemeProvider>(new ThemeProvider());
    themeProvider->ApplyThemingToForm(this, "FrmTrackProperties");

    QWidget::setWindowIcon(QIcon(this->applicationSettingsProvider->GetProgramIconFilePath()));
    setWindowFlags(windowFlags() ^ Qt::WindowMaximizeButtonHint);

    ShowAlbumArt();

    QNetworkProxyFactory::setUseSystemConfiguration(true);
    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, true);
    QWebSettings::globalSettings()->setAttribute(QWebSettings::AutoLoadImages, true);
    QWebSettings::globalSettings()->setAttribute(QWebSettings::PrivateBrowsingEnabled, true);

    this->currentTabIndex = TrackPropertiesTab::TRACK_INFORMATION;

    this->webView = std::shared_ptr<QWebView>(new QWebView());
    this->webView->setAttribute(Qt::WA_DeleteOnClose, true);

    this->webLayout = std::shared_ptr<QGridLayout>(new QGridLayout);
    this->webLayout->addWidget(this->webView.get());

    ui->tabWebContent->setAttribute(Qt::WA_DeleteOnClose, true);
    ui->tabWebContent->setLayout(this->webLayout.get());
}

void FrmTrackProperties::AddTrackToLibrary()
{
    if(!this->libraryTrackReader->IsLibraryTrackByFilepathCached(this->selectedTrackFilepath))
    {
        this->libraryTrackWriter->AddSelectedTrackToLibrary(this->currentTrack);
        SessionSettings::LastTrackPathEdited = this->currentTrack.GetTrackFilepath();

        ui->btnAddToLibrary->setEnabled(false);
        ui->btnRemoveFromLibrary->setEnabled(true);
    }
}

void FrmTrackProperties::RemoveTrackFromLibrary()
{
    if(this->libraryTrackReader->IsLibraryTrackByFilepathCached(this->selectedTrackFilepath))
    {
        this->currentTrack = this->libraryTrackReader->GetCachedLibraryTrackByFilepath(this->selectedTrackFilepath);
        this->libraryTrackWriter->RemoveLibraryTrack(this->selectedTrackFilepath);
        SessionSettings::LastTrackPathEdited = this->selectedTrackFilepath;

        ui->btnRemoveFromLibrary->setEnabled(false);
        ui->btnAddToLibrary->setEnabled(true);
    }
}

void FrmTrackProperties::SelectTrackFromTrackSelectionDropdown()
{
    int selectedTrackIndex = ui->cboTrackSelection->currentIndex();
    QString trackFilepath = this->selectedTrackFilepaths[selectedTrackIndex];

    if(trackFilepath != this->selectedTrackFilepath)
    {
        this->currentSelectedTrackIndex = selectedTrackIndex;
        SetSelectedTrackFilePath(trackFilepath);
    }

    UpdateTrackNavigationButtonsUponSelection();
}

void FrmTrackProperties::SelectNextTrack()
{
    if(this->currentSelectedTrackIndex < (this->selectedTrackFilepaths.count() - 1))
    {
        this->currentSelectedTrackIndex++;
        QString trackFilepath = this->selectedTrackFilepaths[this->currentSelectedTrackIndex];

        SetSelectedTrackFilePath(trackFilepath);
    }

    UpdateTrackNavigationButtonsUponSelection();
}

void FrmTrackProperties::SelectPreviousTrack()
{
    if(this->currentSelectedTrackIndex > 0)
    {
        this->currentSelectedTrackIndex--;
        QString trackFilepath = this->selectedTrackFilepaths[this->currentSelectedTrackIndex];

        SetSelectedTrackFilePath(trackFilepath);
    }

    UpdateTrackNavigationButtonsUponSelection();
}

void FrmTrackProperties::UpdateTrackNavigationButtonsUponSelection()
{
    int totalTrackSelectionCount = this->selectedTrackFilepaths.count() - 1;

    ui->btnNext->setEnabled(this->currentSelectedTrackIndex != totalTrackSelectionCount);
    ui->btnPrevious->setEnabled(this->currentSelectedTrackIndex > 0);
}

void FrmTrackProperties::ChkViewPlayingTrack_StateChange()
{
    if(!this->playerService->IsPlayerServiceRunning() || this->playerService->GetTrackFilepath() == "")
    {
        ui->chkViewPlayingTrack->setCheckState(Qt::CheckState::Unchecked);
        return;
    }

    if(ui->chkViewPlayingTrack->isChecked())
    {
        SetSelectedTrackFilePath(this->playerService->GetTrackFilepath());

        ui->btnNext->setEnabled(false);
        ui->btnPrevious->setEnabled(false);
        ui->cboTrackSelection->setEnabled(false);
        ui->lblTrackCountInformation->setText("Viewing playing track");
        ui->chkViewPlayingTrack->setChecked(true);
    }
    else
    {
        SelectTrackFromTrackSelectionDropdown();
    }
}

void FrmTrackProperties::RescanTrackDetails()
{
    ShowAlbumArt();
    ShowMediaInformation();
    ShowFileInformation();
    ShowWebContent();
}

void FrmTrackProperties::EditTrackDetails()
{
    ui->btnSave->setEnabled(true);
    ui->btnEdit->setEnabled(false);
    ui->txtTrackNumber->setReadOnly(false);
    ui->txtTrackArtist->setReadOnly(false);
    ui->txtAlbumName->setReadOnly(false);
    ui->txtTrackTitle->setReadOnly(false);
    ui->txtTrackGenre->setReadOnly(false);
    ui->txtYearReleased->setReadOnly(false);
}

void FrmTrackProperties::SaveTrackDetails()
{
    try
    {
        PlayerTrack trackToSave(this->selectedTrackFilepath);

        trackToSave.SetTrackNumber(ui->txtTrackNumber->text().length() > 0 ? ui->txtTrackNumber->text().toUInt() : 0);
        trackToSave.SetTrackArtist(ui->txtTrackArtist->text());
        trackToSave.SetTrackAlbum(ui->txtAlbumName->text());
        trackToSave.SetTrackTitle(ui->txtTrackTitle->text());
        trackToSave.SetTrackGenre(ui->txtTrackGenre->text());
        trackToSave.SetTrackYear(ui->txtYearReleased->text().length() > 0 ? ui->txtYearReleased->text().toUInt() : 0);

        this->libraryTrackWriter->UpdateLibraryTrack(trackToSave); // Update in library
        this->playlistTrackWriter->SavePlaylistTrack(trackToSave); // Update in file
        this->currentTrack = trackToSave;

        setWindowTitle(this->currentTrack.GetTrackArtist() + " - " + this->currentTrack.GetTrackTitle());

        SessionSettings::LastTrackPathEdited = this->selectedTrackFilepath;
    }
    catch(std::exception&)
    {
        QMessageBox::critical(nullptr, "Save Track Details", "It was not possible to save your changes.");
    }

    ui->txtTrackNumber->setReadOnly(true);
    ui->txtTrackArtist->setReadOnly(true);
    ui->txtAlbumName->setReadOnly(true);
    ui->txtTrackTitle->setReadOnly(true);
    ui->txtTrackGenre->setReadOnly(true);
    ui->txtYearReleased->setReadOnly(true);
    ui->btnSave->setEnabled(false);
    ui->btnEdit->setEnabled(true);
}

void FrmTrackProperties::ViewTrackInFileManager()
{
    if(this->selectedTrackFilepath.length() > 0)
    {
        QDir trackDirectory = QFileInfo(this->selectedTrackFilepath).absoluteDir();
        QDesktopServices::openUrl(QUrl("file:///" + trackDirectory.absolutePath(), QUrl::TolerantMode));
    }
}

void FrmTrackProperties::ShowAlbumArt()
{
    // Get height/width of QGraphicsView for image
    int albumArtWidth = ui->ctrlAlbumArt->geometry().width();
    int albumArtHeight = ui->ctrlAlbumArt->geometry().height();

    this->albumArtScene = std::shared_ptr<QGraphicsScene>(this->albumArtReader->GetAlbumArt(this->selectedTrackFilepath, this->applicationSettingsProvider->GetMusicNoteFilePath(), albumArtWidth, albumArtHeight));

    if(!this->selectedTrackFilepath.isEmpty())
    {
        ui->ctrlAlbumArt->fitInView(QRectF(0, 0, albumArtWidth, albumArtHeight), Qt::KeepAspectRatio);
    }
    else
    {
        ui->ctrlAlbumArt->scale(1, 1);
    }

    ui->ctrlAlbumArt->setScene(this->albumArtScene.get());
    ui->ctrlAlbumArt->show();
}

void FrmTrackProperties::ShowWebContent()
{
    QString selectedUrl = "";
    QString selectedArtist = QUrl::toPercentEncoding(ui->txtTrackArtist->text());
    QString selectedTitle = QUrl::toPercentEncoding(ui->txtTrackTitle->text());

    switch(ui->cboWebContent->currentIndex())
    {
        case TrackWebContentType::YOUTUBE: // Artist Title
            selectedUrl = QString("https://www.youtube.com/results?search_query=%1 %2").arg(selectedArtist, selectedTitle);
            break;
        case TrackWebContentType::WIKIPEDIA: // Artist
            selectedUrl = QString("https://en.wikipedia.org/w/index.php?search=%1&button=&title=Special%3ASearch").arg(selectedArtist);
            break;
        case TrackWebContentType::LYRICS: // Artist Title Lyrics
            selectedUrl = QString("https://duckduckgo.com/?q=%1+%2+lyrics").arg(selectedArtist, selectedTitle);
            break;
    }

    if(selectedUrl != this->currentWebViewUrl)
    {
        this->webView->load(QUrl(selectedUrl));
        this->currentWebViewUrl = selectedUrl;
    }

    this->currentTabIndex = ui->cboWebContent->currentIndex();
}

void FrmTrackProperties::TabChange(const int &tabIndex)
{
    if(tabIndex == TrackPropertiesTab::WEB_CONTENT) // Web content
    {
        setWindowFlags(windowFlags() & Qt::Window);
        setMaximumSize(this->MAXIMUM_WIDTH, this->MAXIMUM_HEIGHT);

        int currentWebContentTabWidth = this->customWebContentWidth != 0 && this->customWebContentHeight != 0 ? this->customWebContentWidth : this->DEFAULT_WEBCONTENT_TAB_WIDTH;
        int currentWebContentTabHeight = this->customWebContentWidth != 0 && this->customWebContentHeight != 0 ? this->customWebContentHeight : this->DEFAULT_WEBCONTENT_TAB_HEIGHT;
        resize(currentWebContentTabWidth, currentWebContentTabHeight);

        show();

        ShowWebContent();
    }
    else if(tabIndex == TrackPropertiesTab::TRACK_INFORMATION) // Media information
    {
        setWindowFlags(windowFlags() ^ Qt::WindowMaximizeButtonHint);
        setMaximumSize(640, 527);
        show();
    }
}

void FrmTrackProperties::SetSelectedTrackFilePath(const QString &selectedTrackFilepath)
{
    this->selectedTrackFilepath = selectedTrackFilepath;

    if(this->selectedTrackFilepath.length() > 0)
    {
        ui->cboTrackSelection->setCurrentIndex(this->currentSelectedTrackIndex);
        ui->lblTrackCountInformation->setText(QString("Viewing track %1 out of %2").arg(QString::number(this->currentSelectedTrackIndex + 1), QString::number(selectedTrackFilepaths.count())));

        this->currentTrack = this->playlistTrackReader->GetPlaylistTrack(this->selectedTrackFilepath);

        ShowAlbumArt();
        ShowMediaInformation();
        ShowFileInformation();
        ShowWebContent();

        ui->btnAddToLibrary->setEnabled(false);
        ui->btnRemoveFromLibrary->setEnabled(false);

        if(!this->applicationSettingsProvider->GetLibraryPath().isEmpty() && !this->applicationSettingsProvider->GetLibraryMusicPath().isEmpty())
        {
            PlayerTrack selectedTrack = this->libraryTrackReader->GetCachedLibraryTrackByFilepath(this->selectedTrackFilepath);

            ui->btnAddToLibrary->setEnabled(selectedTrack.GetTrackFilepath().isEmpty());
            ui->btnRemoveFromLibrary->setEnabled(!selectedTrack.GetTrackFilepath().isEmpty());
        }
    }
}

void FrmTrackProperties::ShowFileInformation()
{
    qint64 selectedFileSize = 0;
    QFile audioFile(this->selectedTrackFilepath);
    QFileInfo selectedFileInfo(this->selectedTrackFilepath);

    if (audioFile.open(QIODevice::ReadOnly))
    {
        selectedFileSize = (audioFile.size() / 1024 / 1024);
        audioFile.close();
    }

    ui->lblBitrate->setText(QString::number(this->currentTrack.GetTrackBitrate()) + " kbps");
    ui->lblDuration->setText(this->currentTrack.GetTrackShortDuration());
    ui->lblFilename->setText(selectedFileInfo.fileName());
    ui->lblFilesize->setText(QString::number(selectedFileSize) + " MB");
}

void FrmTrackProperties::ShowMediaInformation()
{
    ui->txtTrackNumber->setText(QString::number(this->currentTrack.GetTrackNumber()));
    ui->txtTrackTitle->setText(this->currentTrack.GetTrackTitle());
    ui->txtTrackArtist->setText(this->currentTrack.GetTrackArtist());
    ui->txtAlbumName->setText(this->currentTrack.GetTrackAlbum());
    ui->txtTrackGenre->setText(this->currentTrack.GetTrackGenre());

    setWindowTitle(this->currentTrack.GetTrackArtist() + " - " + this->currentTrack.GetTrackTitle());

    if(this->currentTrack.GetTrackYear() != 0)
    {
        ui->txtYearReleased->setText(QString::number(this->currentTrack.GetTrackYear()));
    }
}

void FrmTrackProperties::resizeEvent(QResizeEvent*)
{
    if(ui->ctrlTrackActions->currentIndex() == TrackPropertiesTab::WEB_CONTENT)
    {
        this->customWebContentWidth = this->size().width();
        this->customWebContentHeight = this->size().height();
    }
}

void FrmTrackProperties::closeEvent(QCloseEvent*)
{
    if(this->isVisible())
    {
        this->webView->close();
    }
}

FrmTrackProperties::~FrmTrackProperties()
{
    delete this->ui;
}
