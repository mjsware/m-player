// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef FRMTRACKSEARCH_HPP
#define FRMTRACKSEARCH_HPP

#include <memory>
#include <QMenu>

#include "MPlayer.Domain/Settings/ThemeProvider.hpp"
#include "MPlayer.Domain/Settings/ApplicationSettingsProvider.hpp"
#include "MPlayer.Domain/Player/PlayerService.hpp"
#include "MPlayer.Domain/Track/TrackGridReader.hpp"
#include "MPlayer.Domain/Track/TrackGridWriter.hpp"
#include "MPlayer.Domain/Window/WindowManager.hpp"
#include "MPlayer.Entities/Track/PlayerTrack.hpp"
#include "MPlayer.Entities/Settings/SessionSettings.hpp"
#include "FrmTrackProperties.hpp"
#include "ui_FrmTrackSearch.h"

using namespace MPlayer::Domain::Window;

class FrmTrackSearch : public QWidget
{
    Q_OBJECT
    public:
        explicit FrmTrackSearch(std::shared_ptr<IWindowManager> windowManager,
                                QVector<PlayerTrack> queriedTracks,
                                PlayerTab playerTab,
                                const QString &searchQuery,
                                std::shared_ptr<IPlayerService> playerService,
                                QTableWidget *playlistTableWidget,
                                QGroupBox *playlistGroupBox,
                                QWidget *parent = nullptr);

    public slots:
        void DoubleClickSearchResults(QTableWidgetItem *selectedRow);
        void ShowSearchContextMenu(const QPoint &pos);

    private:
        void ApplyTheming();
        void AddTracksToPlaylist(const QStringList &selectedPlaylistFilepaths);
        void AddTracksToSearchResults();
        void SelectTrackForPlaying(const QString &trackFilepath);
        void SetupFormControls();
        void SetupFormControlSlots();
        void ViewPropertiesOfSelectedSearchedTracks();

        Ui::FrmTrackSearch *ui;
        PlayerTab currentPlayerTab;
        QVector<PlayerTrack> selectedTracks;
        QTableWidget *playlistTableWidget;
        QGroupBox *playlistGroupBox;
        std::shared_ptr<IWindowManager> windowManager;
        std::shared_ptr<IPlayerService> playerService;
        std::unique_ptr<ITrackGridReader> trackGridReader;
        std::unique_ptr<ITrackGridWriter> trackGridWriter;  
};

#endif // FRMTRACKSEARCH_HPP
