// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef FRMTRACKPROPERTIES_HPP
#define FRMTRACKPROPERTIES_HPP

#include <QClipboard>
#include <QDesktopServices>
#include <QCloseEvent>
#include <QNetworkProxyFactory>
#include <QGraphicsScene>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QWebSettings>
#include <QWebView>
#include <memory>

#include "MPlayer.Domain/Library/LibraryTrackReader.hpp"
#include "MPlayer.Domain/Library/LibraryTrackWriter.hpp"
#include "MPlayer.Domain/Playlist/PlaylistTrackReader.hpp"
#include "MPlayer.Domain/Playlist/PlaylistTrackWriter.hpp"
#include "MPlayer.Domain/Player/PlayerService.hpp"
#include "MPlayer.Domain/Settings/ThemeProvider.hpp"
#include "MPlayer.Domain/Settings/ApplicationSettingsProvider.hpp"
#include "MPlayer.Domain/Track/AlbumArtReader.hpp"
#include "MPlayer.Entities/Track/PlayerTrack.hpp"
#include "MPlayer.Entities/Settings/SessionSettings.hpp"
#include "MPlayer.Entities/Track/TrackPropertiesTab.hpp"
#include "MPlayer.Entities/Track/TrackWebContentType.hpp"
#include "ui_FrmTrackProperties.h"

using namespace MPlayer::Domain::Library;
using namespace MPlayer::Domain::Player;
using namespace MPlayer::Domain::Playlist;
using namespace MPlayer::Domain::Track;

class FrmTrackProperties : public QWidget
{
    Q_OBJECT
    public:
        explicit FrmTrackProperties(const QStringList &selectedTrackFilepaths,
                                    std::shared_ptr<IPlayerService> playerService,
                                    QWidget *parent = nullptr);
        ~FrmTrackProperties();

    public slots:
        void AddTrackToLibrary();
        void ChkViewPlayingTrack_StateChange();
        void closeEvent(QCloseEvent*);
        void EditTrackDetails();
        void RemoveTrackFromLibrary();
        void RescanTrackDetails();
        void resizeEvent(QResizeEvent*);
        void SaveTrackDetails();
        void SelectPreviousTrack();
        void SelectNextTrack();
        void SelectTrackFromTrackSelectionDropdown();
        void ShowWebContent();
        void TabChange(const int &tabIndex);
        void ViewTrackInFileManager();

    private:
        void SetupFormControls();
        void SetupFormControlSlots();
        void SetSelectedTrackFilePath(const QString &selectedTrackFilepaths);
        void ShowAlbumArt();
        void ShowFileInformation();
        void ShowMediaInformation();
        void UpdateTrackNavigationButtonsUponSelection();

        Ui::FrmTrackProperties *ui;
        int currentTabIndex;
        int customWebContentWidth = 0;
        int customWebContentHeight = 0;

        const int MAXIMUM_HEIGHT = 16777215;
        const int MAXIMUM_WIDTH = 16777215;
        const int DEFAULT_WEBCONTENT_TAB_HEIGHT = 980;
        const int DEFAULT_WEBCONTENT_TAB_WIDTH = 980;

        std::shared_ptr<IApplicationSettingsProvider> applicationSettingsProvider;
        std::shared_ptr<ILibraryTrackReader> libraryTrackReader;
        std::shared_ptr<ILibraryTrackWriter> libraryTrackWriter;
        std::shared_ptr<IPlaylistTrackReader> playlistTrackReader;
        std::shared_ptr<IPlaylistTrackWriter> playlistTrackWriter;
        std::shared_ptr<IAlbumArtReader> albumArtReader;
        std::shared_ptr<QGridLayout> webLayout;
        std::shared_ptr<QGraphicsScene> albumArtScene;
        std::shared_ptr<QWebView> webView;
        std::shared_ptr<IPlayerService> playerService;

        QStringList selectedTrackFilepaths;
        QString selectedTrackFilepath;
        int currentSelectedTrackIndex;
        PlayerTrack currentTrack;
        QString currentWebViewUrl;
};

#endif // FRMTRACKPROPERTIES_HPP
