// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "FrmApplicationSettings.hpp"

FrmApplicationSettings::FrmApplicationSettings(QWidget *parent) : QWidget(parent),
    ui(new Ui::FrmApplicationSettings)
{
    ui->setupUi(this);

    this->applicationSettingsProvider = std::shared_ptr<IApplicationSettingsProvider>(new ApplicationSettingsProvider());

    SetupFormControlSlots();
    SetupFormControls();
}

void FrmApplicationSettings::SetupFormControlSlots()
{
    connect(ui->btnReset, SIGNAL(clicked()), this, SLOT(ResetApplicationSettings()));
    connect(ui->btnSave, SIGNAL(clicked()), this, SLOT(SaveApplicationSettings()));
    connect(ui->btnSelectPlaylist, SIGNAL(clicked()), this, SLOT(SelectPlaylistFile()));
    connect(ui->btnSelectLibrary, SIGNAL(clicked()), this, SLOT(SelectLibraryLocation()));
    connect(ui->chkLoadPlaylist, SIGNAL(stateChanged(int)), SLOT(ChkLoadPlaylist_StateChange()));
}

void FrmApplicationSettings::SetupFormControls()
{
    std::unique_ptr<IThemeProvider> themeProvider = std::unique_ptr<IThemeProvider>(new ThemeProvider());
    themeProvider->ApplyThemingToForm(this, "FrmApplicationSettings");

    this->applicationSettingsProvider->GetApplicationSettings();

    ui->chkHideMPlayer->setChecked(this->applicationSettingsProvider->GetHideOnStartup());
    ui->chkLoadPlaylist->setChecked(this->applicationSettingsProvider->GetAutoLoadPlaylist());
    ui->txtPlaylistLocation->setText(this->applicationSettingsProvider->GetPlaylistPath());
    ui->txtLibraryLocation->setText(this->applicationSettingsProvider->GetLibraryMusicPath());
    ui->cboThemes->addItems(this->applicationSettingsProvider->GetProgramThemeList());
    ui->cboThemes->setCurrentText(this->applicationSettingsProvider->GetDefaultThemeName());
    ui->cboAudioDrivers->setCurrentText(this->applicationSettingsProvider->GetDefaultAudioDriverName());

    setFixedSize(width(), height()); // Set widget size
    QWidget::setWindowIcon(QIcon(this->applicationSettingsProvider->GetProgramIconFilePath()));
}

void FrmApplicationSettings::ChkLoadPlaylist_StateChange()
{
    ui->btnSelectPlaylist->setEnabled(ui->chkLoadPlaylist->isChecked());

    if(!ui->chkLoadPlaylist->isChecked())
    {
        ui->txtPlaylistLocation->setText("");
    }
}

void FrmApplicationSettings::SelectPlaylistFile()
{
    std::unique_ptr<QFileDialog> fileDialog(new QFileDialog(this));
    QString selectedPlaylistPath = fileDialog->getOpenFileName(this, tr("Open Playlists"), this->applicationSettingsProvider->GetPlaylistsDirectory(), tr("Playlist Files (*.xml)"));
    ui->txtPlaylistLocation->setText(selectedPlaylistPath);
}

void FrmApplicationSettings::SelectLibraryLocation()
{
    std::unique_ptr<QFileDialog> fileDialog(new QFileDialog(this));
    fileDialog->setFileMode(QFileDialog::Directory);
    fileDialog->setOption(QFileDialog::ShowDirsOnly);
    fileDialog->setDirectory(QDir::homePath());

    if(fileDialog->exec())
    {
        ui->txtLibraryLocation->setText(fileDialog->selectedFiles().at(0));
    }
}

void FrmApplicationSettings::SaveApplicationSettings()
{
    ApplicationSettings applicationSettings = this->applicationSettingsProvider->GetApplicationSettings();

    applicationSettings.SetHideOnStartup(ui->chkHideMPlayer->isChecked());
    applicationSettings.SetAutoLoadPlaylist(ui->txtPlaylistLocation->text().length() > 0 ? ui->chkLoadPlaylist->isChecked() : false);
    applicationSettings.SetPlaylistPath(ui->txtPlaylistLocation->text().length() > 0 ? ui->txtPlaylistLocation->text() : "");
    applicationSettings.SetLibraryMusicPath(ui->txtLibraryLocation->text());
    applicationSettings.SetDefaultTheme(ui->cboThemes->currentText());
    applicationSettings.SetDefaultAudioDriverName(ui->cboAudioDrivers->currentText());

    if(!this->applicationSettingsProvider->SaveApplicationSettings(applicationSettings))
    {
        QMessageBox::critical(nullptr, "Application Settings", "Failed to save your application settings!");
    }
    else
    {
       SessionSettings::HasUpdatedApplicationSettings = true;
       this->close();
    }
}

void FrmApplicationSettings::ResetApplicationSettings()
{
    QMessageBox::StandardButton confirmDialogue = QMessageBox::question(this, "Reset Application Settings", "Resetting your application settings will remove all preferences and your music library.\n\nAre you sure?", QMessageBox::Yes | QMessageBox::No);

    if (confirmDialogue == QMessageBox::Yes)
    {
        if(!this->applicationSettingsProvider->ResetApplicationSettings())
        {
            QMessageBox::critical(nullptr, "Reset Application Settings", "Failed to reset your application settings!");
        }
        else
        {
            SessionSettings::HasUpdatedApplicationSettings = true;
            this->close();
        }
    }
}
