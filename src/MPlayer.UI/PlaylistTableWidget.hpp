// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef PLAYLIST_TABLEWIDGET_HPP
#define PLAYLIST_TABLEWIDGET_HPP

#include <QMimeData>
#include <QHeaderView>
#include <QDropEvent>
#include <QDragMoveEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QApplication>
#include <QGroupBox>
#include <QMenu>

#include "MPlayer.Domain/Playlist/PlaylistTrackReader.hpp"
#include "MPlayer.Domain/Playlist/PlaylistTrackWriter.hpp"
#include "MPlayer.Domain/Settings/ApplicationSettingsProvider.hpp"
#include "MPlayer.Domain/Player/PlayerStatusReader.hpp"
#include "MPlayer.Domain/Player/PlayerService.hpp"
#include "MPlayer.Domain/Library/LibraryTrackReader.hpp"
#include "MPlayer.Domain/Library/LibraryTrackWriter.hpp"
#include "MPlayer.Domain/Track/TrackGridReader.hpp"
#include "MPlayer.Domain/Track/TrackGridWriter.hpp"
#include "MPlayer.Domain/Window/WindowManager.hpp"
#include "MPlayer.Entities/Settings/SessionSettings.hpp"
#include "MPlayer.Entities/Track/TrackColumn.hpp"

#include "FrmTrackProperties.hpp"

using namespace MPlayer::Domain::Window;

class PlaylistTableWidget : public QTableWidget
{
    Q_OBJECT
    public:
        explicit PlaylistTableWidget(QWidget *parent = nullptr);

        void AddPlaylistTracks(const QStringList &selectedPlaylistFilepaths);
        void AddSelectedPlaylistTracks();
        void AddSelectedPlaylistTracksByAlbum();
        void AutoLoadPlaylistTracks();
        void CalculatePlaylistTrackStatistics();
        void OpenPlaylist(const bool &appendToPlaylist);
        void SavePlaylist();
        void SetPlaylistControls(std::shared_ptr<QLabel> statusLabel, QGroupBox *ctrlPlaylistTracksGroup);
        void SetPlaylistDependencies(std::shared_ptr<IPlayerService> playerService,
                                     std::shared_ptr<IWindowManager> windowManager,
                                     std::shared_ptr<ILibraryTrackReader> libraryTrackReader,
                                     std::shared_ptr<ILibraryTrackWriter> libraryTrackWriter);
    public slots:
        void ClearPlaylist();
        void dragEnterEvent(QDragEnterEvent *event);
        void dropEvent(QDropEvent *event);
        void dragMoveEvent(QDragMoveEvent *event);
        void SelectPlaylistPlayingTrack();
        void ShowPlaylistContextMenu(const QPoint &pos);

    private:
        void SetPlaylistAttributes();
        void SetPlaylistHeaderItems();
        void ViewPropertiesOfSelectedPlaylistTracks();

        bool addTrackAction;
        QString defaultAlbumDirectory;

        std::shared_ptr<ILibraryTrackReader> libraryTrackReader;
        std::shared_ptr<ILibraryTrackWriter> libraryTrackWriter;
        std::shared_ptr<IPlaylistTrackReader> playlistTrackReader;
        std::shared_ptr<IPlaylistTrackWriter> playlistTrackWriter;
        std::shared_ptr<ITrackGridReader> trackGridReader;
        std::shared_ptr<ITrackGridWriter> trackGridWriter;
        std::shared_ptr<IPlayerStatusReader> playerStatusReader;
        std::shared_ptr<IApplicationSettingsProvider> applicationSettingsProvider;
        std::shared_ptr<IPlayerService> playerService;
        std::shared_ptr<IWindowManager> windowManager;
        std::shared_ptr<QLabel> statusLabel;

        QGroupBox *ctrlPlaylistTracksGroup;
};

#endif // PLAYLIST_TABLEWIDGET_HPP
