// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef FRM_APPLICATION_SETTINGS_HPP
#define FRM_APPLICATION_SETTINGS_HPP

#include <QFileDialog>
#include <QMessageBox>
#include <memory>

#include "MPlayer.Domain/Settings/ThemeProvider.hpp"
#include "MPlayer.Domain/Settings/ApplicationSettingsProvider.hpp"
#include "MPlayer.Entities/Settings/SessionSettings.hpp"

#include "ui_FrmApplicationSettings.h"

using namespace MPlayer::Domain::Settings;

class FrmApplicationSettings : public QWidget
{
    Q_OBJECT
    public:
        explicit FrmApplicationSettings(QWidget *parent = nullptr);

    public slots:
        void ChkLoadPlaylist_StateChange();
        void ResetApplicationSettings();
        void SaveApplicationSettings();
        void SelectLibraryLocation();
        void SelectPlaylistFile();

    private:
        void SetupFormControls();
        void SetupFormControlSlots();

        Ui::FrmApplicationSettings *ui;
        std::shared_ptr<IApplicationSettingsProvider> applicationSettingsProvider;
};

#endif // FRM_APPLICATION_SETTINGS_HPP
