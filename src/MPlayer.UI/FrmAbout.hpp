// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef FRMABOUT_HPP
#define FRMABOUT_HPP

#include <QDesktopWidget>
#include <memory>

#include "MPlayer.Domain/Settings/ThemeProvider.hpp"
#include "MPlayer.Domain/Settings/ApplicationSettingsProvider.hpp"
#include "ui_FrmAbout.h"

using namespace MPlayer::Domain::Settings;

class FrmAbout : public QWidget
{
    Q_OBJECT
    public:
        explicit FrmAbout(QWidget *parent = nullptr);

    private:
        void SetupFormControls();

        Ui::FrmAbout *ui;
};

#endif // FRMABOUT_HPP
