// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "FrmAbout.hpp"

FrmAbout::FrmAbout(QWidget *parent) : QWidget(parent),
    ui(new Ui::FrmAbout)
{
    ui->setupUi(this); // Set up widget ui

    SetupFormControls();
}

void FrmAbout::SetupFormControls()
{
    std::unique_ptr<IThemeProvider> themeProvider = std::unique_ptr<IThemeProvider>(new ThemeProvider());
    themeProvider->ApplyThemingToForm(this, "FrmAbout");

    std::shared_ptr<IApplicationSettingsProvider> applicationSettingsProvider = std::unique_ptr<IApplicationSettingsProvider>(new ApplicationSettingsProvider());
    QWidget::setWindowIcon(QIcon((applicationSettingsProvider->GetProgramIconFilePath())));

    setWindowFlags((windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint); // Set the widget to have no form buttons
    setFixedSize(width(), height()); // Set widget size
}
