// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "FrmTrackSearch.hpp"

FrmTrackSearch::FrmTrackSearch(std::shared_ptr<IWindowManager> windowManager,
                               QVector<PlayerTrack> queriedTracks,
                               PlayerTab playerTab,
                               const QString &searchQuery,
                               std::shared_ptr<IPlayerService> playerService,
                               QTableWidget *playlistTableWidget,
                               QGroupBox *playlistGroupBox,
                               QWidget *parent) : QWidget(parent),
    ui(new Ui::FrmTrackSearch)
{
    ui->setupUi(this);

    this->selectedTracks = queriedTracks;
    this->currentPlayerTab = playerTab;
    this->playerService = playerService;
    this->playlistTableWidget = playlistTableWidget;
    this->playlistGroupBox = playlistGroupBox;
    this->windowManager = windowManager;
    this->trackGridReader = std::unique_ptr<ITrackGridReader>(new TrackGridReader());
    this->trackGridWriter = std::unique_ptr<ITrackGridWriter>(new TrackGridWriter());

    this->setWindowTitle(QString("Search Results - %1").arg(searchQuery));

    SetupFormControlSlots();
    SetupFormControls();
    AddTracksToSearchResults();
    ApplyTheming();
}

void FrmTrackSearch::SetupFormControlSlots()
{
    connect(ui->ctrlSearchTracks, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(DoubleClickSearchResults(QTableWidgetItem*)));
    connect(ui->ctrlSearchTracks, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(ShowSearchContextMenu(const QPoint&)));
}

void FrmTrackSearch::SetupFormControls()
{
    std::unique_ptr<IApplicationSettingsProvider> applicationSettingsProvider = std::unique_ptr<IApplicationSettingsProvider>(new ApplicationSettingsProvider());
    QWidget::setWindowIcon(QIcon(applicationSettingsProvider->GetProgramIconFilePath()));

    ui->ctrlSearchTracks->setEditTriggers(QAbstractItemView::NoEditTriggers); // Don't allow editing of the track container
}

void FrmTrackSearch::ShowSearchContextMenu(const QPoint &pos) // Slot for library context menu
{
    QPoint globalPos = ui->ctrlSearchTracks->mapToGlobal(pos);

    QMenu searchMenu;
    searchMenu.addAction("Play Track");

    if(this->currentPlayerTab == LIBRARY)
    {
        searchMenu.addAction("Add Track to Playlist");
    }

    searchMenu.addSeparator();
    searchMenu.addAction("View Track Properties");

    if(ui->ctrlSearchTracks->rowCount() == 0)
    {
        searchMenu.setEnabled(false);
    }
    else if(ui->ctrlSearchTracks->currentRow() < 0)
    {
        ui->ctrlSearchTracks->selectRow(0);
    }

    QAction *selectedMenuAction = searchMenu.exec(globalPos);

    if (selectedMenuAction) // Menu selected
    {
        int rowId = ui->ctrlSearchTracks->currentRow();
        QString selectedTrackFilepath = ui->ctrlSearchTracks->item(rowId, 9)->text();

        if(selectedMenuAction->text() == "Play Track")
        {
            SelectTrackForPlaying(selectedTrackFilepath);
        }
        else if(selectedMenuAction->text() == "Add Track to Playlist")
        {
            QStringList selectedLibraryTrackList = this->trackGridReader->GetSelectedTrackFilepathsAsList(ui->ctrlSearchTracks);
            AddTracksToPlaylist(selectedLibraryTrackList);
        }
        else if(selectedMenuAction->text() == "View Track Properties")
        {
            ViewPropertiesOfSelectedSearchedTracks();
        }
    }
}

void FrmTrackSearch::AddTracksToPlaylist(const QStringList &selectedPlaylistFilepaths)
{
    std::unique_ptr<IPlaylistTrackReader> playlistTrackReader = std::unique_ptr<IPlaylistTrackReader>(new PlaylistTrackReader());
    QVector<PlayerTrack> playlistTracks = playlistTrackReader->GetPlaylistTracks(selectedPlaylistFilepaths);

    for(int i = 0; i < playlistTracks.length(); i++)
    {
        this->trackGridWriter->AddTrackToTableGrid(i, false, playlistTracks[i], this->playlistTableWidget);
        this->playlistGroupBox->setTitle("  Playlist (" + QString::number(this->playlistTableWidget->rowCount()) + ")");
        QApplication::processEvents();
    }

    this->playlistTableWidget->resizeColumnsToContents();
}

void FrmTrackSearch::ViewPropertiesOfSelectedSearchedTracks()
{
    QStringList selectedSearchTrackList = this->trackGridReader->GetSelectedTrackFilepathsAsList(ui->ctrlSearchTracks);

    this->windowManager->ShowProgramWindow(new FrmTrackProperties(selectedSearchTrackList, this->playerService));
}

void FrmTrackSearch::SelectTrackForPlaying(const QString &trackFilepath)
{
    if(!trackFilepath.isEmpty())
    {
        SessionSettings::SearchedPlayerTab = this->currentPlayerTab;
        SessionSettings::LastSearchedTrackFilePath = trackFilepath;
        SessionSettings::HasSearchedTrackFilePath = true;
        this->close();
    }
}

void FrmTrackSearch::DoubleClickSearchResults(QTableWidgetItem *selectedRow)
{
    if(selectedRow->row() >= 0)
    {
        SelectTrackForPlaying(ui->ctrlSearchTracks->item(selectedRow->row(), 9)->text());
    }
}

void FrmTrackSearch::AddTracksToSearchResults()
{
    while (ui->ctrlSearchTracks->rowCount() > 0)
    {
        ui->ctrlSearchTracks->removeRow(0);
    }

    ui->ctrlSearchTracks->setRowCount(0);

    if(this->selectedTracks.count() > 0)
    {
        bool isPlayingTrack = false;

        for(int i = 0; i < this->selectedTracks.length(); i++)
        {
            isPlayingTrack = this->playerService->GetTrackFilepath() == this->selectedTracks[i].GetTrackFilepath();
            this->trackGridWriter->AddTrackToTableGrid(i, isPlayingTrack, this->selectedTracks[i], ui->ctrlSearchTracks);

            ui->gboSearchResults->setTitle("Tracks (" + QString::number(ui->ctrlSearchTracks->rowCount()) + ")");
            QApplication::processEvents();
        }

        ui->ctrlSearchTracks->setSortingEnabled(true);
        ui->ctrlSearchTracks->resizeColumnsToContents();
        ui->ctrlSearchTracks->sortByColumn(TRACK_ARTIST, Qt::AscendingOrder);
        ui->ctrlSearchTracks->setSortingEnabled(false);
    }
}

void FrmTrackSearch::ApplyTheming()
{
    std::unique_ptr<IThemeProvider> themeProvider = std::unique_ptr<IThemeProvider>(new ThemeProvider());
    themeProvider->ApplyThemingToForm(this, "FrmTrackSearch");
}
