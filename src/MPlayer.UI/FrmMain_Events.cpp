// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "FrmMain.hpp"

void FrmMain::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason)
    {
        case QSystemTrayIcon::DoubleClick:
            ItemShowHide_Click();
            break;
        case QSystemTrayIcon::Context:
            TrayIconContextMenu(QCursor::pos()); // Send position of the click
            break;
        default:
            break;
    }
}

void FrmMain::AddSelectedPlaylistTracksByAlbum()
{
    ui->btnPlaylistSelectPlaying->setText("Stop");
    ui->ctrlPlaylistTracks->AddSelectedPlaylistTracksByAlbum();
    ui->btnPlaylistSelectPlaying->setText("Select Playing");
}

void FrmMain::AddSelectedPlaylistTracks()
{
    ui->btnPlaylistSelectPlaying->setText("Stop");
    ui->ctrlPlaylistTracks->AddSelectedPlaylistTracks();
    ui->btnPlaylistSelectPlaying->setText("Select Playing");
}

void FrmMain::BtnClearPlaylist_Click()
{
    ui->ctrlPlaylistTracks->ClearPlaylist();
}

void FrmMain::BtnLibrarySearch_Click()
{
    InitiateTrackSearch(ui->btnLibrarySearch, ui->cboLibrarySearch->currentText(), LIBRARY);
    AddHistoryToSearchComboBox(ui->cboLibrarySearch, ui->cboLibrarySearch->currentText());
}

void FrmMain::BtnNext_Click()
{
    StopTrack();
    NextTrack();
}

void FrmMain::BtnOpenPlaylist_Click()
{
    ui->btnPlaylistSelectPlaying->setText("Stop");
    ui->ctrlPlaylistTracks->OpenPlaylist(ui->chkAppendToPlaylist->isChecked());
    ui->btnPlaylistSelectPlaying->setText("Select Playing");
}

void FrmMain::BtnPlaylistSearch_Click()
{
    InitiateTrackSearch(ui->btnPlaylistSearch, ui->cboPlaylistSearch->currentText(), PLAYLIST);
    AddHistoryToSearchComboBox(ui->cboPlaylistSearch, ui->cboPlaylistSearch->currentText());
}

void FrmMain::BtnPlaylistSelectPlaying_Click()
{
    ui->ctrlPlaylistTracks->SelectPlaylistPlayingTrack();
}

void FrmMain::BtnPrevious_Click()
{
    time_t currentLengthSeconds(this->playerService->GetCurrentDuration());
    bool restartTrack = currentLengthSeconds >= 3;

    StopTrack();
    PreviousTrack(restartTrack);
}

void FrmMain::BtnSavePlaylist_Click()
{
    ui->ctrlPlaylistTracks->SavePlaylist();
}

void FrmMain::BtnStop_Click()
{
    StopTrack();
    this->setWindowTitle("M++ Player");
}

void FrmMain::CalculateTrackStatistics()
{
    PlayerTab currentTab = static_cast<PlayerTab>(ui->ctrlTab->currentIndex());
    QTableWidget *selectedTableWidget = currentTab == PLAYLIST ? ui->ctrlPlaylistTracks : ui->ctrlLibraryTracks;
    QStringList currentTrackFilepaths = this->trackGridReader->GetTrackFilepathsAsList(selectedTableWidget);

    this->statusLabel->setText(this->playerStatusReader->GetTabStatusStringForPlayerTab(currentTrackFilepaths, currentTab));
}

void FrmMain::CloseApplication()
{
    this->close();
}

void FrmMain::closeEvent(QCloseEvent*)
{
    this->playlistChecker.stop();
    this->fileChangeChecker.stop();
    this->durationTimer.stop();
    this->searchChecker.stop();
    this->fileChangeChecker.stop();
    this->libraryGenerationChecker.stop();

    SaveWindowSettings();

    this->playerService->StopTrack();
    this->playerService->PausePlayerService(1500);
    this->windowManager->CloseExistingProgramWindows();

    delete ui;

    qApp->quit();
}

void FrmMain::DoubleClickList(QTableWidgetItem *item)
{
    int rowId = item->row();
    QString selectedTrackFilepath = item->tableWidget()->item(rowId, TRACK_FILEPATH)->text();
    this->playerMode = item->tableWidget() == ui->ctrlLibraryTracks ? LIBRARY : PLAYLIST;

    if(OpenTrack(selectedTrackFilepath))
    {
        this->trackGridWriter->SetPlayingStatusOfTrackForTableWidget(rowId, item->tableWidget());
    }

    SetupPlayerTrackListForTrackShuffler(item->tableWidget(), selectedTrackFilepath);
}

void FrmMain::DurationChecker()
{
    if(!this->playerService->IsPlaying() && !this->playerService->IsPaused() && !this->playerService->IsPlayerServiceRunning())
    {
        StopTrack();
        NextTrack();
    }
    else if(this->playerService->IsPlaying())
    {
        QString currentDurationString = this->playerService->GetCurrentDurationString();
        QString totalDurationString = this->playerService->GetTotalDurationString();

        ui->lblDuration->setText(currentDurationString + "/" + totalDurationString);
        ui->ctrlPosition->setMaximum(static_cast<int>(this->playerService->GetTotalDuration()));
        ui->ctrlPosition->setValue(static_cast<int>(this->playerService->GetCurrentDuration()));
    }
}

bool FrmMain::eventFilter(QObject *object, QEvent *event)
{
    if(object == ui->ctrlAlbumArt && event->type() == QEvent::MouseButtonDblClick)
    {
        ItemViewTrackProperties_Click();
    }
    return QWidget::eventFilter(object, event);
}

void FrmMain::ItemAboutMPlayer_Click()
{
    this->windowManager->ShowProgramWindow(new FrmAbout());
}

void FrmMain::ItemChangeSettings_Click()
{
    this->windowManager->ShowProgramWindow(new FrmApplicationSettings());
}

void FrmMain::ItemGenerateLibrary_Click()
{
    if(!this->applicationSettingsProvider->GetLibraryPath().isEmpty() && !this->applicationSettingsProvider->GetLibraryMusicPath().isEmpty())
    {
        this->libraryGenerator->RunLibraryService();
        this->libraryGenerationChecker.start(1000);
    }
    else
    {
        QMessageBox::critical(this, "Library Generation", "Please select a library path in your settings before proceeding.");
    }
}

void FrmMain::ItemRepeat_Click()
{
    if(this->playerService->IsPlayerServiceRunning() && ui->itemRepeat->isChecked())
    {
        ui->itemShuffle->setChecked(false);
        ui->itemShuffleAllLibraryTracks->setChecked(false);
        ui->itemRepeat->setChecked(true);
        this->repeatFlag = true;
        this->shuffleFlag = false;
        return;
    }

    ResetPlayModeStatus();
}

void FrmMain::ItemShowHide_Click()
{
    if(this->isHidden())
    {
        this->show();
        this->trayIcon->setToolTip("M++ Player (Double click to hide)");
    }
    else
    {
        this->hide();
        this->trayIcon->setToolTip("M++ Player (Double click to show)");
    }
}

void FrmMain::ItemShuffle_Click()
{
    if(ui->itemShuffle->isChecked())
    {
        ShuffleTracks(SHUFFLE);
        return;
    }

    ResetPlayModeStatus();
}

void FrmMain::ItemShuffleAllLibraryTracks_Click()
{
    if(this->playerMode == LIBRARY && ui->itemShuffleAllLibraryTracks->isChecked())
    {
        ShuffleTracks(SHUFFLE_ALL_LIBRARY_TRACKS);
        return;
    }

    ResetPlayModeStatus();
}

void FrmMain::ItemViewTrackProperties_Click()
{
    if(this->playerService->IsPlayerServiceRunning() && !this->playerService->GetTrackFilepath().isEmpty())
    {
        ViewPropertiesOfSelectedLibraryTracks();
    }
}

void FrmMain::ItemViewWebsite_Click()
{
    QDesktopServices::openUrl(QUrl("https://gitlab.com/mjsware/m-player"));
}

void FrmMain::keyPressEvent(QKeyEvent* pe)
{
    if(pe->key() == Qt::Key_Return)
    {
        switch(ui->ctrlTab->currentIndex())
        {
            case LIBRARY:
                BtnLibrarySearch_Click();
                break;
            case PLAYLIST:
                BtnPlaylistSearch_Click();
                break;
        }
    }
}

void FrmMain::moveEvent(QMoveEvent*)
{
    SaveWindowSettings();
}

void FrmMain::MovePlaylistTrackDown()
{
    this->trackGridWriter->MoveTrackDown(ui->ctrlPlaylistTracks);

    SetupPlaylistTrackListForTrackShuffler();
}

void FrmMain::MovePlaylistTrackUp()
{
    this->trackGridWriter->MoveTrackUp(ui->ctrlPlaylistTracks);

    SetupPlaylistTrackListForTrackShuffler();
}

void FrmMain::OnLibraryAlbumItemChange()
{
    ListTracks(false);
}

void FrmMain::OnLibraryAlbumItemDoubleClick()
{
    if(ui->ctrlLibraryTracks->rowCount() > 0)
    {
        DoubleClickList(ui->ctrlLibraryTracks->item(0, 0));
    }
}

void FrmMain::OnLibraryArtistItemChange()
{
    ListAlbums();
    ListTracks(false);
}

void FrmMain::OnLibraryArtistItemDoubleClick()
{
    if(ui->lstAlbums->count() > 1) // Select first album
    {
        ui->lstAlbums->item(1)->setSelected(true);
        ui->lstAlbums->setCurrentRow(1);
        ui->lstAlbums->setFocus();

        if(ui->ctrlLibraryTracks->rowCount() > 0)
        {
            DoubleClickList(ui->ctrlLibraryTracks->item(0, 0));
        }
    }
}

void FrmMain::PlayPauseTrack()
{
    if(!this->playerService->IsPlaying())
    {
        return;
    }

    if(!this->playerService->IsPaused())
    {
        this->playerService->PauseTrack();
    }
    else
    {
        this->playerService->PlayTrack();
    }

    ui->btnPlayPause->setText(this->playerService->IsPaused() ? "Play" : "Pause");
    ui->itemPlayPause->setText(this->playerService->IsPaused() ? "Play" : "Pause");
}

void FrmMain::PlaySelectedSearchTrack()
{
    QString searchedFilePath = SessionSettings::LastSearchedTrackFilePath;

    if(SessionSettings::HasSearchedTrackFilePath && searchedFilePath.length() > 0)
    {
        QTableWidget *selectedTableWidget = GetSelectedTableWidgetByPlayerMode();

        this->playerMode = SessionSettings::SearchedPlayerTab;

        if(!OpenTrack(searchedFilePath))
        {
            return;
        }

        if(this->playerMode == LIBRARY)
        {
            ShowPlayingTrackFromLibrary();
        }
        else if(this->playerMode == PLAYLIST && SetPlayingStatusOfTrackForTableWidgetRow(searchedFilePath, ui->ctrlPlaylistTracks) != -1)
        {
            ui->ctrlPlaylistTracks->SelectPlaylistPlayingTrack();
        }

        SetupPlayerTrackListForTrackShuffler(selectedTableWidget, searchedFilePath);

        SessionSettings::HasSearchedTrackFilePath = false;
    }
}

void FrmMain::ResetPlayerInterface()
{
    SetPlayerInterface(DEFAULT, this->previousFormHeight, this->previousFormWidth, true, true);
}

void FrmMain::resizeEvent(QResizeEvent*)
{
    SaveWindowSettings();
}

void FrmMain::SearchTracksFromSelectedAlbum()
{
    if(ui->lstAlbums->selectedItems().count() > 0)
    {
        OpenSearchForm(LIBRARY, ui->lstAlbums->selectedItems().at(0)->text());
    }
}

void FrmMain::BtnLibrarySelectPlaying()
{
    ShowPlayingTrackFromLibrary();
}

void FrmMain::SetUpdatedApplicationSettings()
{
    if(SessionSettings::HasUpdatedApplicationSettings)
    {
        QString previousLibraryMusicPath = this->applicationSettingsProvider->GetLibraryMusicPath();
        ApplicationSettings applicationSettings = this->applicationSettingsProvider->GetApplicationSettings();

        if(applicationSettings.GetLibraryMusicPath() == "") // Reset library view
        {
            StopTrack();
            ResetPlayModeStatus();

            ui->lstArtists->clear();
            ui->lstAlbums->clear();
            ui->ctrlLibraryTracks->setRowCount(0);
            ui->ctrlArtistGroup->setTitle("  Artists (0)");
            ui->ctrlAlbumGroup->setTitle("  Albums (0)");
            ui->ctrlLibraryTracksGroup->setTitle("  Tracks (0)");

            CalculateTrackStatistics();

            this->libraryTrackReader->GetLatestLibraryTracks();
        }
        else if (applicationSettings.GetLibraryMusicPath() != previousLibraryMusicPath) // Regenerate library
        {
            StopTrack();
            ResetPlayModeStatus();
            ItemGenerateLibrary_Click();
        }

        SessionSettings::HasUpdatedApplicationSettings = false;
    }
}

void FrmMain::ShowAlbumContextMenu(const QPoint &pos)
{
    if(ui->lstAlbums->selectedItems().count() == 0)
    {
        return;
    }

    QPoint globalPos = ui->lstAlbums->mapToGlobal(pos);

    QMenu albumMenu;

    QAction *viewOtherTracksAction = new QAction("View Tracks From Selected Album (If any)");
    connect(viewOtherTracksAction, SIGNAL(triggered()), this, SLOT(SearchTracksFromSelectedAlbum()));
    viewOtherTracksAction->setEnabled(ui->lstAlbums->selectedItems().count() > 0 && ui->lstAlbums->selectedItems().at(0)->text() != "All Albums");
    albumMenu.addAction(viewOtherTracksAction);

    albumMenu.exec(globalPos);
}

void FrmMain::ShowLibraryContextMenu(const QPoint &pos) // Slot for library context menu
{
    QPoint globalPos = ui->ctrlLibraryTracks->mapToGlobal(pos);
    QMenu libraryMenu;

    libraryMenu.addAction("Play Track");
    libraryMenu.addAction("Add Track(s) to Playlist");
    libraryMenu.addSeparator();
    libraryMenu.addAction("Remove Track(s) from Library");
    libraryMenu.addSeparator();

    QMenu *copyClipboardSubMenu = libraryMenu.addMenu("Copy (Clipboard)");
    QList<QAction*> copyClipboardActions = QList<QAction*> { new QAction("Artist"), new QAction("Album"), new QAction("Artist - Title"), new QAction("Title"),
                                                             new QAction("Genre"),  new QAction("Year"),  new QAction("Filepath") };

    copyClipboardSubMenu->addActions(copyClipboardActions);

    libraryMenu.addAction("Select All Tracks");
    libraryMenu.addSeparator();
    libraryMenu.addAction("View Track Properties");

    if(ui->ctrlLibraryTracks->rowCount() == 0)
    {
        libraryMenu.setEnabled(false);
    }
    else if(ui->ctrlLibraryTracks->currentRow() < 0)
    {
        ui->ctrlLibraryTracks->selectRow(0);
    }

    QAction *selectedMenuAction = libraryMenu.exec(globalPos);

    if (selectedMenuAction) // Menu selected
    {
        int rowId = ui->ctrlLibraryTracks->currentRow();
        QString selectedLibraryTrackFilepath = ui->ctrlLibraryTracks->item(rowId, TRACK_FILEPATH)->text();

        if(selectedMenuAction->text() == "Play Track")
        {
            DoubleClickList(ui->ctrlLibraryTracks->item(rowId, 0));
        }
        else if(selectedMenuAction->text() == "Add Track(s) to Playlist")
        {
            QStringList selectedLibraryTrackList = this->trackGridReader->GetSelectedTrackFilepathsAsList(ui->ctrlLibraryTracks);
            AddTracksToPlaylistFromLibrary(selectedLibraryTrackList);
        }
        else if(selectedMenuAction->text() == "Remove Track(s) from Library")
        {
            QMessageBox::StandardButton removalQuestion = QMessageBox::question(this,
                                                                                "Remove library track(s)",
                                                                                "Removing a track from the library will not result in it being deleted."
                                                                                "\n\nAre you sure?",
                                                                                QMessageBox::Yes | QMessageBox::No);
            if (removalQuestion == QMessageBox::Yes)
            {
                QStringList libraryTracksToRemove = this->trackGridReader->GetSelectedTrackFilepathsAsList(ui->ctrlLibraryTracks);

                for(int i = 0; i < libraryTracksToRemove.count(); i++)
                {
                    this->libraryTrackWriter->RemoveLibraryTrack(libraryTracksToRemove[i]);
                }

                ui->lstAlbums->clear();
                ui->ctrlLibraryTracks->setRowCount(0);
                ListArtists();
            }
        }
        else if(selectedMenuAction->text() == "Select All Tracks")
        {
            ui->ctrlLibraryTracks->selectAll();
        }
        else if(selectedMenuAction->text() == "View Track Properties")
        {
            ViewPropertiesOfSelectedLibraryTracks();
        }
        else if(this->libraryTrackReader->IsLibraryTrackByFilepathCached(selectedLibraryTrackFilepath))
        {
            QClipboard *userClipboard = QApplication::clipboard();
            PlayerTrack propertiesTrack = this->libraryTrackReader->GetCachedLibraryTrackByFilepath(selectedLibraryTrackFilepath);
            userClipboard->setText(propertiesTrack.GetTrackAttributeByName(selectedMenuAction->text()), QClipboard::Clipboard);
        }
    }
}

void FrmMain::ShowLibraryPlaylistInterface()
{
    SetPlayerInterface(SHOW_LIBRARY_PLAYLIST, this->previousFormHeight, this->previousFormWidth, false, true);
}

void FrmMain::ShowPlayingTrackFromPlaylist()
{
    if(this->playerService->IsPlayerServiceRunning() && SessionSettings::LastPlaylistTrackPathRequested != "")
    {
        this->playerMode = PLAYLIST;

        UpdatePlayerToPlayingStatus(SessionSettings::LastPlaylistTrackPathRequested);
        SetPlayingStatusOfTrackForTableWidgetRow(SessionSettings::LastPlaylistTrackPathRequested, ui->ctrlPlaylistTracks);

        SessionSettings::LastPlaylistTrackPathRequested = "";
    }
}

void FrmMain::ShowStatusInterface()
{
    if(!isMaximized())
    {
        SetPlayerInterface(SHOW_STATUS, 870, 400, true, false);
        resize(QDesktopWidget().availableGeometry(this).size() * 0.1);
        setFixedSize(1047, 400);
        adjustSize();
    }
}

void FrmMain::ShowUpdatedTracks()
{
    if(SessionSettings::LastTrackPathEdited != "")
    {
        UpdateSavedLibraryTrack();
        UpdateSavedPlaylistTrack();

        SessionSettings::LastTrackPathEdited = "";
    }

    if(SessionSettings::HasDraggedPlaylistTracks) // Drag, drop files onto playlist
    {
        ui->ctrlPlaylistTracks->AddPlaylistTracks(SessionSettings::DraggedPlaylistFilepaths);
        SessionSettings::HasDraggedPlaylistTracks = false;
    }
}

void FrmMain::StopTrack()
{
    this->durationTimer.stop();
    this->playerService->StopTrack();
    this->playerService->PausePlayerService(1500);
    this->playingLibraryAlbum = "";

    CalculateTrackStatistics();

    ui->itemPlayPause->setEnabled(false);
    ui->itemStop->setEnabled(false);
    ui->itemNextTrack->setEnabled(false);
    ui->itemPreviousTrack->setEnabled(false);
    ui->ctrlPosition->setEnabled(false);
    ui->itemViewTrackProperties->setEnabled(false);
    ui->ctrlPosition->setValue(0);
    ui->lblDuration->setVisible(false);
    ui->lblStatus->setText("Stopped");
    ui->btnPlayPause->setEnabled(false);
    ui->btnStop->setEnabled(false);
    ui->btnPlayPause->setText("Play");
    ui->btnNext->setEnabled(false);
    ui->btnPrevious->setEnabled(false);

    this->trackGridWriter->ResetPlayingStatusOfTrackForTableWidget(ui->ctrlLibraryTracks);
    this->trackGridWriter->ResetPlayingStatusOfTrackForTableWidget(ui->ctrlPlaylistTracks);
}

void FrmMain::TabChange()
{
    CalculateTrackStatistics();
}

void FrmMain::UpdateDurationValue()
{
    this->playerService->SetPosition(ui->ctrlPosition->value());
}

void FrmMain::UpdateLibraryDisplay()
{
    if(!this->libraryGenerator->IsLibraryServiceRunning() && this->libraryGenerator->IsGenerated())
    {
        this->libraryTrackReader->GetLatestLibraryTracks();

        ui->lstArtists->clear();
        ui->lstAlbums->clear();
        ui->ctrlLibraryTracks->setRowCount(0);

        ListArtists();

        ui->ctrlAlbumGroup->setTitle("  Albums (" + QString::number(ui->lstAlbums->count()) + ")");
        ui->ctrlLibraryTracksGroup->setTitle("   Tracks (" + QString::number(ui->ctrlLibraryTracks->rowCount()) + ")");

        this->statusLabel->setText("Library generated...");
        this->libraryGenerationChecker.stop();
    }
    else
    {
        this->statusLabel->setText("Generating library (" + QString::number(this->libraryGenerator->GetLibraryPercentageGenerated(), 10, 0) + "%)...");
    }
}

void FrmMain::UpdateVolumeValue(const int &volume)
{
    ui->lblVolume->setText("Volume: " + QString::number(volume));
    this->playerService->SetVolume(volume);
}
