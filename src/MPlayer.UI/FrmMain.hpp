// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef FRMMAIN_HPP
#define FRMMAIN_HPP

#include <QTimer>
#include <QSystemTrayIcon>
#include <QMessageBox>
#include <memory>

#include "MPlayer.Domain/Track/TrackShuffler.hpp"
#include "MPlayer.Domain/Player/PlayerStatusReader.hpp"
#include "MPlayer.Domain/Track/TrackGridReader.hpp"
#include "MPlayer.Domain/Track/TrackGridWriter.hpp"
#include "MPlayer.Domain/Library/LibraryGenerator.hpp"
#include "MPlayer.Domain/Library/LibraryTrackReader.hpp"
#include "MPlayer.Domain/Library/LibraryTrackWriter.hpp"
#include "MPlayer.Domain/Player/PlayerService.hpp"
#include "MPlayer.Domain/Settings/ThemeProvider.hpp"
#include "MPlayer.Domain/Settings/ApplicationSettingsProvider.hpp"
#include "MPlayer.Domain/Window/WindowManager.hpp"
#include "MPlayer.Domain/Track/AlbumArtReader.hpp"
#include "MPlayer.Domain/Playlist/PlaylistTrackReader.hpp"
#include "MPlayer.Entities/Player/PlayerTab.hpp"
#include "MPlayer.Entities/Player/PlayerInterfaceStatus.hpp"
#include "MPlayer.Entities/Settings/SessionSettings.hpp"
#include "MPlayer.Entities/Track/PlayerTrack.hpp"
#include "MPlayer.Entities/Track/ShuffleMode.hpp"

#include "FrmAbout.hpp"
#include "FrmTrackSearch.hpp"
#include "FrmTrackProperties.hpp"
#include "FrmApplicationSettings.hpp"
#include "PlaylistTableWidget.hpp"
#include "ui_FrmMain.h"

class FrmMain : public QMainWindow
{
    Q_OBJECT
    public:
        explicit FrmMain(QWidget *parent = nullptr);

    private slots:
        void iconActivated(QSystemTrayIcon::ActivationReason);

    public slots:
        void AddSelectedPlaylistTracks();
        void AddSelectedPlaylistTracksByAlbum();
        void BtnClearPlaylist_Click();
        void BtnLibrarySearch_Click();
        void BtnLibrarySelectPlaying();
        void BtnNext_Click();
        void BtnOpenPlaylist_Click();
        void BtnPlaylistSearch_Click();
        void BtnPlaylistSelectPlaying_Click();
        void BtnPrevious_Click();
        void BtnSavePlaylist_Click();
        void BtnStop_Click();
        void CalculateTrackStatistics();
        void CloseApplication();
        void closeEvent(QCloseEvent*);
        void DoubleClickList(QTableWidgetItem *item);
        void DurationChecker();
        bool eventFilter(QObject *, QEvent *);
        void ItemAboutMPlayer_Click();
        void ItemChangeSettings_Click();
        void ItemGenerateLibrary_Click();
        void ItemRepeat_Click();
        void ItemShowHide_Click();
        void ItemShuffle_Click();
        void ItemShuffleAllLibraryTracks_Click();
        void ItemViewTrackProperties_Click();
        void ItemViewWebsite_Click();
        void keyPressEvent(QKeyEvent*);
        void moveEvent(QMoveEvent*);
        void MovePlaylistTrackDown();
        void MovePlaylistTrackUp();
        void OnLibraryAlbumItemChange();
        void OnLibraryAlbumItemDoubleClick();
        void OnLibraryArtistItemChange();
        void OnLibraryArtistItemDoubleClick();
        void PlayPauseTrack();
        void PlaySelectedSearchTrack();
        void ResetPlayerInterface();
        void resizeEvent(QResizeEvent*);
        void SearchTracksFromSelectedAlbum();
        void SetUpdatedApplicationSettings();
        void ShowAlbumContextMenu(const QPoint &pos);
        void ShowLibraryContextMenu(const QPoint &pos);
        void ShowLibraryPlaylistInterface();
        void ShowPlayingTrackFromPlaylist();
        void ShowStatusInterface();
        void ShowUpdatedTracks();
        void StopTrack();
        void TabChange();
        void UpdateDurationValue();
        void UpdateLibraryDisplay();
        void UpdateVolumeValue(const int &volume);

    private:
        void AddHistoryToSearchComboBox(QComboBox *comboBox, const QString &searchQuery);
        void AddTracksToPlaylistFromLibrary(const QStringList &trackFilepaths);
        PlayerTrack GetAvailablePlayerTrack(const QString &selectedTrackFilepath);
        int GetCurrentRowIdOfSelectedTableWidget();
        QTableWidget* GetSelectedTableWidgetByPlayerMode();
        void InitiateTrackSearch(QPushButton *tabSearchButton, const QString &searchQuery, const PlayerTab &playerTab);
        void ListAlbums();
        void ListArtists();
        void ListTracks(const bool &selectLibraryTrack);
        void LoadWindowSettings();
        void NextTrack();
        void OpenSearchForm(const PlayerTab &playerTab, const QString &searchQuery);
        bool OpenTrack(QString &selectedTrackFilepath);
        void PreviousTrack(const bool &restartTrack);
        void ResetPlayModeStatus();
        void SaveWindowSettings();
        void SelectShuffledPlayingTrack(const QString &selectedTrackFilepath, QTableWidget *selectedTableWidget);
        void SetPlayerInterface(const PlayerInterfaceStatus &interfaceStatus,
                                const int &mininumHeight,
                                const int &mininumWidth,
                                const bool &showStatusGroup,
                                const bool &showTabControl);
        int SetPlayingStatusOfTrackForTableWidgetRow(const QString &selectedTrackFilepath, QTableWidget *selectedTableWidget);
        void SetupFormControls();
        void SetupFormControlSlots();
        void SetupFormDependencies();
        void SetupPlayerTrackListForTrackShuffler(QTableWidget *selectedTableWidget, const QString &selectedTrackFilepath);
        void SetupPlaylistTrackListForTrackShuffler();
        void ShowAlbumArt(QString selectedFilePath = "");
        void ShowApplicationTrayIcon();
        void ShowPlayingTrackFromLibrary(QString selectedTrackFilepath = "");
        void ShowTrackIconBalloon(PlayerTrack &currentTrack);
        void ShuffleTracks(ShuffleMode shuffleMode);
        void StartTimersForLoad();
        void TrayIconContextMenu(QPoint point);
        void UpdatePlayerToPlayingStatus(QString &selectedTrackFilepath);
        void UpdateSavedLibraryTrack();
        void UpdateSavedPlaylistTrack();
        void ViewPropertiesOfSelectedLibraryTracks();

        Ui::FrmMain *ui;
        std::shared_ptr<IWindowManager> windowManager;
        std::shared_ptr<ILibraryTrackReader> libraryTrackReader;
        std::shared_ptr<ILibraryTrackWriter> libraryTrackWriter;
        std::shared_ptr<ILibraryGenerator> libraryGenerator;
        std::shared_ptr<IPlaylistTrackReader> playlistTrackReader;
        std::shared_ptr<IPlayerService> playerService;
        std::shared_ptr<ITrackShuffler> trackShuffler;
        std::shared_ptr<IApplicationSettingsProvider> applicationSettingsProvider;
        std::shared_ptr<IPlayerStatusReader> playerStatusReader;
        std::shared_ptr<IAlbumArtReader> albumArtReader;
        std::shared_ptr<ITrackGridReader> trackGridReader;
        std::shared_ptr<ITrackGridWriter> trackGridWriter;
        std::shared_ptr<QSystemTrayIcon> trayIcon;
        std::shared_ptr<QGraphicsScene> albumArtScene;
        std::shared_ptr<QLabel> statusLabel;

        QString selectedLibraryArtist;
        QString selectedLibraryAlbum;
        QString playingLibraryAlbum;
        QTimer durationTimer; // Duration check timer
        QTimer searchChecker; // Search results checker
        QTimer fileChangeChecker; // Media information checker
        QTimer libraryGenerationChecker; // Library generation
        QTimer playlistChecker;
        QTimer settingsChecker;
        bool repeatFlag = false;
        bool shuffleFlag = false;
        bool selectLibraryTrack = false;
        int interfaceStatus = PlayerInterfaceStatus::DEFAULT;
        int playerMode = LIBRARY; // 0 = Library, 1 = Playlist
        int previousFormHeight = 0;
        int previousFormWidth = 0;
};

#endif // FRMMAIN_HPP
