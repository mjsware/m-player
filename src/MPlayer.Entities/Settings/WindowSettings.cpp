// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "WindowSettings.hpp"

MPlayer::Entities::Settings::WindowSettings::WindowSettings() {

}

MPlayer::Entities::Settings::WindowSettings::WindowSettings(const QByteArray &formGeometry,
                                                            const QByteArray &formSaveState,
                                                            const bool &formMaximised,
                                                            const int &interfaceStatus,
                                                            const QPoint &formPosition,
                                                            const QSize &formSize)
{
    this->formGeometry = formGeometry;
    this->formSaveState = formSaveState;
    this->formMaximised = formMaximised;
    this->interfaceStatus = interfaceStatus;
    this->formPosition = formPosition;
    this->formSize = formSize;
}

QByteArray MPlayer::Entities::Settings::WindowSettings::GetFormGeometry()
{
    return this->formGeometry;
}

QByteArray MPlayer::Entities::Settings::WindowSettings::GetFormState()
{
    return this->formSaveState;
}

bool MPlayer::Entities::Settings::WindowSettings::IsFormMaximised()
{
    return this->formMaximised;
}

int MPlayer::Entities::Settings::WindowSettings::GetInterfaceStatus()
{
    return this->interfaceStatus;
}

QPoint MPlayer::Entities::Settings::WindowSettings::GetFormPosition()
{
    return this->formPosition;
}

QSize MPlayer::Entities::Settings::WindowSettings::GetFormSize()
{
    return this->formSize;
}
