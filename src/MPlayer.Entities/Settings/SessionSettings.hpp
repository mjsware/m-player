// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef SESSION_SETTINGS_HPP
#define SESSION_SETTINGS_HPP

#include "../Player/PlayerTab.hpp"

#include <QString>
#include <QStringList>

using namespace MPlayer::Entities::Player;

namespace MPlayer
{
    namespace Entities
    {
        namespace Settings
        {
            class SessionSettings
            {
                public:
                    static bool HasUpdatedApplicationSettings;

                    static QString LastTrackPathEdited;
                    static QString LastPlaylistTrackPathRequested;

                    static bool HasDraggedPlaylistTracks;
                    static QStringList DraggedPlaylistFilepaths;

                    static bool HasSearchedTrackFilePath;
                    static QString LastSearchedTrackFilePath;
                    static PlayerTab SearchedPlayerTab;
            };
        }
    }
}

#endif // SESSION_SETTINGS_HPP
