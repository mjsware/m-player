// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef APPLICATION_SETTINGS_HPP
#define APPLICATION_SETTINGS_HPP

#include <QString>
#include <QStandardPaths>

namespace MPlayer
{
    namespace Entities
    {
        namespace Settings
        {
            class ApplicationSettings
            {
                public:
                    ApplicationSettings();
                    bool GetAutoLoadPlaylist();
                    QString GetDefaultAudioDriverName();
                    QString GetDefaultThemeName();
                    float GetDefaultVolume();
                    bool GetHideOnStartup();
                    QString GetLibraryMusicPath();
                    QString GetLibraryPath();
                    QString GetMusicNoteFilePath();
                    QString GetPlaylistsDirectory();
                    QString GetPlaylistPath();
                    QString GetProgramIconFilePath();
                    QStringList GetProgramThemeList();
                    QString GetProgramThemePath();
                    QString GetSettingsDirectoryPath();
                    QString GetSettingsFilePath();
                    QString GetTopDirectoryPath();

                    void SetAutoLoadPlaylist(const bool &autoLoadPlaylist);
                    void SetDefaultAudioDriverName(const QString &defaultAudioDriverName);
                    void SetDefaultTheme(const QString &defaultThemeName);
                    void SetDefaultVolume(const float &defaultVolume);
                    void SetHideOnStartup(const bool &hideOnStartup);
                    void SetLibraryMusicPath(const QString &libraryMusicPath);
                    void SetPlaylistPath(const QString &selectedPlaylistPath);
                    void SetProgramThemes(const QStringList &programThemes);
                    void SetProgramThemePath(const QString &programThemePath);

                private:
                    bool hideOnStartup;
                    bool autoLoadPlaylist;
                    float defaultVolume;
                    QString selectedPlaylistPath;
                    QString programThemePath;
                    QString defaultThemeName;
                    QString libraryMusicPath;
                    QString settingsFilePath;
                    QString settingsDirectoryPath;
                    QString libraryPath;
                    QString homePath;
                    QString topDirectoryPath;
                    QString playlistsDirectoryPath;
                    QString musicNotePath;
                    QString programIconPath;
                    QStringList programThemes;
                    QString defaultAudioDriverName;
            };
        }
    }
}

#endif // APPLICATION_SETTINGS_HPP
