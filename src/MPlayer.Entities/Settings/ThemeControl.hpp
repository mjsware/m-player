// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef THEME_CONTROL_HPP
#define THEME_CONTROL_HPP

#include <QString>

namespace MPlayer
{
    namespace Entities
    {
        namespace Settings
        {
            class ThemeControl
            {
                public:
                    ThemeControl();
                    ThemeControl(const QString &controlName, const QString &controlThemeContent);

                    QString GetControlName();
                    QString GetControlThemeContent();

                private:
                    QString controlName;
                    QString controlThemeContent;
            };
        }
    }
}

#endif // THEME_CONTROL_HPP
