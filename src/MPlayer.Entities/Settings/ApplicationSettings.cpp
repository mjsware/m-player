// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "ApplicationSettings.hpp"

MPlayer::Entities::Settings::ApplicationSettings::ApplicationSettings::ApplicationSettings()
{
    // Set default paths
    this->homePath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    this->topDirectoryPath = this->homePath + "/.m-player";
    this->settingsDirectoryPath = this->topDirectoryPath + "/Settings";
    this->playlistsDirectoryPath = this->topDirectoryPath + "/Playlists";
    this->settingsFilePath = this->settingsDirectoryPath + "/Settings.xml";
    this->libraryPath = this->settingsDirectoryPath + "/Library.xml";
    this->programThemePath = "/usr/share/m-player/Themes/Themes.xml";
    this->musicNotePath = "/usr/share/pixmaps/M++PlayerMusicNote.png";
    this->programIconPath = "/usr/share/pixmaps/M++PlayerIcon.png";

    // Default settings
    this->hideOnStartup = false;
    this->autoLoadPlaylist = false;
    this->selectedPlaylistPath = "";
    this->defaultThemeName = "None";
    this->libraryMusicPath = "";
    this->defaultVolume = 50;
    this->defaultAudioDriverName = "alsa";
}

bool MPlayer::Entities::Settings::ApplicationSettings::GetHideOnStartup()
{
    return this->hideOnStartup;
}

bool MPlayer::Entities::Settings::ApplicationSettings::GetAutoLoadPlaylist()
{
    return this->autoLoadPlaylist;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetPlaylistPath()
{
    return this->selectedPlaylistPath;
}

float MPlayer::Entities::Settings::ApplicationSettings::GetDefaultVolume()
{
    return this->defaultVolume;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetTopDirectoryPath()
{
    return this->topDirectoryPath;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetSettingsDirectoryPath()
{
    return this->settingsDirectoryPath;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetSettingsFilePath()
{
    return this->settingsFilePath;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetProgramThemePath()
{
    return this->programThemePath;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetDefaultThemeName()
{
    return this->defaultThemeName;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetLibraryMusicPath()
{
    return this->libraryMusicPath;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetProgramIconFilePath()
{
    return this->programIconPath;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetMusicNoteFilePath()
{
    return this->musicNotePath;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetLibraryPath()
{
    return this->libraryPath;
}

QStringList MPlayer::Entities::Settings::ApplicationSettings::GetProgramThemeList()
{
    return this->programThemes;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetPlaylistsDirectory()
{
    return this->playlistsDirectoryPath;
}

QString MPlayer::Entities::Settings::ApplicationSettings::GetDefaultAudioDriverName()
{
    return this->defaultAudioDriverName;
}

void MPlayer::Entities::Settings::ApplicationSettings::SetHideOnStartup(const bool &hideOnStartup)
{
    this->hideOnStartup = hideOnStartup;
}

void MPlayer::Entities::Settings::ApplicationSettings::SetAutoLoadPlaylist(const bool &autoLoadPlaylist)
{
    this->autoLoadPlaylist = autoLoadPlaylist;
}

void MPlayer::Entities::Settings::ApplicationSettings::SetPlaylistPath(const QString &selectedPlaylistPath)
{
    this->selectedPlaylistPath = selectedPlaylistPath;
}

void MPlayer::Entities::Settings::ApplicationSettings::SetDefaultVolume(const float &defaultVolume)
{
    this->defaultVolume = defaultVolume;
}

void MPlayer::Entities::Settings::ApplicationSettings::SetProgramThemePath(const QString &programThemePath)
{
    this->programThemePath = programThemePath;
}

void MPlayer::Entities::Settings::ApplicationSettings::SetDefaultTheme(const QString &defaultThemeName)
{
    this->defaultThemeName = defaultThemeName;
}

void MPlayer::Entities::Settings::ApplicationSettings::SetDefaultAudioDriverName(const QString &defaultAudioDriverName)
{
    this->defaultAudioDriverName = defaultAudioDriverName;
}

void MPlayer::Entities::Settings::ApplicationSettings::SetLibraryMusicPath(const QString &libraryMusicPath)
{
    this->libraryMusicPath = libraryMusicPath;
}

void MPlayer::Entities::Settings::ApplicationSettings::SetProgramThemes(const QStringList &programThemes)
{
    this->programThemes = programThemes;
}
