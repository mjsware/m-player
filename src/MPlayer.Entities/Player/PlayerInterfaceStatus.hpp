// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef PLAYER_INTERFACE_STATUS_HPP
#define PLAYER_INTERFACE_STATUS_HPP

namespace MPlayer
{
    namespace Entities
    {
        namespace Player
        {
            enum PlayerInterfaceStatus
            {
                DEFAULT = 0,
                SHOW_STATUS = 1,
                SHOW_LIBRARY_PLAYLIST = 2
            };
        }
    }
}

#endif // PLAYER_INTERFACE_STATUS_HPP
