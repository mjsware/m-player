// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef PLAYER_TRACK_HPP
#define PLAYER_TRACK_HPP

#include <QString>
#include <QStringList>

namespace MPlayer
{
    namespace Entities
    {
        namespace Track
        {
            class PlayerTrack
            {
                public:
                    PlayerTrack();

                    PlayerTrack(const unsigned int &trackNumber,
                                const int &trackBitrate,
                                const unsigned int &trackYear,
                                const QString &trackTitle,
                                const QString &trackArtist,
                                const QString &trackAlbum,
                                const QString &trackGenre,
                                const QString &trackFilepath,
                                const QString &trackDuration,
                                const bool &trackPlaylistAdded,
                                const QString &trackShortDuration,
                                const qint64 &trackFilesize); // Constructor for all track properties

                    PlayerTrack(const QString &trackFilepath); // Constructor based on a track file path

                    QString GetTrackAlbum();
                    QString GetTrackArtist();
                    QString GetTrackArtistAndTitle();
                    QString GetTrackAttributeByName(const QString &trackAttribute);
                    int GetTrackBitrate();
                    QString GetTrackDuration();
                    QString GetTrackFilepath();
                    qint64 GetTrackFilesize();
                    QString GetTrackGenre();
                    unsigned int GetTrackNumber();
                    bool GetTrackPlaylistAddedStatus();
                    QString GetTrackShortDuration();
                    QString GetTrackTitle();
                    unsigned int GetTrackYear();

                    void SetTrackAlbum(const QString &trackAlbum);
                    void SetTrackArtist(const QString &trackArtist);
                    void SetTrackBitrate(const int &trackBitrate);
                    void SetTrackDuration(const QString &trackDuration);
                    void SetTrackFilepath(const QString &trackFilepath);
                    void SetTrackFilesize(const qint64 &trackFilesize);
                    void SetTrackGenre(const QString &trackGenre);
                    void SetTrackNumber(const unsigned int &trackNumber);
                    void SetTrackPlaylistAddedStatus(const bool &trackPlaylistAdded);
                    void SetTrackShortDuration(const QString &trackShortDuration);
                    void SetTrackTitle(const QString &trackTitle);
                    void SetTrackYear(const unsigned int &trackYear);

                private:
                    bool trackPlaylistAdded = false;
                    int trackBitrate = 0;
                    unsigned int trackNumber = 0;
                    unsigned int trackYear = 0;
                    QString trackTitle;
                    QString trackArtist;
                    QString trackAlbum;
                    QString trackGenre;
                    QString trackFilepath;
                    QString trackDuration;
                    QString trackShortDuration;
                    qint64 trackFilesize = 0;
            };
        }
    }
}

#endif // PLAYER_TRACK_HPP
