// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "PlayerTrack.hpp"

MPlayer::Entities::Track::PlayerTrack::PlayerTrack() {

}

MPlayer::Entities::Track::PlayerTrack::PlayerTrack(const unsigned int &trackNumber,
                                                   const int &trackBitrate,
                                                   const unsigned int &trackYear,
                                                   const QString &trackTitle,
                                                   const QString &trackArtist,
                                                   const QString &trackAlbum,
                                                   const QString &trackGenre,
                                                   const QString &trackFilepath,
                                                   const QString &trackDuration,
                                                   const bool &trackPlaylistAdded,
                                                   const QString &trackShortDuration,
                                                   const qint64 &trackFilesize)
{
    this->trackNumber = trackNumber;
    this->trackBitrate = trackBitrate;
    this->trackYear = trackYear;
    this->trackTitle = trackTitle;
    this->trackArtist = trackArtist;
    this->trackAlbum = trackAlbum;
    this->trackGenre = trackGenre;
    this->trackFilepath = trackFilepath;
    this->trackDuration = trackDuration;
    this->trackPlaylistAdded = trackPlaylistAdded;
    this->trackFilesize = trackFilesize;
    this->trackShortDuration = trackShortDuration;
}

MPlayer::Entities::Track::PlayerTrack::PlayerTrack(const QString &trackFilepath)
{
    this->trackFilepath = trackFilepath;
}

bool MPlayer::Entities::Track::PlayerTrack::GetTrackPlaylistAddedStatus()
{
    return this->trackPlaylistAdded;
}

unsigned int MPlayer::Entities::Track::PlayerTrack::GetTrackNumber()
{
    return this->trackNumber;
}

unsigned int MPlayer::Entities::Track::PlayerTrack::GetTrackYear()
{
    return this->trackYear;
}

int MPlayer::Entities::Track::PlayerTrack::GetTrackBitrate()
{
    return this->trackBitrate;
}

QString MPlayer::Entities::Track::PlayerTrack::GetTrackTitle()
{
    return this->trackTitle;
}

QString MPlayer::Entities::Track::PlayerTrack::GetTrackArtist()
{
    return this->trackArtist;
}

QString MPlayer::Entities::Track::PlayerTrack::GetTrackAlbum()
{
    return this->trackAlbum;
}

QString MPlayer::Entities::Track::PlayerTrack::GetTrackGenre()
{
    return this->trackGenre;
}

QString MPlayer::Entities::Track::PlayerTrack::GetTrackFilepath()
{
    return this->trackFilepath;
}

QString MPlayer::Entities::Track::PlayerTrack::GetTrackDuration()
{
    return this->trackDuration;
}

QString MPlayer::Entities::Track::PlayerTrack::GetTrackShortDuration()
{
    return this->trackShortDuration;
}

QString MPlayer::Entities::Track::PlayerTrack::GetTrackArtistAndTitle()
{
    return QString("%1 - %2").arg(this->trackArtist, this->trackTitle);
}

qint64 MPlayer::Entities::Track::PlayerTrack::GetTrackFilesize()
{
    return this->trackFilesize;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackNumber(const unsigned int &trackNumber)
{
    this->trackNumber = trackNumber;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackBitrate(const int &trackBitrate)
{
    this->trackBitrate = trackBitrate;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackYear(const unsigned int &trackYear)
{
    this->trackYear = trackYear;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackTitle(const QString &trackTitle)
{
    this->trackTitle = trackTitle;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackArtist(const QString &trackArtist)
{
    this->trackArtist = trackArtist;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackAlbum(const QString &trackAlbum)
{
    this->trackAlbum = trackAlbum;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackGenre(const QString &trackGenre)
{
    this->trackGenre = trackGenre;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackFilepath(const QString &trackFilepath)
{
    this->trackFilepath = trackFilepath;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackDuration(const QString &trackDuration)
{
    this->trackDuration = trackDuration;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackShortDuration(const QString &trackShortDuration)
{
    this->trackShortDuration = trackShortDuration;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackPlaylistAddedStatus(const bool &trackPlaylistAdded)
{
    this->trackPlaylistAdded = trackPlaylistAdded;
}

void MPlayer::Entities::Track::PlayerTrack::SetTrackFilesize(const qint64 &trackFilesize)
{
    this->trackFilesize = trackFilesize;
}

QString MPlayer::Entities::Track::PlayerTrack::GetTrackAttributeByName(const QString &trackAttribute)
{
    if(trackAttribute == "Artist")
    {
        return GetTrackArtist();
    }

    if(trackAttribute == "Album")
    {
        return GetTrackAlbum();
    }

    if(trackAttribute == "Title")
    {
        return GetTrackTitle();
    }

    if(trackAttribute == "Genre")
    {
        return GetTrackGenre();
    }

    if(trackAttribute == "Year")
    {
        return QString::number(GetTrackYear());
    }

    if(trackAttribute == "Filepath")
    {
        return GetTrackFilepath();
    }

    if(trackAttribute == "Artist - Title")
    {
        return GetTrackArtistAndTitle();
    }

    return QString();
}
