// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef TRACKCOLUMN_HPP
#define TRACKCOLUMN_HPP

namespace MPlayer
{
    namespace Entities
    {
        namespace Track
        {
            enum TrackColumn
            {
                TRACK_NUMBER = 0,
                TRACK_PLAY_STATUS = 1,
                TRACK_TITLE = 2,
                TRACK_ARTIST = 3,
                TRACK_ALBUM = 4,
                TRACK_GENRE = 5,
                TRACK_DURATION = 6,
                TRACK_YEAR = 7,
                TRACK_BITRATE = 8,
                TRACK_FILEPATH = 9
            };
        }
    }
}

#endif // TRACKCOLUMN_HPP
