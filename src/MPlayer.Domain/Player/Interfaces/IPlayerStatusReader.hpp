// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef IPLAYER_STATUS_READER_HPP
#define IPLAYER_STATUS_READER_HPP

#include <QTime>
#include <QLabel>

#include "MPlayer.Domain/Library/LibraryTrackReader.hpp"
#include "MPlayer.Domain/Playlist/PlaylistTrackReader.hpp"
#include "MPlayer.Entities/Player/PlayerTab.hpp"
#include "MPlayer.Entities/Track/PlayerTrack.hpp"

using namespace MPlayer::Domain::Playlist;
using namespace MPlayer::Domain::Library;
using namespace MPlayer::Entities::Player;

namespace MPlayer
{
    namespace Domain
    {
        namespace Player
        {
            class IPlayerStatusReader
            {
                public:
                    virtual ~IPlayerStatusReader() = 0;
                    virtual QString GetTabStatusStringForPlayerTab(QStringList currentTrackFilepaths, PlayerTab currentTab) = 0;
            };
        }
    }
}

#endif // IPLAYER_STATUS_READER_HPP
