// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef IPLAYER_SERVICE_HPP
#define IPLAYER_SERVICE_HPP

#include "MPlayer.Data/PlayerRepository.hpp"

using namespace MPlayer::Data;

namespace MPlayer
{
    namespace Domain
    {
        namespace Player
        {
            class IPlayerService
            {
                public:
                    virtual ~IPlayerService() = 0;
                    virtual int64_t GetCurrentDuration() = 0;
                    virtual QString GetCurrentDurationString() = 0;
                    virtual QString GetFileFormat() = 0;
                    virtual int64_t GetTotalDuration() = 0;
                    virtual QString GetTotalDurationString() = 0;
                    virtual QString GetTrackFilepath() = 0;
                    virtual bool IsPaused() = 0;
                    virtual bool IsPlaying() = 0;
                    virtual bool IsPlayerServiceRunning() = 0;
                    virtual bool IsStopped() = 0;
                    virtual void OpenTrack(const QString &trackFilepath) = 0;
                    virtual void PausePlayerService(const unsigned long &miliseconds) = 0;
                    virtual void PauseTrack() = 0;
                    virtual void PlayTrack() = 0;
                    virtual void SetPosition(const int64_t &durationPos) = 0;
                    virtual void SetTrackFilepath(const QString &trackFilepath) = 0;
                    virtual void SetVolume(const float &volume) = 0;
                    virtual void StartPlayerService() = 0;
                    virtual void StopTrack() = 0;
            };
        }
    }
}

#endif // IPLAYER_SERVICE_HPP
