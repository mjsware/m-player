// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "PlayerService.hpp"

MPlayer::Domain::Player::PlayerService::PlayerService(const float &volume, const QString &defaultAudioDriverName)
{
    this->playerRepository = std::shared_ptr<IPlayerRepository>(new PlayerRepository(volume, defaultAudioDriverName));
}

QString MPlayer::Domain::Player::PlayerService::GetCurrentDurationString()
{
    int64_t currentLength = GetCurrentDuration();
    time_t currentLengthSeconds(currentLength);
    tm *currentTime = gmtime(&currentLengthSeconds);

    return GetDurationString(currentTime);
}

QString MPlayer::Domain::Player::PlayerService::GetTotalDurationString()
{
    int64_t totalLength = GetTotalDuration();
    time_t totalLengthSeconds(totalLength);
    tm *totalTime = gmtime(&totalLengthSeconds);

    return GetDurationString(totalTime);
}

QString MPlayer::Domain::Player::PlayerService::GetDurationString(tm *time)
{
    return time->tm_hour > 0 ? QString().sprintf("%02i:%02i:%02i", time->tm_hour, time->tm_min, time->tm_sec)
                             : QString().sprintf("%02i:%02i", time->tm_min, time->tm_sec);
}

void MPlayer::Domain::Player::PlayerService::PlayTrack()
{
    this->playerRepository->PlayTrack();
}

void MPlayer::Domain::Player::PlayerService::PauseTrack()
{
    this->playerRepository->PauseTrack();
}

void MPlayer::Domain::Player::PlayerService::StartPlayerService()
{
    this->playerRepository->start();
}

void MPlayer::Domain::Player::PlayerService::SetPosition(const int64_t &durationPos)
{
    this->playerRepository->SetPosition(durationPos);
}

void MPlayer::Domain::Player::PlayerService::SetTrackFilepath(const QString &trackFilepath)
{
    this->playerRepository->SetTrackFilepath(trackFilepath);
}

void MPlayer::Domain::Player::PlayerService::SetVolume(const float &volume)
{
    if(IsPlaying())
    {
        this->playerRepository->SetVolume(volume);
    }
}

void MPlayer::Domain::Player::PlayerService::StopTrack()
{
    this->playerRepository->StopTrack();
}

QString MPlayer::Domain::Player::PlayerService::GetFileFormat()
{
    return this->playerRepository->GetFileFormat();
}

QString MPlayer::Domain::Player::PlayerService::GetTrackFilepath()
{
    return this->playerRepository->GetTrackFilepath();
}

int64_t MPlayer::Domain::Player::PlayerService::GetCurrentDuration()
{
    return this->playerRepository->GetCurrentDuration();
}

int64_t MPlayer::Domain::Player::PlayerService::GetTotalDuration()
{
    return this->playerRepository->GetTotalDuration();
}

bool MPlayer::Domain::Player::PlayerService::IsPaused()
{
    return this->playerRepository->IsPaused();
}

bool MPlayer::Domain::Player::PlayerService::IsPlaying()
{
    return this->playerRepository->IsPlaying();
}

bool MPlayer::Domain::Player::PlayerService::IsStopped()
{
    return this->playerRepository->IsStopped();
}

bool MPlayer::Domain::Player::PlayerService::IsPlayerServiceRunning()
{
    return this->playerRepository->isRunning();
}

void MPlayer::Domain::Player::PlayerService::PausePlayerService(const unsigned long &miliseconds)
{
    this->playerRepository->wait(miliseconds);
}

void MPlayer::Domain::Player::PlayerService::OpenTrack(const QString &trackFilepath)
{
    if(IsPlayerServiceRunning())
    {
        StopTrack();
        PausePlayerService(1500);
    }

    SetTrackFilepath(trackFilepath);
    PlayTrack();
    StartPlayerService();
}
