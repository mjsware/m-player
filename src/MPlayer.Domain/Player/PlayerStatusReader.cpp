// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "PlayerStatusReader.hpp"

MPlayer::Domain::Player::PlayerStatusReader::PlayerStatusReader(std::shared_ptr<ILibraryTrackReader> libraryTrackReader,
                                                                std::shared_ptr<IPlaylistTrackReader> playlistTrackReader)
{
    this->libraryTrackReader = libraryTrackReader;
    this->playlistTrackReader = playlistTrackReader;
}

QString MPlayer::Domain::Player::PlayerStatusReader::GetTabStatusStringForPlayerTab(QStringList currentTrackFilepaths, PlayerTab currentTab)
{
    long durationSecs = 0;
    qint64 totalFileSize = 0;
    QString selectedTrackFilepath = "";

    for(int i = 0; i < currentTrackFilepaths.count(); i++)
    {
        selectedTrackFilepath = currentTrackFilepaths[i];
        PlayerTrack selectedTrack = currentTab == PlayerTab::LIBRARY ? this->libraryTrackReader->GetCachedLibraryTrackByFilepath(selectedTrackFilepath)
                                                                     : this->playlistTrackReader->GetPlaylistTrack(selectedTrackFilepath);
        totalFileSize += selectedTrack.GetTrackFilesize();
        durationSecs += QTime::fromString("00:00:00", "hh:mm:ss").secsTo(QTime::fromString(selectedTrack.GetTrackDuration(), "hh:mm:ss"));
    }

    if(currentTrackFilepaths.count() > 0)
    {
        return GetCalculatedTrackListStatusString(currentTrackFilepaths.count(), totalFileSize, durationSecs);
    }

    return QString();
}

QString MPlayer::Domain::Player::PlayerStatusReader::GetCalculatedTrackListStatusString(const int &totalRowCount, qint64 &totalFileSize, const long &durationSecs)
{
    // Calculate time by number of seconds
    time_t seconds(durationSecs);
    tm *p = gmtime(&seconds); // convert to broken down time

    QString dayLabel, hrLabel, minLabel, secLabel, timeLabel;

    int daysDuration = p->tm_mday - 1; // Minus 1 from days duration (starts at 1) // http://www.cplusplus.com/reference/ctime/tm/
    int hoursDuration = p->tm_hour;

    hrLabel = hoursDuration > 1 ? "Hrs" : "Hr";
    minLabel = p->tm_min > 1 ? "Mins" : "Min";
    secLabel = p->tm_sec > 1 ? "Secs" : "Sec";

    if(hoursDuration == 0) // Only show hours when appropriate
    {
        timeLabel = QString("%1 %2, %3 %4").arg(QString::number(p->tm_min), minLabel, QString::number(p->tm_sec), secLabel);
    }
    else if(daysDuration > 0)
    {
        dayLabel = daysDuration > 1 ? "Days" : "Day";

        timeLabel = QString::number(daysDuration) + " " + dayLabel + ", " +
                    QString::number(hoursDuration) + " " + hrLabel + ", " +
                    QString::number(p->tm_min) + " " + minLabel + ", " +
                    QString::number(p->tm_sec) + " " + secLabel;
    }
    else // Default string to show
    {
        timeLabel = QString::number(hoursDuration) + " " + hrLabel + ", " +
                    QString::number(p->tm_min) + " " + minLabel + ", " +
                    QString::number(p->tm_sec) + " " + secLabel;
    }

    // Calculate total size of playlist files together
    totalFileSize = totalFileSize / 1024 / 1024;

    return QString(" %1 Tracks, %2, %3 MB").arg(QString::number(totalRowCount), timeLabel, QString::number(totalFileSize));
}
