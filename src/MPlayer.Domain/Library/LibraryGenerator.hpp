// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef LIBRARY_GENERATOR_HPP
#define LIBRARY_GENERATOR_HPP

#include "Interfaces/ILibraryGenerator.hpp"

namespace MPlayer
{
    namespace Domain
    {
        namespace Library
        {
            class LibraryGenerator : public ILibraryGenerator
            {
                public:
                    LibraryGenerator();
                    double GetLibraryPercentageGenerated() override;
                    bool IsGenerated() override;
                    bool IsLibraryServiceRunning() override;
                    void RunLibraryService() override;

                private:
                    std::shared_ptr<ILibraryRepository> libraryRepository;
            };
        }
    }
}

#endif // LIBRARY_GENERATOR_HPP
