// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "LibraryTrackWriter.hpp"

MPlayer::Domain::Library::LibraryTrackWriter::LibraryTrackWriter(std::shared_ptr<ILibraryTrackReader> libraryReader)
{
    this->libraryRepository = std::shared_ptr<ILibraryRepository>(new LibraryRepository());
    this->libraryReader = libraryReader;
}

void MPlayer::Domain::Library::LibraryTrackWriter::UpdateLibraryTrack(PlayerTrack &selectedTrack)
{
    if(this->libraryReader->IsLibraryTrackByFilepathCached(selectedTrack.GetTrackFilepath()))
    {
        this->libraryRepository->UpdateLibraryTrack(selectedTrack);
        this->libraryReader->GetLatestLibraryTracks();
    }
}

void MPlayer::Domain::Library::LibraryTrackWriter::RemoveLibraryTrack(const QString &trackFilepath)
{
    if(this->libraryReader->IsLibraryTrackByFilepathCached(trackFilepath))
    {
        this->libraryRepository->RemoveLibraryTrack(trackFilepath);
        this->libraryReader->GetLatestLibraryTracks();
    }
}

void MPlayer::Domain::Library::LibraryTrackWriter::AddSelectedTrackToLibrary(const PlayerTrack &selectedTrack)
{
    AddSelectedTracksToLibrary(QVector<PlayerTrack> { selectedTrack });
}

void MPlayer::Domain::Library::LibraryTrackWriter::AddSelectedTracksToLibrary(QVector<PlayerTrack> selectedTracks)
{
    if(selectedTracks.count() == 0)
    {
        return;
    }

    this->libraryRepository->AddSelectedLibraryTracks(selectedTracks);
    this->libraryReader->GetLatestLibraryTracks();
}
