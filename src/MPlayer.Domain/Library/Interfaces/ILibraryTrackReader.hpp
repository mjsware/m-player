// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef ILIBRARY_TRACK_READER_HPP
#define ILIBRARY_TRACK_READER_HPP

#include "MPlayer.Data/LibraryRepository.hpp"
#include "MPlayer.Entities/Track/PlayerTrack.hpp"

using namespace MPlayer::Data;
using namespace MPlayer::Entities::Track;

namespace MPlayer
{
    namespace Domain
    {
        namespace Library
        {
            class ILibraryTrackReader
            {
                public:
                    virtual ~ILibraryTrackReader() = 0;
                    virtual PlayerTrack GetCachedLibraryTrack(const int &index) = 0;
                    virtual int GetCachedLibraryTrackCount() = 0;
                    virtual PlayerTrack GetCachedLibraryTrackByFilepath(const QString &trackFilepath) = 0;
                    virtual QStringList GetCachedLibraryFileListByQuery(const QString &query) = 0;
                    virtual QStringList GetCachedLibraryTrackFilepaths() = 0;
                    virtual QStringList GetCachedLibraryAlbumListForArtist(const QString &selectedArtist) = 0;
                    virtual QStringList GetCachedLibraryArtistList() = 0;
                    virtual QVector<PlayerTrack> GetCachedLibraryTrackListForAlbum(const QString &selectedArtist, const QString &selectedAlbum) = 0;
                    virtual void GetLatestLibraryTracks() = 0;
                    virtual QVector<PlayerTrack> GetLibraryTracksByQuery(const QString &query) = 0;
                    virtual bool IsLibraryTrackByFilepathCached(const QString &trackFilepath) = 0;
            };
        }
    }
}

#endif // ILIBRARY_TRACK_READER_HPP
