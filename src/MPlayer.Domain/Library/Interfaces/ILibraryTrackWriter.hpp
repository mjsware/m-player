// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef ILIBRARY_TRACK_WRITER_HPP
#define ILIBRARY_TRACK_WRITER_HPP

#include "MPlayer.Data/LibraryRepository.hpp"
#include "MPlayer.Domain/Library/LibraryTrackReader.hpp"
#include "MPlayer.Entities/Track/PlayerTrack.hpp"

using namespace MPlayer::Data;
using namespace MPlayer::Entities::Track;

namespace MPlayer
{
    namespace Domain
    {
        namespace Library
        {
            class ILibraryTrackWriter
            {
                public:
                    virtual ~ILibraryTrackWriter() = 0;
                    virtual void AddSelectedTrackToLibrary(const PlayerTrack &selectedTrack) = 0;
                    virtual void AddSelectedTracksToLibrary(QVector<PlayerTrack> selectedTracks) = 0;
                    virtual void RemoveLibraryTrack(const QString &trackFilepath) = 0;
                    virtual void UpdateLibraryTrack(PlayerTrack &selectedTrack) = 0;
            };
        }
    }
}

#endif // ILIBRARY_TRACK_WRITER_HPP
