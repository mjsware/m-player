// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef ILIBRARY_GENERATOR_HPP
#define ILIBRARY_GENERATOR_HPP

#include "MPlayer.Data/LibraryRepository.hpp"

using namespace MPlayer::Data;

namespace MPlayer
{
    namespace Domain
    {
        namespace Library
        {
            class ILibraryGenerator
            {
                public:
                    virtual ~ILibraryGenerator() = 0;
                    virtual double GetLibraryPercentageGenerated() = 0;
                    virtual bool IsGenerated() = 0;
                    virtual bool IsLibraryServiceRunning() = 0;
                    virtual void RunLibraryService() = 0;
            };
        }
    }
}

#endif // ILIBRARY_GENERATOR_HPP
