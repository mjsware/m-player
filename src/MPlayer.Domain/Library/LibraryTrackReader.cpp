// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "LibraryTrackReader.hpp"

MPlayer::Domain::Library::LibraryTrackReader::LibraryTrackReader()
{
    this->libraryRepository = std::shared_ptr<ILibraryRepository>(new LibraryRepository());

    GetLatestLibraryTracks();
}

PlayerTrack MPlayer::Domain::Library::LibraryTrackReader::GetCachedLibraryTrack(const int &index)
{
    if(this->cachedLibraryTracks.count() > index && !this->cachedLibraryTracks[index].GetTrackFilepath().isEmpty())
    {
        return this->cachedLibraryTracks[index];
    }
    return PlayerTrack();

}

PlayerTrack MPlayer::Domain::Library::LibraryTrackReader::GetCachedLibraryTrackByFilepath(const QString &trackFilepath)
{
    for (int i = 0; i < this->cachedLibraryTracks.count(); i++)
    {
        if(this->cachedLibraryTracks[i].GetTrackFilepath() == trackFilepath)
        {
            return this->cachedLibraryTracks[i];
        }
    }
    return PlayerTrack();
}


QStringList MPlayer::Domain::Library::LibraryTrackReader::GetCachedLibraryArtistList()
{
    QStringList cachedTrackArtists;
    PlayerTrack currentTrack;

    for(int i = 0; i < GetCachedLibraryTrackCount(); i++)
    {
        currentTrack = GetCachedLibraryTrack(i);

        if(!cachedTrackArtists.contains(currentTrack.GetTrackArtist(), Qt::CaseSensitive))
        {
            cachedTrackArtists.append(currentTrack.GetTrackArtist());
        }
    }

    return cachedTrackArtists;
}

QStringList MPlayer::Domain::Library::LibraryTrackReader::GetCachedLibraryTrackFilepaths()
{
    QStringList cachedTrackFilepaths;
    PlayerTrack currentTrack;

    for(int i = 0; i < GetCachedLibraryTrackCount(); i++)
    {
        currentTrack = GetCachedLibraryTrack(i);

        if(!cachedTrackFilepaths.contains(currentTrack.GetTrackFilepath(), Qt::CaseSensitive))
        {
            cachedTrackFilepaths.append(currentTrack.GetTrackFilepath());
        }
    }

    return cachedTrackFilepaths;
}

QStringList MPlayer::Domain::Library::LibraryTrackReader::GetCachedLibraryAlbumListForArtist(const QString &selectedArtist)
{
    QStringList cachedTrackAlbums;
    PlayerTrack currentTrack;

    for(int i = 0; i < GetCachedLibraryTrackCount(); i++)
    {
        currentTrack = GetCachedLibraryTrack(i);

        if(QString::compare(currentTrack.GetTrackArtist(), selectedArtist, Qt::CaseSensitive) == 0 || selectedArtist == "All Artists")
        {
            if(!cachedTrackAlbums.contains(currentTrack.GetTrackAlbum(), Qt::CaseSensitive))
            {
                cachedTrackAlbums.append(currentTrack.GetTrackAlbum());
            }
        }
    }

    return cachedTrackAlbums;
}

QVector<PlayerTrack> MPlayer::Domain::Library::LibraryTrackReader::GetCachedLibraryTrackListForAlbum(const QString &selectedArtist, const QString &selectedAlbum)
{
    QVector<PlayerTrack> cachedAlbumTracks;

    for(int i = 0; i < GetCachedLibraryTrackCount(); i++)
    {
        PlayerTrack currentTrack = GetCachedLibraryTrack(i);

        if(QString::compare(currentTrack.GetTrackArtist(), selectedArtist, Qt::CaseSensitive) == 0 || selectedArtist == "All Artists")
        {
            if(QString::compare(currentTrack.GetTrackAlbum(), selectedAlbum, Qt::CaseSensitive) == 0 || selectedAlbum == "All Albums")
            {
                cachedAlbumTracks.append(currentTrack);
            }
        }
    }

    return cachedAlbumTracks;
}

bool MPlayer::Domain::Library::LibraryTrackReader::IsLibraryTrackByFilepathCached(const QString &trackFilepath)
{
    for (int i = 0; i < this->cachedLibraryTracks.count(); i++)
    {
        if(this->cachedLibraryTracks[i].GetTrackFilepath() == trackFilepath)
        {
            return true;
        }
    }
    return false;
}

QStringList MPlayer::Domain::Library::LibraryTrackReader::GetCachedLibraryFileListByQuery(const QString &query)
{
    QStringList foundLibraryTrackFilepaths;

    for (int i = 0; i < this->cachedLibraryTracks.count(); i++)
    {
        QFileInfo currentTrackFileInfo(this->cachedLibraryTracks[i].GetTrackFilepath());

        if(this->cachedLibraryTracks[i].GetTrackArtist().indexOf(query, 0, Qt::CaseInsensitive) != -1
                || this->cachedLibraryTracks[i].GetTrackAlbum().indexOf(query, 0, Qt::CaseInsensitive) != -1
                || this->cachedLibraryTracks[i].GetTrackTitle().indexOf(query, 0, Qt::CaseInsensitive) != -1
                || this->cachedLibraryTracks[i].GetTrackGenre().indexOf(query, 0, Qt::CaseInsensitive) != -1
                || currentTrackFileInfo.fileName().indexOf(query, 0, Qt::CaseInsensitive) != -1
                || QString::number(this->cachedLibraryTracks[i].GetTrackYear()).indexOf(query, 0, Qt::CaseInsensitive) != -1)
        {
            foundLibraryTrackFilepaths.append(this->cachedLibraryTracks[i].GetTrackFilepath());
            continue;
        }
    }
    return foundLibraryTrackFilepaths;
}

void MPlayer::Domain::Library::LibraryTrackReader::GetLatestLibraryTracks()
{
    this->cachedLibraryTracks = GetLibraryTracksByQuery("");
}

QVector<PlayerTrack> MPlayer::Domain::Library::LibraryTrackReader::GetLibraryTracksByQuery(const QString &query)
{
    return this->libraryRepository->GetLibraryTracksByQuery(query);
}

int MPlayer::Domain::Library::LibraryTrackReader::GetCachedLibraryTrackCount()
{
    return this->cachedLibraryTracks.count();
}
