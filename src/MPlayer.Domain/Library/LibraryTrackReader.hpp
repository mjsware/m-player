// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef LIBRARY_TRACK_READER_HPP
#define LIBRARY_TRACK_READER_HPP

#include "Interfaces/ILibraryTrackReader.hpp"

namespace MPlayer
{
    namespace Domain
    {
        namespace Library
        {
            class LibraryTrackReader: public ILibraryTrackReader
            {
                public:
                    LibraryTrackReader();
                    PlayerTrack GetCachedLibraryTrack(const int &index) override;
                    int GetCachedLibraryTrackCount() override;
                    PlayerTrack GetCachedLibraryTrackByFilepath(const QString &trackFilepath) override;
                    QStringList GetCachedLibraryFileListByQuery(const QString &query) override;
                    QStringList GetCachedLibraryTrackFilepaths() override;
                    QStringList GetCachedLibraryAlbumListForArtist(const QString &selectedArtist) override;
                    QStringList GetCachedLibraryArtistList() override;
                    QVector<PlayerTrack> GetCachedLibraryTrackListForAlbum(const QString &selectedArtist, const QString &selectedAlbum) override;
                    void GetLatestLibraryTracks() override;
                    QVector<PlayerTrack> GetLibraryTracksByQuery(const QString &query) override;
                    bool IsLibraryTrackByFilepathCached(const QString &trackFilepath) override;

                protected:
                    std::shared_ptr<ILibraryRepository> libraryRepository;
                    QVector<PlayerTrack> cachedLibraryTracks;
            };
        }
    }
}

#endif // LIBRARY_TRACK_READER_HPP
