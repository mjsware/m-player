// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "TrackGridWriter.hpp"

void MPlayer::Domain::Track::TrackGridWriter::AddTrackToTableGrid(const int &rowId,
                                                                  const bool &isPlayingTrack,
                                                                  PlayerTrack &currentTrack,
                                                                  QTableWidget *selectedTable)
{
    selectedTable->insertRow(rowId);

    QTableWidgetItem *trackNumber = new QTableWidgetItem(QString::number(currentTrack.GetTrackNumber()));
    trackNumber->setTextAlignment(Qt::AlignHCenter + Qt::AlignVCenter);
    selectedTable->setItem(rowId, TRACK_NUMBER, trackNumber);

    QTableWidgetItem *trackPlayStatus = new QTableWidgetItem(isPlayingTrack ? "*" : "");
    trackPlayStatus->setTextAlignment(Qt::AlignHCenter + Qt::AlignVCenter);
    selectedTable->setItem(rowId, TRACK_PLAY_STATUS, trackPlayStatus);

    QTableWidgetItem *trackTitle = new QTableWidgetItem(currentTrack.GetTrackTitle());
    selectedTable->setItem(rowId, TRACK_TITLE, trackTitle);

    QTableWidgetItem *trackArtist = new QTableWidgetItem(currentTrack.GetTrackArtist());
    selectedTable->setItem(rowId, TRACK_ARTIST, trackArtist);

    QTableWidgetItem *trackAlbum = new QTableWidgetItem(currentTrack.GetTrackAlbum());
    selectedTable->setItem(rowId, TRACK_ALBUM, trackAlbum);

    QTableWidgetItem *trackGenre = new QTableWidgetItem(currentTrack.GetTrackGenre());
    selectedTable->setItem(rowId, TRACK_GENRE, trackGenre);

    QTableWidgetItem *trackDuration = new QTableWidgetItem(currentTrack.GetTrackShortDuration());
    trackDuration->setTextAlignment(Qt::AlignHCenter + Qt::AlignVCenter);
    selectedTable->setItem(rowId, TRACK_DURATION, trackDuration);

    QTableWidgetItem *trackYear = new QTableWidgetItem(currentTrack.GetTrackYear() != 0 ? QString::number(currentTrack.GetTrackYear()) : "Unknown");
    trackDuration->setTextAlignment(Qt::AlignHCenter + Qt::AlignVCenter);
    selectedTable->setItem(rowId, TRACK_YEAR, trackYear);

    QTableWidgetItem *trackBitrate = new QTableWidgetItem(QString::number(currentTrack.GetTrackBitrate()) + " kbps");
    trackDuration->setTextAlignment(Qt::AlignHCenter + Qt::AlignVCenter);
    selectedTable->setItem(rowId, TRACK_BITRATE, trackBitrate);

    QTableWidgetItem *trackFilepath = new QTableWidgetItem(currentTrack.GetTrackFilepath());
    selectedTable->setItem(rowId, TRACK_FILEPATH, trackFilepath);
}

void MPlayer::Domain::Track::TrackGridWriter::UpdateTrackInTableWidget(const int &rowId, PlayerTrack &currentTrack, QTableWidget *selectedTableWidget)
{
    if(rowId != -1)
    {
        selectedTableWidget->item(rowId, TRACK_NUMBER)->setText(QString::number(currentTrack.GetTrackNumber()));
        selectedTableWidget->item(rowId, TRACK_TITLE)->setText(currentTrack.GetTrackTitle());
        selectedTableWidget->item(rowId, TRACK_ARTIST)->setText(currentTrack.GetTrackArtist());
        selectedTableWidget->item(rowId, TRACK_ALBUM)->setText(currentTrack.GetTrackAlbum());
        selectedTableWidget->item(rowId, TRACK_GENRE)->setText(currentTrack.GetTrackGenre());
        selectedTableWidget->item(rowId, TRACK_YEAR)->setText(currentTrack.GetTrackYear() != 0 ? QString::number(currentTrack.GetTrackYear()) : "Unknown");
    }
}


void MPlayer::Domain::Track::TrackGridWriter::ResetPlayingStatusOfTrackForTableWidget(QTableWidget *selectedTableWidget)
{
    for(int i = 0; i < selectedTableWidget->rowCount(); i++)
    {
        if(selectedTableWidget->item(i, TRACK_PLAY_STATUS) != nullptr)
        {
            selectedTableWidget->item(i, TRACK_PLAY_STATUS)->setText("");
        }
    }
}

void MPlayer::Domain::Track::TrackGridWriter::SetPlayingStatusOfTrackForTableWidget(const int &rowId, QTableWidget *selectedTableWidget)
{
    if(rowId >= 0)
    {
        selectedTableWidget->item(rowId, TRACK_PLAY_STATUS)->setText("*");
        selectedTableWidget->item(rowId, TRACK_PLAY_STATUS)->setTextAlignment(Qt::AlignHCenter + Qt::AlignVCenter);
        selectedTableWidget->selectRow(rowId);
    }
}

void MPlayer::Domain::Track::TrackGridWriter::SelectTrackListBoxItemByValue(QListWidget *listWidget, const QString &itemValue)
{
    for(int i = 0; i < listWidget->count(); i++)
    {
        if(QString::compare(listWidget->item(i)->text(), itemValue, Qt::CaseSensitive) == 0)
        {
            listWidget->item(i)->setSelected(true);
            listWidget->setCurrentRow(i);
            listWidget->setFocus();
            break;
        }
    }
}

void MPlayer::Domain::Track::TrackGridWriter::MoveTrackUp(QTableWidget *selectedTableWidget)
{
    int rowId = -1;
    int oldRowId = -1;
    QModelIndex sourceParent;
    bool isPlayingTrack = false;
    QModelIndexList selectedList = selectedTableWidget->selectionModel()->selectedRows();
    PlayerTrack selectedTrack;

    if(selectedList.count() == 1)
    {
        for(int i = 0; i < selectedList.count(); i++)
        {
            sourceParent = selectedList.at(i);
            rowId = sourceParent.row(); // Selected row id

            if(rowId != 0) //  If the item is in first row
            {
                // Get old row id
                oldRowId = rowId - 1;

                // Get selected row (one to be inserted to old row position) with column details
                selectedTrack = GetPlayerTrackFromTableGridColumns(GetTrackRowPropertiesAsList(selectedTableWidget, rowId));

                isPlayingTrack = selectedTableWidget->item(rowId, TRACK_PLAY_STATUS)->text() == "*" ? true : false;

                // Insert new row
                AddTrackToTableGrid(oldRowId, isPlayingTrack, selectedTrack, selectedTableWidget);

                // Remove duplicate row
                selectedTableWidget->removeRow(rowId + 1);

                // Select new row and focus to table
                selectedTableWidget->selectRow(oldRowId);
                selectedTableWidget->setFocus();
            }
        }
    }
}

void MPlayer::Domain::Track::TrackGridWriter::MoveTrackDown(QTableWidget *selectedTableWidget)
{
    int rowId = -1;
    int forwardRowId = -1;
    QModelIndex sourceParent;
    bool isPlayingTrack = false;
    QModelIndexList selectedList = selectedTableWidget->selectionModel()->selectedRows();
    PlayerTrack selectedTrack;

    if(selectedList.count() == 1)
    {
        for(int i = 0; i < selectedList.count(); i++)
        {
            sourceParent = selectedList.at(i);
            rowId = sourceParent.row(); // Selected row id

            if(rowId != (selectedTableWidget->rowCount() - 1)) //  If the item is not last row
            {
                // Get old row id
                forwardRowId = rowId + 1;

                // Get forward row with column details
                selectedTrack = GetPlayerTrackFromTableGridColumns(GetTrackRowPropertiesAsList(selectedTableWidget, forwardRowId));

                isPlayingTrack = selectedTableWidget->item(forwardRowId, TRACK_PLAY_STATUS)->text() == "*" ? true : false;

                 // Remove existing forward row (one in front of selected row)
                selectedTableWidget->removeRow(forwardRowId);

                // Insert new row at last index (selected row)
                AddTrackToTableGrid(rowId, isPlayingTrack, selectedTrack, selectedTableWidget);

                // Select new row and focus to table
                selectedTableWidget->selectRow(forwardRowId);
                selectedTableWidget->setFocus();
            }
        }
    }
}

QStringList MPlayer::Domain::Track::TrackGridWriter::GetTrackRowPropertiesAsList(QTableWidget *selectedTableWidget, int rowId)
{
    QStringList trackProprties;

    trackProprties.append(selectedTableWidget->item(rowId, TRACK_NUMBER)->text());
    trackProprties.append(selectedTableWidget->item(rowId, TRACK_PLAY_STATUS)->text());
    trackProprties.append(selectedTableWidget->item(rowId, TRACK_TITLE)->text());
    trackProprties.append(selectedTableWidget->item(rowId, TRACK_ARTIST)->text());
    trackProprties.append(selectedTableWidget->item(rowId, TRACK_ALBUM)->text());
    trackProprties.append(selectedTableWidget->item(rowId, TRACK_GENRE)->text());
    trackProprties.append(selectedTableWidget->item(rowId, TRACK_DURATION)->text());
    trackProprties.append(selectedTableWidget->item(rowId, TRACK_YEAR)->text());
    trackProprties.append(selectedTableWidget->item(rowId, TRACK_BITRATE)->text());
    trackProprties.append(selectedTableWidget->item(rowId, TRACK_FILEPATH)->text());

    return trackProprties;
}

PlayerTrack MPlayer::Domain::Track::TrackGridWriter::GetPlayerTrackFromTableGridColumns(QStringList trackProperties)
{
    return PlayerTrack(trackProperties[TRACK_NUMBER].toUInt(),
                       trackProperties[TRACK_BITRATE].replace(" kbps","").toInt(),
                       trackProperties[TRACK_YEAR].replace("Unknown", "0").toUInt(),
                       trackProperties[TRACK_TITLE],
                       trackProperties[TRACK_ARTIST],
                       trackProperties[TRACK_ALBUM],
                       trackProperties[TRACK_GENRE],
                       trackProperties[TRACK_FILEPATH],
                       trackProperties[TRACK_DURATION],
                       false,
                       trackProperties[TRACK_DURATION],
                       0);
}
