// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef ITRACK_SHUFFLER_HPP
#define ITRACK_SHUFFLER_HPP

#include <QString>
#include <QStringList>

namespace MPlayer
{
    namespace Domain
    {
        namespace Track
        {
            class ITrackShuffler
            {
                public:
                    virtual ~ITrackShuffler() = 0;
                    virtual void AddTrackFilepathsForDefaultMode(const QStringList &trackFilepaths, const QString playingTrackFilepath) = 0;
                    virtual void AddTrackFilepathsForShuffleMode(const QStringList &trackFilepaths, const QString playingTrackFilepath) = 0;
                    virtual void ClearShuffleList() = 0;
                    virtual bool GetClearedState() = 0;
                    virtual QString GetCurrentTrackFilepath() = 0;
                    virtual int GetNumberOfTracks() = 0;
                    virtual QString GetNextTrackFilepath() = 0;
                    virtual QString GetPreviousTrackFilepath() = 0;
                    virtual bool GetRandomisedState() = 0;
                    virtual QString GetShuffleTrackFilepath(const int &id) = 0;
                    virtual void RandomiseList() = 0;
            };
        }
    }
}

#endif // ITRACK_SHUFFLER_HPP
