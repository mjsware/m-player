// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef ITRACK_GRID_READER_HPP
#define ITRACK_GRID_READER_HPP

#include <QTableWidget>
#include <QString>

#include "MPlayer.Entities/Track/PlayerTrack.hpp"
#include "MPlayer.Entities/Track/TrackColumn.hpp"

using namespace MPlayer::Entities::Track;

namespace MPlayer
{
    namespace Domain
    {
        namespace Track
        {
            class ITrackGridReader
            {
                public:
                    virtual ~ITrackGridReader() = 0;
                    virtual int GetCurrentTrackRowIdOfSelectedTrackWidget(QTableWidget *selectedTableWidget) = 0;
                    virtual int GetRowIdOfSelectedTrackWidgetByFilename(QTableWidget *selectedTableWidget, const QString &trackFilepath) = 0;
                    virtual int GetRowIdOfTrackByFilepath(QTableWidget *selectedTableWidget, const QString &trackFilepath) = 0;
                    virtual QStringList GetSelectedTrackFilepathsAsList(QTableWidget *selectedTableWidget) = 0;
                    virtual QStringList GetTrackFilepathsAsList(QTableWidget *selectedTableWidget) = 0;
            };
        }
    }
}

#endif // ITRACK_GRID_READER_HPP
