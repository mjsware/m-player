// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "AlbumArtReader.hpp"

std::shared_ptr<QGraphicsScene> MPlayer::Domain::Track::AlbumArtReader::GetAlbumArt(const QString &trackFilepath,
                                                                                    const QString &defaultAlbumArtPath,
                                                                                    const int &albumArtWidth,
                                                                                    const int &albumArtHeight)
{
    if(!trackFilepath.isEmpty())
    {
        QStringList filters;
        QFileInfo fileInfo(trackFilepath);
        QString directoryPath = fileInfo.absolutePath();

        std::shared_ptr<QGraphicsScene> albumArtScene = std::shared_ptr<QGraphicsScene>(new QGraphicsScene(QRectF(0, 0, albumArtWidth, albumArtHeight)));

        // Set filters for finding the appropriate files
        filters << "*.jpg" << "*.jpeg" << "*.png";

        QDir currentDirectory(directoryPath);
        currentDirectory.setNameFilters(filters);

        // If appropriate files exist in directory
        if(currentDirectory.entryList().count() > 0)
        {
            QString albumArtFile = currentDirectory.entryList().at(0);
            QPixmap pixmap(directoryPath + QDir::separator() + albumArtFile);

            albumArtScene->addPixmap(pixmap.scaled(QSize(static_cast<int>(albumArtScene->width()),
                                                         static_cast<int>(albumArtScene->height())),
                                                         Qt::KeepAspectRatio,
                                                         Qt::SmoothTransformation)); // Scale album art scene object to fit within QGraphicsView
        }

        // Found album art
        if(albumArtScene->items().count() > 0)
        {
            return albumArtScene;
        }
    }

    // Set default album art (Not found)
    return GetDefaultAlbumArt(defaultAlbumArtPath);
}

std::shared_ptr<QGraphicsScene> MPlayer::Domain::Track::AlbumArtReader::GetDefaultAlbumArt(const QString &defaultAlbumArtPath)
{
    std::shared_ptr<QGraphicsScene> albumArtScene = std::shared_ptr<QGraphicsScene>(new QGraphicsScene);
    albumArtScene->addPixmap(QPixmap(defaultAlbumArtPath));
    return albumArtScene;
}
