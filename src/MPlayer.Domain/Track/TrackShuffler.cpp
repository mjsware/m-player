// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "TrackShuffler.hpp"

MPlayer::Domain::Track::TrackShuffler::TrackShuffler()
{
    this->currentTrackNumber = 0;
    this->isRandomised = false;
    this->isCleared = true;
}

MPlayer::Domain::Track::TrackShuffler::~TrackShuffler()
{
    ClearShuffleList();
}

int MPlayer::Domain::Track::TrackShuffler::GetNumberOfTracks()
{
    return this->shuffleTrackFilepaths.count();
}

bool MPlayer::Domain::Track::TrackShuffler::GetRandomisedState()
{
    return this->isRandomised;
}

bool MPlayer::Domain::Track::TrackShuffler::GetClearedState()
{
    return this->isCleared;
}

QString MPlayer::Domain::Track::TrackShuffler::GetNextTrackFilepath()
{
    ++this->currentTrackNumber;
    return this->currentTrackNumber == this->shuffleTrackFilepaths.count() ? "" : this->shuffleTrackFilepaths.at(this->currentTrackNumber);
}

QString MPlayer::Domain::Track::TrackShuffler::GetCurrentTrackFilepath()
{
   return this->shuffleTrackFilepaths.at(this->currentTrackNumber);
}

QString MPlayer::Domain::Track::TrackShuffler::GetPreviousTrackFilepath()
{
    this->currentTrackNumber--;
    this->currentTrackNumber = this->currentTrackNumber < 0 ? 0 : this->currentTrackNumber;
    return this->shuffleTrackFilepaths.at(this->currentTrackNumber);
}

void MPlayer::Domain::Track::TrackShuffler::AddTrackFilepathsForDefaultMode(const QStringList &trackFilepaths, const QString playingTrackFilepath)
{
    this->shuffleTrackFilepaths.append(trackFilepaths);
    this->currentTrackNumber = this->shuffleTrackFilepaths.indexOf(playingTrackFilepath);
    this->isCleared = false;
    this->isRandomised = false;
}

void MPlayer::Domain::Track::TrackShuffler::AddTrackFilepathsForShuffleMode(const QStringList &trackFilepaths, const QString playingTrackFilepath)
{
    this->shuffleTrackFilepaths.append(playingTrackFilepath);

    for(int i = 0; i < trackFilepaths.count(); i++)
    {
        if(trackFilepaths[i] != playingTrackFilepath)
        {
            this->shuffleTrackFilepaths.append(trackFilepaths[i]);
        }
    }

    this->currentTrackNumber = this->shuffleTrackFilepaths.indexOf(playingTrackFilepath);
    this->isCleared = false;
    this->isRandomised = false;
}

QString MPlayer::Domain::Track::TrackShuffler::GetShuffleTrackFilepath(const int &id)
{
    return this->shuffleTrackFilepaths.count() >= id ? this->shuffleTrackFilepaths.at(id) : "";
}

void MPlayer::Domain::Track::TrackShuffler::RandomiseList()
{
    srand(static_cast<unsigned int>(time(nullptr)));

    int rowId = 0;
    QStringList shuffleTracksFilepathsClone;

    if (this->shuffleTrackFilepaths.count() > 0)
    {
        shuffleTracksFilepathsClone.clear();
        shuffleTracksFilepathsClone.append(this->shuffleTrackFilepaths.at(0));
        this->shuffleTrackFilepaths.removeAt(0);  // Delete first row

        while (this->shuffleTrackFilepaths.count() > 0)
        {
            rowId = (rand() % this->shuffleTrackFilepaths.count()); // Get a random row number based on row count of shuffle table
            shuffleTracksFilepathsClone.append(this->shuffleTrackFilepaths.at(rowId)); // Import random row into cloned table
            this->shuffleTrackFilepaths.removeAt(rowId); // Delete existing row number to prevent copying row more than once
        }
        this->shuffleTrackFilepaths = shuffleTracksFilepathsClone; // Copy random table back
        this->isRandomised = true; // Set random flag
    }
}

void MPlayer::Domain::Track::TrackShuffler::ClearShuffleList()
{
    this->shuffleTrackFilepaths.clear();
    this->isRandomised = false;
    this->isCleared = true;
    this->currentTrackNumber = 0;
}
