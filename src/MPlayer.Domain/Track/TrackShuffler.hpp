// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef TRACK_SHUFFLER_HPP
#define TRACK_SHUFFLER_HPP

#include "Interfaces/ITrackShuffler.hpp"

namespace MPlayer
{
    namespace Domain
    {
        namespace Track
        {
            class TrackShuffler : public ITrackShuffler
            {
                public:
                    TrackShuffler();
                    ~TrackShuffler() override;
                    void AddTrackFilepathsForDefaultMode(const QStringList &trackFilepaths, const QString playingTrackFilepath) override;
                    void AddTrackFilepathsForShuffleMode(const QStringList &trackFilepaths, const QString playingTrackFilepath) override;
                    void ClearShuffleList() override;
                    bool GetClearedState() override;
                    QString GetCurrentTrackFilepath() override;
                    int GetNumberOfTracks() override;
                    QString GetNextTrackFilepath() override;
                    QString GetPreviousTrackFilepath() override;
                    bool GetRandomisedState() override;
                    QString GetShuffleTrackFilepath(const int &id) override;
                    void RandomiseList() override;

                private:
                    int currentTrackNumber;
                    bool isRandomised;
                    bool isCleared;
                    QStringList shuffleTrackFilepaths;
            };
        }
    }
}

#endif // TRACK_SHUFFLER_HPP
