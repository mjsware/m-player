// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "TrackGridReader.hpp"

QStringList MPlayer::Domain::Track::TrackGridReader::GetSelectedTrackFilepathsAsList(QTableWidget *selectedTableWidget)
{
    int rowId = 0;
    QStringList tableFilepaths;

    for(int i = 0; i < selectedTableWidget->selectionModel()->selectedRows().count(); i++)
    {
        rowId = selectedTableWidget->selectionModel()->selectedRows().at(i).row();
        tableFilepaths.append(selectedTableWidget->item(rowId, TRACK_FILEPATH)->text());
    }

    return tableFilepaths;
}

QStringList MPlayer::Domain::Track::TrackGridReader::GetTrackFilepathsAsList(QTableWidget *selectedTableWidget)
{
    QStringList tableFilepaths;

    for(int i = 0; i < selectedTableWidget->rowCount(); i++)
    {
        if(selectedTableWidget->item(i, TRACK_FILEPATH) != nullptr)
        {
            tableFilepaths.append(selectedTableWidget->item(i, TRACK_FILEPATH)->text());
        }
    }

    return tableFilepaths;
}

int MPlayer::Domain::Track::TrackGridReader::GetRowIdOfTrackByFilepath(QTableWidget *selectedTableWidget, const QString &trackFilepath)
{
    for(int i = 0; i < selectedTableWidget->rowCount(); i++)
    {
        if(selectedTableWidget->item(i, TRACK_FILEPATH) != nullptr && QString::compare(selectedTableWidget->item(i, TRACK_FILEPATH)->text(), trackFilepath, Qt::CaseSensitive) == 0)
        {
            return i;
        }
    }
    return -1;
}

int MPlayer::Domain::Track::TrackGridReader::GetCurrentTrackRowIdOfSelectedTrackWidget(QTableWidget *selectedTableWidget)
{
    for(int i = 0; i < selectedTableWidget->rowCount(); i++)
    {
        if(selectedTableWidget->item(i, TRACK_PLAY_STATUS) != nullptr && selectedTableWidget->item(i, TRACK_PLAY_STATUS)->text() == "*")
        {
            return i;
        }
    }
    return -1;
}

int MPlayer::Domain::Track::TrackGridReader::GetRowIdOfSelectedTrackWidgetByFilename(QTableWidget *selectedTableWidget, const QString &trackFilepath)
{
    for(int i = 0; i < selectedTableWidget->rowCount(); i++)
    {
        if(selectedTableWidget->item(i, TRACK_FILEPATH) != nullptr && QString::compare(selectedTableWidget->item(i, TRACK_FILEPATH)->text(), trackFilepath, Qt::CaseSensitive) == 0)
        {
            return i;
        }
    }
    return -1;
}
