// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef TRACKGRID_WRITER_HPP
#define TRACKGRID_WRITER_HPP

#include "Interfaces/ITrackGridWriter.hpp"

namespace MPlayer
{
    namespace Domain
    {
        namespace Track
        {
            class TrackGridWriter : public ITrackGridWriter
            {
                public:
                    void AddTrackToTableGrid(const int &rowId,
                                             const bool &isPlayingTrack,
                                             PlayerTrack &currentTrack,
                                             QTableWidget *selectedTable) override;

                    void MoveTrackDown(QTableWidget *selectedTableWidget) override;
                    void MoveTrackUp(QTableWidget *selectedTableWidget) override;
                    void ResetPlayingStatusOfTrackForTableWidget(QTableWidget *selectedTableWidget) override;
                    void SelectTrackListBoxItemByValue(QListWidget *listWidget, const QString &itemValue) override;
                    void SetPlayingStatusOfTrackForTableWidget(const int &rowId, QTableWidget *selectedTableWidget) override;
                    void UpdateTrackInTableWidget(const int &rowId, PlayerTrack &currentTrack, QTableWidget *selectedTableWidget) override;

                private:
                    PlayerTrack GetPlayerTrackFromTableGridColumns(QStringList trackProperties);
                    QStringList GetTrackRowPropertiesAsList(QTableWidget *selectedTableWidget, int rowId);
            };
        }
    }
}

#endif // TRACKGRID_WRITER_HPP
