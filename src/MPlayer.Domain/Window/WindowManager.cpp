// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "WindowManager.hpp"

void MPlayer::Domain::Window::WindowManager::ShowProgramWindow(QWidget *programWindow)
{
    this->existingProgramWindows.push_front(programWindow);

    QRect screenGeometry = QApplication::desktop()->screenGeometry();
    int x = (screenGeometry.width() - this->existingProgramWindows.at(0)->width()) / 2;
    int y = (screenGeometry.height() - this->existingProgramWindows.at(0)->height()) / 2;

    this->existingProgramWindows.at(0)->move(x, y);
    this->existingProgramWindows.at(0)->show();
}

void MPlayer::Domain::Window::WindowManager::CloseExistingProgramWindows()
{
    for(int i = 0; i < this->existingProgramWindows.count(); i++)
    {
        if(this->existingProgramWindows.at(i) != nullptr)
        {
            this->existingProgramWindows.at(i)->close();
        }
    }
    this->existingProgramWindows.clear();
}
