// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef IPLAYLIST_TRACK_READER_HPP
#define IPLAYLIST_TRACK_READER_HPP

#include <QDirIterator>

#include "MPlayer.Data/PlaylistRepository.hpp"
#include "MPlayer.Entities/Track/PlayerTrack.hpp"
#include "MPlayer.Entities/Track/TrackColumn.hpp"

using namespace MPlayer::Data;
using namespace MPlayer::Entities::Track;

namespace MPlayer
{
    namespace Domain
    {
        namespace Playlist
        {
            class IPlaylistTrackReader
            {
                public:
                    virtual ~IPlaylistTrackReader() = 0;
                    virtual QStringList GetAvailablePlaylistTracksInDirectory(const QString &selectedDirectoryPath) = 0;
                    virtual QStringList GetFilelistOfPlaylistDirectory(const QString &playlistPath) = 0;
                    virtual PlayerTrack GetPlaylistTrack(const QString &playlistTrackFilepath) = 0;
                    virtual QVector<PlayerTrack> GetPlaylistTracks(const QStringList &playlistTrackFilepaths) = 0;
                    virtual QVector<PlayerTrack> GetSearchedPlaylistTracks(const QString &query, QTableWidget *selectedTableWidget) = 0;
                    virtual bool ValidateDroppedPlaylistFile(const QString &playlistTrackFilepath) = 0;
            };
        }
    }
}

#endif // IPLAYLIST_TRACK_READER_HPP
