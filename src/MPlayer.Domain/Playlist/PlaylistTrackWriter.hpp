// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef PLAYLIST_TRACK_WRITER_HPP
#define PLAYLIST_TRACK_WRITER_HPP

#include "Interfaces/IPlaylistTrackWriter.hpp"

using namespace MPlayer::Data;

namespace MPlayer
{
    namespace Domain
    {
        namespace Playlist
        {
            class PlaylistTrackWriter : public IPlaylistTrackWriter
            {
                public:
                    PlaylistTrackWriter();
                    void SavePlaylist(const QString &playlistPath, const QStringList &playlistFilePaths) override;
                    void SavePlaylistTrack(PlayerTrack &playlistTrack) override;

                private:
                    std::shared_ptr<IPlaylistRepository> playlistRepository;
            };
        }
    }
}

#endif // PLAYLIST_TRACK_WRITER_HPP
