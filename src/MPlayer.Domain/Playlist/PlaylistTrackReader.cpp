// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "PlaylistTrackReader.hpp"

MPlayer::Domain::Playlist::PlaylistTrackReader::PlaylistTrackReader()
{
    this->playlistRepository = std::shared_ptr<IPlaylistRepository>(new PlaylistRepository());
}

QStringList MPlayer::Domain::Playlist::PlaylistTrackReader::GetFilelistOfPlaylistDirectory(const QString &playlistPath)
{
    return this->playlistRepository->GetFilelistOfPlaylistDirectory(playlistPath);
}

bool MPlayer::Domain::Playlist::PlaylistTrackReader::ValidateDroppedPlaylistFile(const QString &playlistTrackFilepath)
{
    return playlistTrackFilepath.endsWith(".mp3")
            || playlistTrackFilepath.endsWith(".wma")
            || playlistTrackFilepath.endsWith(".wav")
            || playlistTrackFilepath.endsWith(".ogg")
            || playlistTrackFilepath.endsWith(".mp4")
            || playlistTrackFilepath.endsWith(".m4a")
            || playlistTrackFilepath.endsWith(".flac");
}

PlayerTrack MPlayer::Domain::Playlist::PlaylistTrackReader::GetPlaylistTrack(const QString &playlistTrackFilepath)
{
    QVector<PlayerTrack> playlistTracks = GetPlaylistTracks(QStringList(playlistTrackFilepath));
    return playlistTracks.count() == 1 ? playlistTracks[0] : PlayerTrack();
}

QVector<PlayerTrack> MPlayer::Domain::Playlist::PlaylistTrackReader::GetPlaylistTracks(const QStringList &playlistTrackFilepaths)
{
    return this->playlistRepository->GetPlaylistTracks(playlistTrackFilepaths);
}

QVector<PlayerTrack> MPlayer::Domain::Playlist::PlaylistTrackReader::GetSearchedPlaylistTracks(const QString &query, QTableWidget *selectedTableWidget)
{
    QVector<PlayerTrack> foundPlaylistTracks;
    QString trackNumber, trackBitrate, trackTitle, trackArtist, trackAlbum, trackGenre, trackYear, trackFilepath, trackDuration;

    for(int i = 0; i < selectedTableWidget->rowCount(); i++)
    {
        trackFilepath = selectedTableWidget->item(i, TRACK_FILEPATH)->text();

        QFileInfo currentTrackFileInfo(trackFilepath);

        trackTitle = selectedTableWidget->item(i, TRACK_TITLE)->text();
        trackArtist = selectedTableWidget->item(i, TRACK_ARTIST)->text();
        trackAlbum = selectedTableWidget->item(i, TRACK_ALBUM)->text();
        trackGenre = selectedTableWidget->item(i, TRACK_GENRE)->text();
        trackYear = selectedTableWidget->item(i, TRACK_YEAR)->text();

        if(trackTitle.indexOf(query, 0, Qt::CaseInsensitive) != -1
                || trackArtist.indexOf(query, 0, Qt::CaseInsensitive) != -1
                || trackAlbum.indexOf(query, 0, Qt::CaseInsensitive) != -1
                || trackGenre.indexOf(query, 0, Qt::CaseInsensitive) != -1
                || trackYear.indexOf(query, 0, Qt::CaseInsensitive) != -1
                || currentTrackFileInfo.fileName().indexOf(query, 0, Qt::CaseInsensitive) != -1)
        {
            trackNumber = selectedTableWidget->item(i, TRACK_NUMBER)->text();
            trackDuration = selectedTableWidget->item(i, TRACK_DURATION)->text();
            trackBitrate = selectedTableWidget->item(i, TRACK_BITRATE)->text();

            foundPlaylistTracks.append(PlayerTrack(trackNumber.toUInt(), trackBitrate.toInt(), trackYear.toUInt(),
                                                   trackTitle, trackArtist,trackAlbum, trackGenre,
                                                   trackFilepath, trackDuration, false, trackDuration, 0));
        }
    }
    return foundPlaylistTracks;
}

QStringList MPlayer::Domain::Playlist::PlaylistTrackReader::GetAvailablePlaylistTracksInDirectory(const QString &selectedDirectoryPath)
{
    QStringList allTrackFilepaths;
    QDirIterator playlistDirectories(selectedDirectoryPath, QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

    do // Sub folder check!
    {
        playlistDirectories.next();
        allTrackFilepaths.append(GetPlaylistTrackFilepathsForDirectory(playlistDirectories.filePath()));
    }
    while(playlistDirectories.hasNext());

    allTrackFilepaths.append(GetPlaylistTrackFilepathsForDirectory(selectedDirectoryPath)); // Top folder check!

    return allTrackFilepaths;
}

QStringList MPlayer::Domain::Playlist::PlaylistTrackReader::GetPlaylistTrackFilepathsForDirectory(const QString &currentPlaylistAlbumPath)
{
    QString trackFilepath = "";
    QStringList trackFilepaths, fileExtensionFilters;

    QDir currentPlaylistDirectory(currentPlaylistAlbumPath);
    currentPlaylistDirectory.setNameFilters(fileExtensionFilters);
    currentPlaylistDirectory.setFilter(QDir::Files | QDir::NoSymLinks);

    fileExtensionFilters << "*.mp3" << "*.wma" << "*.wav" << "*.ogg" << "*.mp4" << "*.m4a" << "*.flac";

    for(int i = 0; i < currentPlaylistDirectory.entryList().count(); i++)
    {
        trackFilepath = currentPlaylistDirectory.entryList().at(i);

        if(!trackFilepaths.contains(currentPlaylistDirectory.filePath(trackFilepath), Qt::CaseSensitive))
        {
            trackFilepaths.append(currentPlaylistDirectory.filePath(trackFilepath));
        }
    }

    return trackFilepaths;
}
