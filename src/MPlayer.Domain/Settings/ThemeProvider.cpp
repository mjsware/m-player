// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "ThemeProvider.hpp"

MPlayer::Domain::Settings::ThemeProvider::ThemeProvider()
{
    this->themeRepository = std::shared_ptr<IThemeRepository>(new ThemeRepository());
}

QVector<ThemeControl> MPlayer::Domain::Settings::ThemeProvider::GetThemeControlsForForm(const QString &formName)
{
    return this->themeRepository->GetThemeControlsForForm(formName);
}

void MPlayer::Domain::Settings::ThemeProvider::ApplyThemingToForm(QWidget *form, QString formName)
{
    QVector<ThemeControl> themeControls = this->GetThemeControlsForForm(formName);

    for(int i = 0; i < themeControls.count(); i++)
    {
        QWidget *formWidget = form->findChild<QWidget*>(themeControls[i].GetControlName());

        if(formWidget)
        {
            formWidget->setStyleSheet(themeControls[i].GetControlThemeContent());
        }
        else
        {
            form->setStyleSheet(themeControls[i].GetControlThemeContent());
        }
    }
}
