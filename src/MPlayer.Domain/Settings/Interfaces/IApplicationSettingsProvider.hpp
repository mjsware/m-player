// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef IAPPLICATION_SETTINGS_PROVIDER_HPP
#define IAPPLICATION_SETTINGS_PROVIDER_HPP

#include <QString>
#include <QDebug>
#include <memory>

#include "MPlayer.Data/SettingsRepository.hpp"
#include "MPlayer.Entities/Settings/ApplicationSettings.hpp"
#include "MPlayer.Entities/Settings/WindowSettings.hpp"

using namespace MPlayer::Data;
using namespace MPlayer::Entities::Settings;

namespace MPlayer
{
    namespace Domain
    {
        namespace Settings
        {
            class IApplicationSettingsProvider
            {
                public:
                    virtual ~IApplicationSettingsProvider() = 0;
                    virtual ApplicationSettings GetApplicationSettings() = 0;
                    virtual bool GetAutoLoadPlaylist() = 0;
                    virtual QString GetDefaultAudioDriverName() = 0;
                    virtual QString GetDefaultThemeName() = 0;
                    virtual float GetDefaultVolume() = 0;
                    virtual bool GetHideOnStartup() = 0;
                    virtual QString GetLibraryPath() = 0;
                    virtual QString GetLibraryMusicPath() = 0;
                    virtual QString GetMusicNoteFilePath() = 0;
                    virtual QString GetPlaylistsDirectory() = 0;
                    virtual QString GetPlaylistPath() = 0;
                    virtual QString GetProgramIconFilePath() = 0;
                    virtual QStringList GetProgramThemeList() = 0;
                    virtual QString GetProgramThemePath() = 0;
                    virtual WindowSettings GetWindowSettings(const QString &formName) = 0;
                    virtual bool HasSavedWindowSettingsForForm(const QString &formName) = 0;
                    virtual bool ResetApplicationSettings() = 0;
                    virtual bool SaveApplicationSettings(ApplicationSettings &applicationSettings) = 0;
                    virtual void SaveDefaultVolume(const float &defaultVolume) = 0;
                    virtual void SaveWindowSettings(const QString &formName, WindowSettings &windowSettings) = 0;                 
            };
        }
    }
}

#endif // IAPPLICATION_SETTINGS_PROVIDER_HPP
