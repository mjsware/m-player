// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "ApplicationSettingsProvider.hpp"

MPlayer::Domain::Settings::ApplicationSettingsProvider::ApplicationSettingsProvider()
{
    this->settingsRepository = std::shared_ptr<ISettingsRepository>(new SettingsRepository());

    CreateDirectories();
    GetApplicationSettings();
}

float MPlayer::Domain::Settings::ApplicationSettingsProvider::GetDefaultVolume()
{
    return this->applicationSettings.GetDefaultVolume();
}

bool MPlayer::Domain::Settings::ApplicationSettingsProvider::GetHideOnStartup()
{
    return this->applicationSettings.GetHideOnStartup();
}

bool MPlayer::Domain::Settings::ApplicationSettingsProvider::GetAutoLoadPlaylist()
{
    return this->applicationSettings.GetAutoLoadPlaylist();
}

QString MPlayer::Domain::Settings::ApplicationSettingsProvider::GetDefaultAudioDriverName()
{
    return this->applicationSettings.GetDefaultAudioDriverName();
}

QString MPlayer::Domain::Settings::ApplicationSettingsProvider::GetPlaylistPath()
{
    return this->applicationSettings.GetPlaylistPath();
}

QString MPlayer::Domain::Settings::ApplicationSettingsProvider::GetProgramThemePath()
{
    return this->applicationSettings.GetProgramThemePath();
}

QString MPlayer::Domain::Settings::ApplicationSettingsProvider::GetLibraryPath()
{
    return this->applicationSettings.GetLibraryPath();
}

QString MPlayer::Domain::Settings::ApplicationSettingsProvider::GetLibraryMusicPath()
{
    return this->applicationSettings.GetLibraryMusicPath();
}

QString MPlayer::Domain::Settings::ApplicationSettingsProvider::GetMusicNoteFilePath()
{
    return this->applicationSettings.GetMusicNoteFilePath();
}

QString MPlayer::Domain::Settings::ApplicationSettingsProvider::GetProgramIconFilePath()
{
    return this->applicationSettings.GetProgramIconFilePath();
}

QString MPlayer::Domain::Settings::ApplicationSettingsProvider::GetPlaylistsDirectory()
{
    return this->applicationSettings.GetPlaylistsDirectory();
}

QString MPlayer::Domain::Settings::ApplicationSettingsProvider::GetDefaultThemeName()
{
    return this->applicationSettings.GetDefaultThemeName();
}

QStringList MPlayer::Domain::Settings::ApplicationSettingsProvider::GetProgramThemeList()
{
    return this->applicationSettings.GetProgramThemeList();
}

void MPlayer::Domain::Settings::ApplicationSettingsProvider::GetProgramThemes()
{
    QString programThemePath = this->applicationSettings.GetProgramThemePath();
    QStringList programThemes = this->settingsRepository->GetProgramThemes(programThemePath);
    this->applicationSettings.SetProgramThemes(programThemes);

    if(programThemes.count() == 0)
    {
        qDebug() << "An issue occurred while reading the themes file";
    }
}

bool MPlayer::Domain::Settings::ApplicationSettingsProvider::CreateDirectories()
{
    return this->settingsRepository->CreateDirectories(this->applicationSettings);
}

ApplicationSettings MPlayer::Domain::Settings::ApplicationSettingsProvider::GetApplicationSettings()
{
    QString settingsFilePath = this->applicationSettings.GetSettingsFilePath();

    this->applicationSettings = this->settingsRepository->GetApplicationSettings(settingsFilePath);

    if(this->applicationSettings.GetDefaultThemeName().isEmpty())
    {
        qDebug() << "An issue occurred while reading the settings file";
    }
    else
    {
        GetProgramThemes();
    }

    return this->applicationSettings;
}

bool MPlayer::Domain::Settings::ApplicationSettingsProvider::SaveApplicationSettings(ApplicationSettings &applicationSettings)
{
    return this->settingsRepository->SaveApplicationSettings(applicationSettings);
}

bool MPlayer::Domain::Settings::ApplicationSettingsProvider::HasSavedWindowSettingsForForm(const QString &formName)
{
    QSettings appSettings("Matthew James", "M++ Player");
    QStringList appKeys = appSettings.allKeys();

    for(int i = 0; i < appKeys.count(); i++)
    {
        if(appKeys[i].contains(formName))
        {
            return true;
        }
    }
    return false;
}

WindowSettings MPlayer::Domain::Settings::ApplicationSettingsProvider::GetWindowSettings(const QString &formName)
{
    QSettings appSettings("Matthew James", "M++ Player");

    appSettings.beginGroup(formName);

    QByteArray formGeometry = appSettings.value("geometry").toByteArray();
    QByteArray formSaveState = appSettings.value("save_state").toByteArray();
    QPoint formPosition = appSettings.value("pos").toPoint();
    QSize formSize = appSettings.value("size").toSize();
    bool isMaximised = appSettings.value("maximised").toBool();
    int interfaceStatus = appSettings.value("interfaceStatus").toInt();

    appSettings.endGroup();

    return WindowSettings(formGeometry, formSaveState, isMaximised, interfaceStatus, formPosition, formSize);
}

void MPlayer::Domain::Settings::ApplicationSettingsProvider::SaveWindowSettings(const QString &formName, WindowSettings &windowSettings)
{
    QSettings appSettings("Matthew James", "M++ Player");

    appSettings.beginGroup(formName);
    appSettings.setValue("geometry", windowSettings.GetFormGeometry());
    appSettings.setValue("save_state", windowSettings.GetFormState());
    appSettings.setValue("maximised", windowSettings.IsFormMaximised());
    appSettings.setValue("interfaceStatus", windowSettings.GetInterfaceStatus());

    if (!windowSettings.IsFormMaximised())
    {
        appSettings.setValue("pos", windowSettings.GetFormPosition());
        appSettings.setValue("size", windowSettings.GetFormSize());
    }

    appSettings.endGroup();
}

void MPlayer::Domain::Settings::ApplicationSettingsProvider::SaveDefaultVolume(const float &defaultVolume)
{
    ApplicationSettings applicationSettings = GetApplicationSettings();
    applicationSettings.SetDefaultVolume(defaultVolume);

    SaveApplicationSettings(applicationSettings);
}

bool MPlayer::Domain::Settings::ApplicationSettingsProvider::ResetApplicationSettings()
{
    ApplicationSettings applicationSettings = GetApplicationSettings();

    QFile musicLibraryFile(applicationSettings.GetLibraryPath()); // Remove music library file
    musicLibraryFile.remove();

    applicationSettings.SetDefaultTheme("None");
    applicationSettings.SetPlaylistPath("");
    applicationSettings.SetLibraryMusicPath("");
    applicationSettings.SetHideOnStartup(false);
    applicationSettings.SetAutoLoadPlaylist(false);
    applicationSettings.SetDefaultVolume(50);

    return SaveApplicationSettings(applicationSettings);
}

