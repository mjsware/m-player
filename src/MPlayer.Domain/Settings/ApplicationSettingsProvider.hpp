// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef APPLICATION_SETTINGS_PROVIDER_HPP
#define APPLICATION_SETTINGS_PROVIDER_HPP

#include "Interfaces/IApplicationSettingsProvider.hpp"

namespace MPlayer
{
    namespace Domain
    {
        namespace Settings
        {
            class ApplicationSettingsProvider : public IApplicationSettingsProvider
            {
                public:
                    ApplicationSettingsProvider();
                    ApplicationSettings GetApplicationSettings() override;
                    bool GetAutoLoadPlaylist() override;
                    QString GetDefaultAudioDriverName() override;
                    QString GetDefaultThemeName() override;
                    float GetDefaultVolume() override;
                    bool GetHideOnStartup() override;
                    QString GetLibraryPath() override;
                    QString GetLibraryMusicPath() override;
                    QString GetMusicNoteFilePath() override;
                    QString GetPlaylistsDirectory() override;
                    QString GetPlaylistPath() override;
                    QString GetProgramIconFilePath() override;
                    QStringList GetProgramThemeList() override;
                    QString GetProgramThemePath() override;
                    WindowSettings GetWindowSettings(const QString &formName) override;
                    bool HasSavedWindowSettingsForForm(const QString &formName) override;
                    bool ResetApplicationSettings() override;
                    bool SaveApplicationSettings(ApplicationSettings &applicationSettings) override;
                    void SaveDefaultVolume(const float &defaultVolume) override;
                    void SaveWindowSettings(const QString &formName, WindowSettings &windowSettings) override;

                private:
                    bool CreateDirectories();
                    void GetProgramThemes();

                    ApplicationSettings applicationSettings;
                    std::shared_ptr<ISettingsRepository> settingsRepository;
            };
        }
    }
}

#endif // APPLICATION_SETTINGS_PROVIDER_HPP
