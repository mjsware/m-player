// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include <QApplication>

#include "MPlayer.Domain/Settings/ApplicationSettingsProvider.hpp"
#include "MPlayer.UI/FrmMain.hpp"

using namespace MPlayer::Domain::Settings;

void ShowMainForm(FrmMain &mainForm)
{
    std::unique_ptr<IApplicationSettingsProvider> applicationSettingsProvider(new ApplicationSettingsProvider());
    ApplicationSettings applicationSettings = applicationSettingsProvider->GetApplicationSettings();

    if(applicationSettings.GetHideOnStartup()) // Hide or show main window on start up?
    {
        mainForm.hide();
    }
    else
    {
        mainForm.show();
    }
}

int main(int argc, char *argv[])
{
    QApplication mainApp(argc, argv);
    mainApp.setQuitOnLastWindowClosed(false);

    mainApp.setOrganizationName("Matthew James");
    mainApp.setOrganizationDomain("https://gitlab.com/mjsware/");
    mainApp.setApplicationName("M++ Player");
    mainApp.setApplicationVersion("0.4.7");

    FrmMain mainForm;
    ShowMainForm(mainForm);

    return mainApp.exec();
}
