// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include <QtTest>

#include "MPlayer.Domain/Track/TrackShuffler.hpp"

using namespace MPlayer::Domain::Track;

namespace MPlayer
{
    namespace Tests
    {
        namespace Domain
        {
            namespace Track
            {
                class TrackShufflerTests : public QObject
                {
                    Q_OBJECT

                    private slots:
                        void init();
                        void cleanup();

                        // Tests
                        void AddTrackFilepathsForDefaultMode_WithFilepaths_AddsTrackFilepathsToShuffleList();
                        void AddTrackFilepathsForDefaultMode_WithFilepaths_AddsTrackFilepathsInTheCorrectOrder();
                        void AddTrackFilepathsForShuffleMode_WithFilepaths_AddsTrackFilepathsToShuffleList();
                        void AddTrackFilepathsForShuffleMode_WithFilepaths_AddsTrackFilepathsInTheCorrectOrder();
                        void ClearShuffleList_WithTracksInShuffleList_ResetsTrackShuffleList();
                        void GetNextTrackFilepath_WithOneTrackFilepath_ReturnsEmptyFilepath();
                        void GetNextTrackFilepath_WithMultipleTrackFilepaths_ReturnsCorrectFilepath();
                        void GetPreviousTrackFilepath_WithOneTrackFilepath_ReturnsFirstTrackFilepath();
                        void GetPreviousTrackFilepath_WithMultipleTrackFilepathsAndCurrentTrackNumberIncremented_ReturnsCorrectFilepath();
                        void GetShuffleTrackFilepath_WithInvalidIndex_ReturnsEmptyFilepath();
                        void GetShuffleTrackFilepath_WithValidIndex_ReturnsCorrectFilepath();
                        void GetCurrentTrackFilepath_WithOneTrackFilepath_ReturnsPlayingFilepath();
                        void GetCurrentTrackFilepath_WithMultipleTrackFilepaths_ReturnsPlayingFilepath();
                        void RandomiseList_WithMultipleTrackFilepaths_SetsRandomisedState();
                        void TrackShuffler_OnInstantiation_IsSetToDefaultState();

                    private:
                        ITrackShuffler *trackShuffler;
                };
            }
        }
    }
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::init()
{
    this->trackShuffler = new TrackShuffler();
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::cleanup()
{
    if(this->trackShuffler)
    {
        delete this->trackShuffler;
    }
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::AddTrackFilepathsForDefaultMode_WithFilepaths_AddsTrackFilepathsToShuffleList()
{
    // Arrange
    int expectedTrackCount = 3;

    QStringList givenFilepaths;
    givenFilepaths << "/tmp/myFilepath1.mp3" << "/tmp/playing.mp3" << "/tmp/myFilepath3.mp3";
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";

    // Act
    this->trackShuffler->AddTrackFilepathsForDefaultMode(givenFilepaths, givenPlayingTrackFilepath);

    // Assert
    QVERIFY(this->trackShuffler->GetNumberOfTracks() == expectedTrackCount);
    QVERIFY(!this->trackShuffler->GetClearedState());
    QVERIFY(!this->trackShuffler->GetRandomisedState());
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::AddTrackFilepathsForDefaultMode_WithFilepaths_AddsTrackFilepathsInTheCorrectOrder()
{
    // Arrange
    QStringList givenFilepaths;
    givenFilepaths << "/tmp/myFilepath1.mp3" << "/tmp/playing.mp3" << "/tmp/myFilepath3.mp3";

    QString expectedPlayingTrackFilepath = "/tmp/playing.mp3";
    QStringList expectedFilepaths;
    expectedFilepaths << "/tmp/myFilepath1.mp3" << "/tmp/playing.mp3" << "/tmp/myFilepath3.mp3";

    // Act
    this->trackShuffler->AddTrackFilepathsForDefaultMode(givenFilepaths, givenFilepaths[1]);

    // Assert
    QVERIFY(this->trackShuffler->GetShuffleTrackFilepath(0) == expectedFilepaths[0]);
    QVERIFY(this->trackShuffler->GetShuffleTrackFilepath(1) == expectedFilepaths[1]);
    QVERIFY(this->trackShuffler->GetShuffleTrackFilepath(2) == expectedFilepaths[2]);
    QVERIFY(this->trackShuffler->GetCurrentTrackFilepath() == expectedPlayingTrackFilepath);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::AddTrackFilepathsForShuffleMode_WithFilepaths_AddsTrackFilepathsToShuffleList()
{
    // Arrange
    int expectedTrackCount = 3;

    QStringList givenFilepaths;
    givenFilepaths << "/tmp/myFilepath1.mp3" << "/tmp/playing.mp3" << "/tmp/myFilepath3.mp3";
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";

    // Act
    this->trackShuffler->AddTrackFilepathsForShuffleMode(givenFilepaths, givenPlayingTrackFilepath);

    // Assert
    QVERIFY(this->trackShuffler->GetNumberOfTracks() == expectedTrackCount);
    QVERIFY(!this->trackShuffler->GetClearedState());
    QVERIFY(!this->trackShuffler->GetRandomisedState());
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::AddTrackFilepathsForShuffleMode_WithFilepaths_AddsTrackFilepathsInTheCorrectOrder()
{
    // Arrange
    QStringList givenFilepaths;
    givenFilepaths << "/tmp/myFilepath1.mp3" << "/tmp/playing.mp3" << "/tmp/myFilepath3.mp3";

    QString expectedPlayingTrackFilepath = "/tmp/playing.mp3";
    QStringList expectedFilepaths;
    expectedFilepaths << "/tmp/myFilepath1.mp3" << "/tmp/playing.mp3" << "/tmp/myFilepath3.mp3";

    // Act
    this->trackShuffler->AddTrackFilepathsForShuffleMode(givenFilepaths, givenFilepaths[1]);

    // Assert
    QVERIFY(this->trackShuffler->GetShuffleTrackFilepath(0) == expectedFilepaths[1]);
    QVERIFY(this->trackShuffler->GetShuffleTrackFilepath(1) == expectedFilepaths[0]);
    QVERIFY(this->trackShuffler->GetShuffleTrackFilepath(2) == expectedFilepaths[2]);
    QVERIFY(this->trackShuffler->GetCurrentTrackFilepath() == expectedPlayingTrackFilepath);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::ClearShuffleList_WithTracksInShuffleList_ResetsTrackShuffleList()
{
    // Arrange
    QStringList givenFilepaths;
    givenFilepaths << "/tmp/playing.mp3" << "/tmp/myFilepath2.mp3";
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";

    int expectedTrackCount = 0;

    this->trackShuffler->AddTrackFilepathsForDefaultMode(givenFilepaths, givenPlayingTrackFilepath);

    // Act
    this->trackShuffler->ClearShuffleList();

    // Assert
    QVERIFY(this->trackShuffler->GetNumberOfTracks() == expectedTrackCount);
    QVERIFY(this->trackShuffler->GetClearedState());
    QVERIFY(!this->trackShuffler->GetRandomisedState());
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::GetNextTrackFilepath_WithOneTrackFilepath_ReturnsEmptyFilepath()
{
    // Arrange
    int expectedTrackCount = 1;
    QString givenFilepath = "/tmp/playing.mp3";
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";

    this->trackShuffler->AddTrackFilepathsForDefaultMode(QStringList(givenFilepath), givenPlayingTrackFilepath);

    // Act
    QString returnedFilepath = this->trackShuffler->GetNextTrackFilepath();

    // Assert
    QVERIFY(returnedFilepath.isEmpty());
    QVERIFY(this->trackShuffler->GetNumberOfTracks() == expectedTrackCount);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::GetNextTrackFilepath_WithMultipleTrackFilepaths_ReturnsCorrectFilepath()
{
    // Arrange
    QStringList givenFilepaths;
    givenFilepaths << "/tmp/myFilepath1.mp3" << "/tmp/playing.mp3" << "/tmp/myFilepath3.mp3";
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";

    QString expectedFilepath = "/tmp/myFilepath3.mp3";

    int expectedTrackCount = givenFilepaths.count();

    this->trackShuffler->AddTrackFilepathsForDefaultMode(givenFilepaths, givenPlayingTrackFilepath);

    // Act
    QString returnedFilepath = this->trackShuffler->GetNextTrackFilepath();

    // Assert
    QVERIFY(returnedFilepath == expectedFilepath);
    QVERIFY(this->trackShuffler->GetNumberOfTracks() == expectedTrackCount);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::GetPreviousTrackFilepath_WithOneTrackFilepath_ReturnsFirstTrackFilepath()
{
    // Arrange
    int expectedTrackCount = 1;
    QString expectedFilepath = "/tmp/playing.mp3";
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";

    this->trackShuffler->AddTrackFilepathsForDefaultMode(QStringList(givenPlayingTrackFilepath), givenPlayingTrackFilepath);

    // Act
    QString returnedFilepath = this->trackShuffler->GetPreviousTrackFilepath();

    // Assert
    QVERIFY(returnedFilepath == expectedFilepath);
    QVERIFY(this->trackShuffler->GetNumberOfTracks() == expectedTrackCount);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::GetPreviousTrackFilepath_WithMultipleTrackFilepathsAndCurrentTrackNumberIncremented_ReturnsCorrectFilepath()
{
    // Arrange
    QString expectedFilepath = "/tmp/myFilepath2.mp3";

    QStringList givenFilepaths;
    givenFilepaths << "/tmp/playing.mp3" << "/tmp/myFilepath2.mp3" << "/tmp/myFilepath3.mp3";

    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";

    int expectedTrackCount = givenFilepaths.count();

    this->trackShuffler->AddTrackFilepathsForDefaultMode(givenFilepaths,givenPlayingTrackFilepath);

    this->trackShuffler->GetNextTrackFilepath(); // Get next track filepath to increment current track number
    this->trackShuffler->GetNextTrackFilepath(); // Get next track filepath to increment current track number

    // Act
    QString returnedFilepath = this->trackShuffler->GetPreviousTrackFilepath();

    // Assert
    QVERIFY(returnedFilepath == expectedFilepath);
    QVERIFY(this->trackShuffler->GetNumberOfTracks() == expectedTrackCount);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::GetShuffleTrackFilepath_WithInvalidIndex_ReturnsEmptyFilepath()
{
    // Arrange
    int givenInvalidIndex = 5224;
    QStringList givenFilepaths;
    givenFilepaths << "/tmp/playing.mp3" << "/tmp/myFilepath2.mp3" << "/tmp/myFilepath3.mp3";

    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";

    this->trackShuffler->AddTrackFilepathsForDefaultMode(givenFilepaths, givenPlayingTrackFilepath);

    // Act
    QString returnedTrackFilepath = this->trackShuffler->GetShuffleTrackFilepath(givenInvalidIndex);

    // Assert
    QVERIFY(returnedTrackFilepath.isEmpty());
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::GetShuffleTrackFilepath_WithValidIndex_ReturnsCorrectFilepath()
{
    // Arrange
    int givenValidIndex = 1;
    QStringList givenFilepaths;
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";
    givenFilepaths << "/tmp/playing.mp3" << "/tmp/myFilepath2.mp3" << "/tmp/myFilepath3.mp3";

    QString expectedFilepath = "/tmp/myFilepath2.mp3";

    this->trackShuffler->AddTrackFilepathsForDefaultMode(givenFilepaths, givenPlayingTrackFilepath);

    // Act
    QString returnedTrackFilepath = this->trackShuffler->GetShuffleTrackFilepath(givenValidIndex);

    // Assert
    QVERIFY(returnedTrackFilepath == expectedFilepath);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::GetCurrentTrackFilepath_WithOneTrackFilepath_ReturnsPlayingFilepath()
{
    // Arrange
    QStringList givenFilepaths;
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";
    QString expectedFilepath = "/tmp/playing.mp3";

    this->trackShuffler->AddTrackFilepathsForDefaultMode(QStringList(givenPlayingTrackFilepath), givenPlayingTrackFilepath);

    // Act
    QString returnedTrackFilepath = this->trackShuffler->GetCurrentTrackFilepath();

    // Assert
    QVERIFY(returnedTrackFilepath == expectedFilepath);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::GetCurrentTrackFilepath_WithMultipleTrackFilepaths_ReturnsPlayingFilepath()
{
    // Arrange
    QStringList givenFilepaths;
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";
    givenFilepaths << "/tmp/myFilepath1.mp3" << "/tmp/myFilepath2.mp3" << "/tmp/playing.mp3";

    QString expectedFilepath = "/tmp/playing.mp3";

    this->trackShuffler->AddTrackFilepathsForDefaultMode(givenFilepaths, givenPlayingTrackFilepath);

    // Act
    QString returnedTrackFilepath = this->trackShuffler->GetCurrentTrackFilepath();

    // Assert
    QVERIFY(returnedTrackFilepath == expectedFilepath);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::RandomiseList_WithMultipleTrackFilepaths_SetsRandomisedState()
{
    // Arrange
    QStringList givenFilepaths;
    QString givenPlayingTrackFilepath = "/tmp/playing.mp3";
    givenFilepaths << "/tmp/myFilepath1.mp3" << "/tmp/myFilepath2.mp3" << "/tmp/myFilepath3.mp3";

    int expectedTrackCount = givenFilepaths.count();

    this->trackShuffler->AddTrackFilepathsForDefaultMode(givenFilepaths, givenPlayingTrackFilepath);

    // Act
    this->trackShuffler->RandomiseList();

    // Assert
    QVERIFY(this->trackShuffler->GetRandomisedState());
    QVERIFY(this->trackShuffler->GetNumberOfTracks() == expectedTrackCount);
}

void MPlayer::Tests::Domain::Track::TrackShufflerTests::TrackShuffler_OnInstantiation_IsSetToDefaultState()
{
    // Arrange
    ITrackShuffler *trackShufflerInstance;

    // Act
    trackShufflerInstance = new TrackShuffler();

    // Assert
    QVERIFY(trackShufflerInstance->GetNumberOfTracks() == 0);
    QVERIFY(trackShufflerInstance->GetClearedState());
    QVERIFY(!trackShufflerInstance->GetRandomisedState());

    delete trackShufflerInstance;
}

QTEST_APPLESS_MAIN(MPlayer::Tests::Domain::Track::TrackShufflerTests)
#include "TrackShufflerTests.moc"
