# This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
# Copyright © Matthew James
# "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase c++11
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH  += $$PWD/../../musicPlayerQt/

LIBS += $$PWD/../../build-musicPlayerQt-Qt_5_11_3-Debug/TrackShuffler.o \
        $$PWD/../../build-musicPlayerQt-Qt_5_11_3-Debug/ITrackShuffler.o


SOURCES += \
    Domain/Track/TrackShufflerTests.cpp \
