# This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
# Copyright © Matthew James 
# "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#-------------------------------------------------
#
# Project created by QtCreator 2013-11-06T21:03:30
#
#-------------------------------------------------

QT       += core gui widgets webkitwidgets xml

TARGET = musicPlayerQt
TEMPLATE = app

SOURCES += main.cpp \
    MPlayer.Data/Interfaces/ILibraryRepository.cpp \
    MPlayer.Data/LibraryRepository.cpp \
    MPlayer.Data/Interfaces/IPlayerRepository.cpp \
    MPlayer.Data/PlayerRepository.cpp \
    MPlayer.Data/Interfaces/IPlaylistRepository.cpp \
    MPlayer.Data/PlaylistRepository.cpp \
    MPlayer.Data/Interfaces/ISettingsRepository.cpp \
    MPlayer.Data/SettingsRepository.cpp \
    MPlayer.Data/Interfaces/IThemeRepository.cpp \
    MPlayer.Data/ThemeRepository.cpp \
    MPlayer.Domain/Window/Interfaces/IWindowManager.cpp \
    MPlayer.Domain/Window/WindowManager.cpp \
    MPlayer.Domain/Track/Interfaces/ITrackShuffler.cpp \
    MPlayer.Domain/Track/TrackShuffler.cpp \
    MPlayer.Domain/Settings/Interfaces/IThemeProvider.cpp \
    MPlayer.Domain/Settings/ThemeProvider.cpp \
    MPlayer.Domain/Player/Interfaces/IPlayerStatusReader.cpp \
    MPlayer.Domain/Player/PlayerStatusReader.cpp \
    MPlayer.Domain/Track/Interfaces/IAlbumArtReader.cpp \
    MPlayer.Domain/Track/AlbumArtReader.cpp \
    MPlayer.Domain/Player/Interfaces/IPlayerService.cpp \
    MPlayer.Domain/Player/PlayerService.cpp \
    MPlayer.Domain/Settings/Interfaces/IApplicationSettingsProvider.cpp \
    MPlayer.Domain/Settings/ApplicationSettingsProvider.cpp \
    MPlayer.Entities/Settings/ApplicationSettings.cpp \
    MPlayer.Entities/Settings/SessionSettings.cpp \
    MPlayer.Entities/Settings/ThemeControl.cpp \
    MPlayer.Entities/Settings/WindowSettings.cpp \
    MPlayer.Entities/Track/PlayerTrack.cpp \
    MPlayer.UI/FrmApplicationSettings.cpp \
    MPlayer.UI/FrmAbout.cpp \
    MPlayer.UI/FrmMain.cpp \
    MPlayer.UI/FrmMain_Events.cpp \
    MPlayer.UI/FrmTrackProperties.cpp \
    MPlayer.UI/FrmTrackSearch.cpp \
    MPlayer.UI/PlaylistTableWidget.cpp \
    MPlayer.Domain/Library/Interfaces/ILibraryGenerator.cpp \
    MPlayer.Domain/Library/LibraryGenerator.cpp \
    MPlayer.Domain/Library/Interfaces/ILibraryTrackReader.cpp \
    MPlayer.Domain/Library/Interfaces/ILibraryTrackWriter.cpp \
    MPlayer.Domain/Library/LibraryTrackReader.cpp \
    MPlayer.Domain/Library/LibraryTrackWriter.cpp \
    MPlayer.Domain/Playlist/Interfaces/IPlaylistTrackReader.cpp \
    MPlayer.Domain/Playlist/Interfaces/IPlaylistTrackWriter.cpp \
    MPlayer.Domain/Playlist/PlaylistTrackReader.cpp \
    MPlayer.Domain/Playlist/PlaylistTrackWriter.cpp \
    MPlayer.Domain/Track/TrackGridReader.cpp \
    MPlayer.Domain/Track/TrackGridWriter.cpp \
    MPlayer.Domain/Track/Interfaces/ITrackGridReader.cpp \
    MPlayer.Domain/Track/Interfaces/ITrackGridWriter.cpp

HEADERS  += \
    MPlayer.Data/Interfaces/ILibraryRepository.hpp \
    MPlayer.Data/LibraryRepository.hpp \
    MPlayer.Data/Interfaces/IPlayerRepository.hpp \
    MPlayer.Data/PlayerRepository.hpp \
    MPlayer.Data/Interfaces/IPlaylistRepository.hpp \
    MPlayer.Data/PlaylistRepository.hpp \
    MPlayer.Data/Interfaces/ISettingsRepository.hpp \
    MPlayer.Data/SettingsRepository.hpp \
    MPlayer.Data/Interfaces/IThemeRepository.hpp \
    MPlayer.Data/ThemeRepository.hpp \
    MPlayer.Domain/Window/Interfaces/IWindowManager.hpp \
    MPlayer.Domain/Window/WindowManager.hpp \
    MPlayer.Domain/Track/Interfaces/ITrackShuffler.hpp \
    MPlayer.Domain/Track/TrackShuffler.hpp \
    MPlayer.Domain/Settings/Interfaces/IThemeProvider.hpp \
    MPlayer.Domain/Settings/ThemeProvider.hpp \
    MPlayer.Domain/Player/Interfaces/IPlayerStatusReader.hpp \
    MPlayer.Domain/Player/PlayerStatusReader.hpp \
    MPlayer.Domain/Track/Interfaces/IAlbumArtReader.hpp \
    MPlayer.Domain/Track/AlbumArtReader.hpp \
    MPlayer.Domain/Player/Interfaces/IPlayerService.hpp \
    MPlayer.Domain/Player/PlayerService.hpp \
    MPlayer.Domain/Settings/Interfaces/IApplicationSettingsProvider.hpp \
    MPlayer.Domain/Settings/ApplicationSettingsProvider.hpp \
    MPlayer.Entities/Player/PlayerInterfaceStatus.hpp \
    MPlayer.Entities/Player/PlayerTab.hpp \
    MPlayer.Entities/Settings/ApplicationSettings.hpp \
    MPlayer.Entities/Settings/SessionSettings.hpp \
    MPlayer.Entities/Settings/ThemeControl.hpp \
    MPlayer.Entities/Settings/WindowSettings.hpp \
    MPlayer.Entities/Track/PlayerTrack.hpp \
    MPlayer.Entities/Track/TrackColumn.hpp \
    MPlayer.Entities/Track/TrackPropertiesTab.hpp \
    MPlayer.UI/FrmApplicationSettings.hpp \
    MPlayer.UI/FrmAbout.hpp \
    MPlayer.UI/FrmMain.hpp \
    MPlayer.UI/FrmTrackProperties.hpp \
    MPlayer.UI/FrmTrackSearch.hpp \
    MPlayer.UI/PlaylistTableWidget.hpp \
    MPlayer.Domain/Library/LibraryGenerator.hpp \
    MPlayer.Domain/Library/Interfaces/ILibraryGenerator.hpp \
    MPlayer.Domain/Library/LibraryTrackReader.hpp \
    MPlayer.Domain/Library/LibraryTrackWriter.hpp \
    MPlayer.Domain/Library/Interfaces/ILibraryTrackReader.hpp \
    MPlayer.Domain/Library/Interfaces/ILibraryTrackWriter.hpp \
    MPlayer.Domain/Playlist/Interfaces/IPlaylistTrackReader.hpp \
    MPlayer.Domain/Playlist/Interfaces/IPlaylistTrackWriter.hpp \
    MPlayer.Domain/Playlist/PlaylistTrackReader.hpp \
    MPlayer.Domain/Playlist/PlaylistTrackWriter.hpp \
    MPlayer.Domain/Track/TrackGridReader.hpp \
    MPlayer.Domain/Track/TrackGridWriter.hpp \
    MPlayer.Domain/Track/Interfaces/ITrackGridReader.hpp \
    MPlayer.Domain/Track/Interfaces/ITrackGridWriter.hpp \
    MPlayer.Entities/Track/TrackWebContentType.hpp \
    MPlayer.Entities/Track/ShuffleMode.hpp

FORMS    += \
    MPlayer.UI/FrmTrackSearch.ui \
    MPlayer.UI/FrmMain.ui \
    MPlayer.UI/FrmApplicationSettings.ui \
    MPlayer.UI/FrmAbout.ui \
    MPlayer.UI/FrmTrackProperties.ui

DESTDIR = ../bin
MOC_DIR = ../build/moc
RCC_DIR = ../build/rcc
UI_DIR = ../build/ui
unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32
macx:OBJECTS_DIR = ../build/o/mac

LIBS += -ltag_c -lao -lavformat -lavcodec -lavutil -lswresample

# -lGL library: libgl1-mesa-dev libgl1-mesa-glx
