// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "LibraryRepository.hpp"

MPlayer::Data::LibraryRepository::LibraryRepository()
{
    this->isGenerated = false;
    this->percentageGenerated = 0;
    this->applicationSettingsProvider = std::shared_ptr<IApplicationSettingsProvider>(new ApplicationSettingsProvider);
}

bool MPlayer::Data::LibraryRepository::IsGenerated()
{
    return this->isGenerated;
}

double MPlayer::Data::LibraryRepository::GetLibraryPercentageGenerated()
{
    return this->percentageGenerated;
}

QVector <PlayerTrack> MPlayer::Data::LibraryRepository::GetGeneratedLibraryTracks()
{
    return this->generatedLibraryTracks;
}

QStringList MPlayer::Data::LibraryRepository::GetLibraryTrackFilepathsForDirectory(const QString &currentLibraryAlbumPath)
{
    QString trackFilepath = "";
    QStringList trackFilepaths, fileExtensionFilters;

    fileExtensionFilters << "*.mp3" << "*.wma" << "*.wav" << "*.ogg" << "*.mp4" << "*.m4a" << "*.flac";

    QDir currentLibraryDirectory(currentLibraryAlbumPath);
    currentLibraryDirectory.setNameFilters(fileExtensionFilters);
    currentLibraryDirectory.setFilter(QDir::Files | QDir::NoSymLinks);

    for(int i = 0; i < currentLibraryDirectory.entryList().count(); i++)
    {
        trackFilepath = currentLibraryDirectory.entryList().at(i);

        if(!trackFilepaths.contains(currentLibraryDirectory.filePath(trackFilepath), Qt::CaseSensitive))
        {
            trackFilepaths.append(currentLibraryDirectory.filePath(trackFilepath));
        }
    }

    return trackFilepaths;
}

void MPlayer::Data::LibraryRepository::run()
{
    QStringList foundTrackFilepaths;

    this->isGenerated = false;
    this->percentageGenerated = 0;
    this->generatedLibraryTracks = QVector<PlayerTrack>();

    ApplicationSettings applicationSettings = this->applicationSettingsProvider->GetApplicationSettings();

    if(applicationSettings.GetLibraryMusicPath().length() > 0) // Get list of music files in library location
    {
        QDirIterator libraryDirectories(applicationSettings.GetLibraryMusicPath(), QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

        do // Sub folder check!
        {
            libraryDirectories.next();
            foundTrackFilepaths.append(GetLibraryTrackFilepathsForDirectory(libraryDirectories.filePath()));
        }
        while(libraryDirectories.hasNext());

        foundTrackFilepaths.append(GetLibraryTrackFilepathsForDirectory(applicationSettings.GetLibraryMusicPath())); // Top folder check!

        if(foundTrackFilepaths.count() > 0) // Got music track list, now get the details of each track
        {
            TagLib_Tag *tags = nullptr;
            TagLib_File *mediaFile = nullptr;
            QString currentDuration, currentShortDuration;
            const TagLib_AudioProperties *properties = nullptr;
            time_t parsedSeconds;
            tm *currentTime = nullptr;
            std::unique_ptr<QFile> musicFile;

            taglib_set_strings_unicode(1);

            for(int i = 0; i < foundTrackFilepaths.length(); i++)
            {
                this->percentageGenerated = (static_cast<double>(i) / static_cast<double>(foundTrackFilepaths.length())) * 100;

                PlayerTrack currentTrack(foundTrackFilepaths[i]);
                musicFile = std::unique_ptr<QFile>(new QFile(currentTrack.GetTrackFilepath()));
                mediaFile = taglib_file_new(currentTrack.GetTrackFilepath().toStdString().c_str());

                if(mediaFile != nullptr && musicFile != nullptr)
                {
                    tags = taglib_file_tag(mediaFile);
                    properties = taglib_file_audioproperties(mediaFile);

                    if(tags != nullptr)
                    {
                        currentTrack.SetTrackNumber(taglib_tag_track(tags));
                        currentTrack.SetTrackTitle(taglib_tag_title(tags));
                        currentTrack.SetTrackArtist(taglib_tag_artist(tags));
                        currentTrack.SetTrackAlbum(taglib_tag_album(tags));
                        currentTrack.SetTrackGenre(taglib_tag_genre(tags));
                        currentTrack.SetTrackYear(taglib_tag_year(tags));
                        currentTrack.SetTrackFilesize(musicFile->size());

                        if(properties != nullptr)
                        {
                            // Calculate time by number of seconds
                            parsedSeconds = taglib_audioproperties_length(properties);
                            currentTime = gmtime(&parsedSeconds); // Convert to broken down time
                            currentDuration.sprintf("%02i:%02i:%02i", currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec);

                            if(currentTime->tm_hour > 0)
                            {
                                currentShortDuration.sprintf("%02i:%02i:%02i", currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec);
                            }
                            else
                            {
                                currentShortDuration.sprintf("%02i:%02i", currentTime->tm_min, currentTime->tm_sec);
                            }

                            currentTrack.SetTrackDuration(currentDuration);
                            currentTrack.SetTrackShortDuration(currentShortDuration);
                            currentTrack.SetTrackBitrate(taglib_audioproperties_bitrate(properties));

                            this->generatedLibraryTracks.append(currentTrack);
                        }
                    }
                    musicFile->close();
                }

                taglib_tag_free_strings();
                taglib_file_free(mediaFile);
            }
        }

        // Grabbed details of every track, now create a library file with track details
        if(this->generatedLibraryTracks.count() > 0)
        {
            AddAllLibraryTracks(this->generatedLibraryTracks);
        }

        this->percentageGenerated = 100;
        this->isGenerated = true;
    }
}

void MPlayer::Data::LibraryRepository::AddAllLibraryTracks(QVector <PlayerTrack> selectedLibraryTracks)
{
    QFile libraryFile(this->applicationSettingsProvider->GetLibraryPath());

    if (libraryFile.exists())
    {
        libraryFile.remove();
    }

    libraryFile.close();

    if(libraryFile.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter writer(&libraryFile);

        writer.setAutoFormatting(true);
        writer.writeStartDocument();
        writer.writeStartElement("Library");
        writer.writeStartElement("Tracks");

        for(int i = 0; i < selectedLibraryTracks.count(); i++)
        {
            if(selectedLibraryTracks[i].GetTrackFilepath().length() > 0)
            {
                writer.writeStartElement("Track");
                writer.writeAttribute("TrackNumber", QString::number(selectedLibraryTracks[i].GetTrackNumber()));
                writer.writeAttribute("TrackArtist", selectedLibraryTracks[i].GetTrackArtist().length() > 0 ? selectedLibraryTracks[i].GetTrackArtist(): "Unknown");
                writer.writeAttribute("TrackAlbum", selectedLibraryTracks[i].GetTrackAlbum().length() > 0 ? selectedLibraryTracks[i].GetTrackAlbum(): "Unknown");
                writer.writeAttribute("TrackTitle", selectedLibraryTracks[i].GetTrackTitle().length() > 0 ? selectedLibraryTracks[i].GetTrackTitle(): "Unknown");
                writer.writeAttribute("TrackGenre", selectedLibraryTracks[i].GetTrackGenre().length() > 0 ? selectedLibraryTracks[i].GetTrackGenre(): "Unknown");
                writer.writeAttribute("TrackYear", QString::number(selectedLibraryTracks[i].GetTrackYear()));
                writer.writeAttribute("TrackBitrate", QString::number(selectedLibraryTracks[i].GetTrackBitrate()));
                writer.writeAttribute("TrackDuration", selectedLibraryTracks[i].GetTrackDuration());
                writer.writeAttribute("TrackShortDuration", selectedLibraryTracks[i].GetTrackShortDuration());
                writer.writeAttribute("TrackFile", selectedLibraryTracks[i].GetTrackFilepath());
                writer.writeAttribute("TrackPlaylistAdded", "0");
                writer.writeAttribute("TrackFilesize", QString::number(selectedLibraryTracks[i].GetTrackFilesize()));
                writer.writeEndElement();
            }
        }

        writer.writeEndElement();
        writer.writeEndDocument();

        libraryFile.flush();
        libraryFile.close();
    }
}

QVector <PlayerTrack> MPlayer::Data::LibraryRepository::GetLibraryTracksByQuery(const QString &query)
{
    QVector <PlayerTrack> filteredLibraryTracks;
    QFile libraryFile(this->applicationSettingsProvider->GetLibraryPath());

    if(libraryFile.exists() && libraryFile.open(QIODevice::ReadOnly)) // Open the xml file with read only permissions
    {
        QXmlStreamReader reader(&libraryFile);
        QXmlStreamReader::TokenType outerToken;
        PlayerTrack currentTrack;

        while(!reader.atEnd()) // Read until the end
        {
            outerToken = reader.readNext();

            if(outerToken == QXmlStreamReader::StartElement && reader.name() == "Track")
            {
                while(!reader.atEnd())
                {
                    if(reader.attributes().hasAttribute("TrackNumber"))
                    {
                        currentTrack.SetTrackNumber(reader.attributes().value("TrackNumber").toUInt());
                    }

                    if(reader.attributes().hasAttribute("TrackArtist"))
                    {
                        currentTrack.SetTrackArtist(reader.attributes().value("TrackArtist").toString());
                    }

                    if(reader.attributes().hasAttribute("TrackAlbum"))
                    {
                        currentTrack.SetTrackAlbum(reader.attributes().value("TrackAlbum").toString());
                    }

                    if(reader.attributes().hasAttribute("TrackTitle"))
                    {
                        currentTrack.SetTrackTitle(reader.attributes().value("TrackTitle").toString());
                    }

                    if(reader.attributes().hasAttribute("TrackGenre"))
                    {
                        currentTrack.SetTrackGenre(reader.attributes().value("TrackGenre").toString());
                    }

                    if(reader.attributes().hasAttribute("TrackYear"))
                    {
                        currentTrack.SetTrackYear(reader.attributes().value("TrackYear").toUInt());
                    }

                    if(reader.attributes().hasAttribute("TrackBitrate"))
                    {
                        currentTrack.SetTrackBitrate(reader.attributes().value("TrackBitrate").toInt());
                    }

                    if(reader.attributes().hasAttribute("TrackDuration"))
                    {
                        currentTrack.SetTrackDuration(reader.attributes().value("TrackDuration").toString());
                    }

                    if(reader.attributes().hasAttribute("TrackShortDuration"))
                    {
                        currentTrack.SetTrackShortDuration(reader.attributes().value("TrackShortDuration").toString());
                    }

                    if(reader.attributes().hasAttribute("TrackFilesize"))
                    {
                        currentTrack.SetTrackFilesize(reader.attributes().value("TrackFilesize").toLong());
                    }

                    if(reader.attributes().hasAttribute("TrackPlaylistAdded"))
                    {
                        currentTrack.SetTrackPlaylistAddedStatus(reader.attributes().value("TrackPlaylistAdded").toInt());
                    }

                    if(reader.attributes().hasAttribute("TrackFile"))
                    {
                        currentTrack.SetTrackFilepath(reader.attributes().value("TrackFile").toString());

                        QFileInfo currentTrackFileInfo(currentTrack.GetTrackFilepath());

                        if(currentTrack.GetTrackArtist().indexOf(query, 0, Qt::CaseInsensitive) != -1
                                || currentTrack.GetTrackAlbum().indexOf(query, 0, Qt::CaseInsensitive) != -1
                                || currentTrack.GetTrackTitle().indexOf(query, 0, Qt::CaseInsensitive) != -1
                                || currentTrack.GetTrackGenre().indexOf(query, 0, Qt::CaseInsensitive) != -1
                                || currentTrackFileInfo.fileName().indexOf(query, 0, Qt::CaseInsensitive) != -1
                                || QString::number(currentTrack.GetTrackYear()).indexOf(query, 0, Qt::CaseInsensitive) != -1
                                || query.isEmpty())
                        {
                            filteredLibraryTracks.append(currentTrack);
                        }
                    }

                    reader.readNext();
                }
            }
        }

        if (reader.hasError())
        {
            qDebug() << reader.errorString();
            qDebug() << "at line " << reader.lineNumber() << ", column " << reader.columnNumber();
            qDebug() << "Error type " << reader.error();
        }
    }

    libraryFile.close(); // Close the reader
    return filteredLibraryTracks;
}

void MPlayer::Data::LibraryRepository::AddSelectedLibraryTracks(QVector<PlayerTrack> selectedLibraryTracks)
{
    QDomDocument libraryXMLDoc;

    QFile libraryFile(this->applicationSettingsProvider->GetLibraryPath());

    if(libraryFile.exists() && libraryFile.open(QIODevice::ReadWrite)) // Open the xml file with read and write permissions
    {
        QDomElement trackDomNode;
        QTextStream libraryIOStream(&libraryFile);

        libraryXMLDoc.setContent(libraryIOStream.readAll());

        QDomNode parentTracksNode = libraryXMLDoc.elementsByTagName("Tracks").at(0);

        for (int i = 0; i < selectedLibraryTracks.count(); i++)
        {
            trackDomNode = libraryXMLDoc.createElement("Track");
            trackDomNode.setAttribute("TrackNumber", QString::number(selectedLibraryTracks[i].GetTrackNumber()));
            trackDomNode.setAttribute("TrackArtist", selectedLibraryTracks[i].GetTrackArtist());
            trackDomNode.setAttribute("TrackAlbum", selectedLibraryTracks[i].GetTrackAlbum());
            trackDomNode.setAttribute("TrackTitle", selectedLibraryTracks[i].GetTrackTitle());
            trackDomNode.setAttribute("TrackGenre", selectedLibraryTracks[i].GetTrackGenre());
            trackDomNode.setAttribute("TrackYear", QString::number(selectedLibraryTracks[i].GetTrackYear()));
            trackDomNode.setAttribute("TrackBitrate", QString::number(selectedLibraryTracks[i].GetTrackBitrate()));
            trackDomNode.setAttribute("TrackDuration", selectedLibraryTracks[i].GetTrackDuration());
            trackDomNode.setAttribute("TrackShortDuration", selectedLibraryTracks[i].GetTrackShortDuration());
            trackDomNode.setAttribute("TrackFile", selectedLibraryTracks[i].GetTrackFilepath());
            trackDomNode.setAttribute("TrackPlaylistAdded", "1");
            parentTracksNode.appendChild(trackDomNode);
        }

        libraryFile.close();
        libraryFile.open(QIODevice::WriteOnly);
        libraryFile.write(libraryXMLDoc.toByteArray());
    }

    libraryFile.close();
}

void MPlayer::Data::LibraryRepository::UpdateLibraryTrack(PlayerTrack &selectedLibraryTrack)
{
    QDomDocument libraryXMLDoc;
    QFile libraryFile(this->applicationSettingsProvider->GetLibraryPath());

    if(libraryFile.exists() && libraryFile.open(QIODevice::ReadWrite)) // Open the xml file with read and write permissions
    {
        QString trackFilepath;
        QTextStream libraryIOStream(&libraryFile);

        libraryXMLDoc.setContent(libraryIOStream.readAll());

        QDomNodeList trackNodes = libraryXMLDoc.elementsByTagName("Track");

        for (int i = 0; i < trackNodes.count(); i++)
        {
            if(libraryXMLDoc.elementsByTagName("Track").at(i).hasAttributes())
            {
                trackFilepath = libraryXMLDoc.elementsByTagName("Track").at(i).attributes().namedItem("TrackFile").nodeValue();

                if(trackFilepath == selectedLibraryTrack.GetTrackFilepath())
                {
                    // Set Track Number
                    UpdateLibraryTrackAttribute(libraryXMLDoc, i, "TrackNumber", QString::number(selectedLibraryTrack.GetTrackNumber()));

                    // Set Track Artist
                    UpdateLibraryTrackAttribute(libraryXMLDoc, i, "TrackArtist", selectedLibraryTrack.GetTrackArtist());

                    // Set Track Album
                    UpdateLibraryTrackAttribute(libraryXMLDoc, i, "TrackAlbum", selectedLibraryTrack.GetTrackAlbum());

                    // Set Track Title
                    UpdateLibraryTrackAttribute(libraryXMLDoc, i, "TrackTitle", selectedLibraryTrack.GetTrackTitle());

                    // Set Track Genre
                    UpdateLibraryTrackAttribute(libraryXMLDoc, i, "TrackGenre", selectedLibraryTrack.GetTrackGenre());

                    // Set Track Year
                    UpdateLibraryTrackAttribute(libraryXMLDoc, i, "TrackYear", QString::number(selectedLibraryTrack.GetTrackYear()));
                    break;
                }
            }
        }

        libraryFile.close();
        libraryFile.open(QIODevice::WriteOnly);
        libraryFile.write(libraryXMLDoc.toByteArray());
    }

    libraryFile.close();
}

void MPlayer::Data::LibraryRepository::UpdateLibraryTrackAttribute(QDomDocument libraryXMLDoc, const int &index, const QString &trackAttributeName, const QString &trackAttributeValue)
{
    QDomNode trackAttribute = libraryXMLDoc.elementsByTagName("Track").at(index).attributes().namedItem(trackAttributeName);
    trackAttribute.setNodeValue(trackAttributeValue);

    libraryXMLDoc.elementsByTagName("Track").at(index).attributes().removeNamedItem(trackAttributeName);
    libraryXMLDoc.elementsByTagName("Track").at(index).attributes().setNamedItem(trackAttribute);
}

void MPlayer::Data::LibraryRepository::RemoveLibraryTrack(const QString &trackFilepath)
{
    QDomDocument libraryXMLDoc;
    QFile libraryFile(this->applicationSettingsProvider->GetLibraryPath());

    if(libraryFile.exists() && libraryFile.open(QIODevice::ReadWrite)) // Open the xml file with read and write permissions
    {
        QString currentTrackFilepath;
        QDomNode trackAttribute;
        QTextStream libraryIOStream(&libraryFile);

        libraryXMLDoc.setContent(libraryIOStream.readAll());

        QDomNode parentTracksNode = libraryXMLDoc.elementsByTagName("Tracks").at(0);
        QDomNodeList trackNodes = libraryXMLDoc.elementsByTagName("Track");

        for (int i = 0; i < trackNodes.count(); i++)
        {
            if(libraryXMLDoc.elementsByTagName("Track").at(i).hasAttributes())
            {
                currentTrackFilepath = libraryXMLDoc.elementsByTagName("Track").at(i).attributes().namedItem("TrackFile").nodeValue();

                if(currentTrackFilepath == trackFilepath)
                {
                    parentTracksNode.removeChild(libraryXMLDoc.elementsByTagName("Track").at(i));
                    break;
                }
            }
        }

        libraryFile.close();
        libraryFile.open(QIODevice::WriteOnly);
        libraryFile.write(libraryXMLDoc.toByteArray());
    }

    libraryFile.close();
}
