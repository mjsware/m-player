// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef PLAYER_REPOSITORY_HPP
#define PLAYER_REPOSITORY_HPP

#include "Interfaces/IPlayerRepository.hpp"

namespace MPlayer
{
    namespace Data
    {
        class PlayerRepository : public IPlayerRepository
        {
            public:
                PlayerRepository(const float &volume, const QString &defaultAudioDriverName);
                ~PlayerRepository() override;
                int64_t GetCurrentDuration() override;
                QString GetFileFormat() override;
                int64_t GetTotalDuration() override;
                QString GetTrackFilepath() override;
                bool IsPaused() override;
                bool IsPlaying() override;
                bool IsStopped() override;
                void run() override;
                void PauseTrack() override;
                void PlayTrack() override;
                void SetPosition(const int64_t &durationPos) override;
                void SetTrackFilepath(const QString &trackFilepath) override;
                void SetVolume(const float &volume) override;
                void StopTrack() override;

            private:
                void ClearAttributes();
                void CleanResources();
                void ReportError(const QString &errorMessage);
                void SetupDriver();

                float volume;
                int aoDriverID;
                bool playTrack;
                bool pauseTrack;
                bool setPosition;
                int streamID = -1;
                int64_t calculatedPosition;
                QString trackFilepath;
                QString trackFileFormat;
                QString defaultAudioDriverName;

                struct SwrContext* resampleContext = nullptr;
                AVFormatContext* formatContext = nullptr;
                AVCodecContext *codecContext = nullptr;
                AVStream *avStream = nullptr;
                AVCodec *avCodec = nullptr;
                ao_device *aoDevice = nullptr;
                ao_info* aoDriverInfo = nullptr;
                AVFrame* avFrame = nullptr;
                ao_sample_format aoFormat;
                AVPacket *avPacket;
                uint8_t *avBuffer;
        };
    }
}

#endif // PLAYER_REPOSITORY_HPP
