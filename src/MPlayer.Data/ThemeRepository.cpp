// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "ThemeRepository.hpp"

MPlayer::Data::ThemeRepository::ThemeRepository()
{
    this->applicationSettingsProvider = std::shared_ptr<IApplicationSettingsProvider>(new ApplicationSettingsProvider());
}

QVector<ThemeControl> MPlayer::Data::ThemeRepository::GetThemeControlsForForm(const QString &formName)
{
    QVector<ThemeControl> themeControls = QVector<ThemeControl>();
    QFile themeFile(this->applicationSettingsProvider->GetProgramThemePath());

    if(themeFile.open(QIODevice::ReadOnly)) // Open the xml file with read only permissions
    {
        QXmlStreamReader reader(&themeFile);

        QXmlStreamReader::TokenType token;
        QXmlStreamAttributes formAttribute;
        QXmlStreamAttributes controlAttribute;
        QString defaultThemeName = this->applicationSettingsProvider->GetDefaultThemeName();

        while(!reader.atEnd()) // Read until the end
        {
            token = reader.readNext();

            if(token == QXmlStreamReader::StartElement
                    && reader.name() == "ThemeName"
                    && reader.readElementText() == defaultThemeName)
            {
                while(!reader.atEnd()) // Read until the end
                {
                    reader.readNextStartElement();

                    formAttribute = reader.attributes();

                    if(formAttribute.hasAttribute("Name") && formAttribute.value("Name") == formName)
                    {
                        reader.readNextStartElement();

                        while(!reader.atEnd()) // Read until the end
                        {
                            controlAttribute = reader.attributes();

                            if(controlAttribute.hasAttribute("Name"))
                            {
                                QString controlName = controlAttribute.value("Name").toString();
                                QString controlThemeContent = reader.readElementText();

                                if(!controlName.isEmpty() && !controlThemeContent.isEmpty())
                                {
                                    themeControls.append(ThemeControl(controlName, controlThemeContent));
                                }
                            }

                            reader.readNextStartElement();

                            if(reader.name() == "Form" && reader.isEndElement())
                            {
                                break;
                            }
                        }
                    }

                    if(reader.name() == "Theme" && reader.isEndElement())
                    {
                        break;
                    }
                }
            }
        }

        themeFile.close(); // Close the reader
    }

    return themeControls;
}
