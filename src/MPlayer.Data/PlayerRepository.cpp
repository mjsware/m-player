// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "PlayerRepository.hpp"

MPlayer::Data::PlayerRepository::PlayerRepository(const float &volume, const QString &defaultAudioDriverName)
{
    this->playTrack = false;
    this->pauseTrack = false;
    this->defaultAudioDriverName = defaultAudioDriverName;

    SetupDriver();
    SetVolume(volume);
}

void MPlayer::Data::PlayerRepository::SetupDriver()
{
    ao_initialize();

    this->aoDriverID = ao_driver_id(this->defaultAudioDriverName.toLatin1());
    this->aoDriverInfo = ao_driver_info(this->aoDriverID);

    memset(&this->aoFormat, 0, sizeof(this->aoFormat));

    this->aoFormat.bits = 16;
    this->aoFormat.channels = 2; // Stereo
    this->aoFormat.rate = 44000.0; // Set sample frequency to 44 Hz
    this->aoFormat.byte_format = this->aoDriverInfo->preferred_byte_format;
    this->aoFormat.matrix = nullptr;
    this->aoDevice = ao_open_live(this->aoDriverID, &this->aoFormat, nullptr);
    this->avBuffer = new uint8_t; // Create new audio data buffer
    this->setPosition = false;
    this->calculatedPosition = 0;
}

MPlayer::Data::PlayerRepository::~PlayerRepository()
{
    StopTrack();

    if (this->aoDevice)
    {
        ao_close(this->aoDevice);
        ao_shutdown();
        this->aoDevice = nullptr;
    }
}

bool MPlayer::Data::PlayerRepository::IsPlaying()
{
    return this->playTrack;
}

bool MPlayer::Data::PlayerRepository::IsPaused()
{
    return this->pauseTrack;
}

bool MPlayer::Data::PlayerRepository::IsStopped()
{
    return !this->playTrack && !this->pauseTrack;
}

QString MPlayer::Data::PlayerRepository::GetTrackFilepath()
{
    return this->trackFilepath;
}

void MPlayer::Data::PlayerRepository::PlayTrack()
{
    this->playTrack = true;
    this->pauseTrack = false;
}

void MPlayer::Data::PlayerRepository::PauseTrack()
{
    this->playTrack = true;
    this->pauseTrack = true;
}

void MPlayer::Data::PlayerRepository::StopTrack()
{
    this->setPosition = false;
    this->playTrack = false;
    this->pauseTrack = false;
    this->trackFilepath = "";
}

void MPlayer::Data::PlayerRepository::SetTrackFilepath(const QString &trackFilePath)
{
    this->trackFilepath = trackFilePath;
    this->trackFileFormat = trackFilePath.mid(trackFilePath.length() - 3, trackFilePath.length());
}

void MPlayer::Data::PlayerRepository::SetVolume(const float &volume)
{
    this->volume = volume / 100;
}

void MPlayer::Data::PlayerRepository::SetPosition(const int64_t &durationPos)
{
    if(this->playTrack || this->pauseTrack)
    {
        this->setPosition = true;
        this->calculatedPosition = durationPos * AV_TIME_BASE;
    }
}

int64_t MPlayer::Data::PlayerRepository::GetTotalDuration()
{
    if((this->playTrack || this->pauseTrack) && this->formatContext)
    {
        return this->formatContext->duration / AV_TIME_BASE;
    }
    return 0;
}

int64_t MPlayer::Data::PlayerRepository::GetCurrentDuration()
{
    if((this->playTrack || this->pauseTrack) && this->avStream && this->avFrame)
    {
        return static_cast<int64_t>(this->avFrame->pts * av_q2d(this->avStream->time_base)); // Convert AVRational timebase to a double and multiply by received timestamp
    }
    return 0;
}

QString MPlayer::Data::PlayerRepository::GetFileFormat()
{
    return this->trackFileFormat;
}

void MPlayer::Data::PlayerRepository::ReportError(const QString &errorMessage)
{
    qDebug() << errorMessage;
    StopTrack();
}

void MPlayer::Data::PlayerRepository::ClearAttributes()
{
    if(this->avBuffer)
    {
        memset(this->avBuffer, 0, sizeof(uint8_t));
        delete this->avBuffer;
        this->avBuffer = new uint8_t; // Create new audio data buffer
    }

    if(this->avPacket)
    {
        av_packet_unref(this->avPacket);
    }
}

void MPlayer::Data::PlayerRepository::CleanResources()
{
    if(this->avPacket)
    {
        av_packet_unref(this->avPacket);
        memset(this->avPacket, 0, sizeof(*this->avPacket));
        delete this->avPacket;
        this->avPacket = nullptr;
    }

    if(this->avBuffer)
    {
        memset(this->avBuffer, 0, sizeof(*this->avBuffer));
        delete this->avBuffer;
        this->avBuffer = nullptr;
    }

    if(this->avFrame)
    {
        av_freep(this->avFrame); // Free memory, set to NULL
    }

    if(this->avCodec)
    {
        this->avCodec = nullptr;
    }

    if(this->avStream)
    {
        memset(this->avStream, 0, sizeof(*this->avStream));
        this->avStream = nullptr; // Free memory, set to NULL
    }

    if (this->resampleContext)
    {
        swr_close(this->resampleContext);
        swr_free(&this->resampleContext);
        this->resampleContext = nullptr;
    }

    if (this->codecContext)
    {
        if(this->codecContext->codec)
        {
            avcodec_close(this->codecContext);
        }

        avcodec_free_context(&this->codecContext);
        this->codecContext = nullptr;
    }

    if (this->formatContext)
    {
        avformat_close_input(&this->formatContext);
        avformat_free_context(this->formatContext);
        this->formatContext = nullptr;
    }
}

void MPlayer::Data::PlayerRepository::run()
{
    this->formatContext = avformat_alloc_context();

    if(avformat_open_input(&this->formatContext, this->trackFilepath.toStdString().c_str(), nullptr, nullptr) < 0)
    {
        ReportError("Could not open media file.");
        return;
    }

    if(avformat_find_stream_info(this->formatContext, nullptr) < 0)
    {
        ReportError("Could not find the required file information.");
        return;
    }

    //av_dump_format(formatContext, 0, this->filePath.toStdString().c_str(),false);

    for(unsigned int i = 0; i < this->formatContext->nb_streams; i++)
    {
        if(this->formatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) // Find stream audio codec of audio file
        {
            this->streamID = static_cast<int>(i);
            break;
        }
    }

    if(this->streamID == -1)
    {
        ReportError("Could not find an audio stream.");
        return;
    }

    this->avCodec = avcodec_find_decoder(this->formatContext->streams[this->streamID]->codecpar->codec_id);
    this->codecContext = avcodec_alloc_context3(this->avCodec);
    this->avStream = this->formatContext->streams[this->streamID];

    avcodec_parameters_to_context(this->codecContext, this->formatContext->streams[this->streamID]->codecpar);

    if(this->avCodec == nullptr || this->codecContext == nullptr)
    {
        ReportError("Cannot find codec or decoder!");
        return;
    }

    if(avcodec_open2(this->codecContext, this->avCodec, nullptr) < 0)
    {
        ReportError("Codec cannot be found.");
        return;
    }

    if (!(this->resampleContext = swr_alloc())) // Allocate resampler context
    {
        ReportError("Could not allocate resample context.");
        return;
    }

    // Set up resample context
    av_opt_set_int(this->resampleContext, "in_channel_layout", av_get_default_channel_layout(this->codecContext->channels), 0); // File channels
    av_opt_set_int(this->resampleContext, "out_channel_layout", av_get_default_channel_layout(this->aoFormat.channels), 0); // Device channels
    av_opt_set_int(this->resampleContext, "in_sample_rate", this->codecContext->sample_rate, 0); // File sample rate
    av_opt_set_int(this->resampleContext, "out_sample_rate", this->aoFormat.rate, 0);  // Device sample rate
    av_opt_set_int(this->resampleContext, "in_sample_fmt", this->codecContext->sample_fmt, 0); // File bit-depth
    av_opt_set_int(this->resampleContext, "out_sample_fmt", AV_SAMPLE_FMT_S16, 0);

    if (swr_init(this->resampleContext) < 0)
    {
        ReportError("Could not open resample context.");
        return;
    }

    avformat_seek_file(this->formatContext, 0, 0, 0, 0, 0); //  Set to start of audio track

    if(!this->avBuffer)
    {
        this->avBuffer = new uint8_t; // Initalise new audio buffer
    }

    int avBufferSize;
    int64_t avResampleFormat; // Bit depth
    int avResampleCount = 0; // Sample count that will be played after resampling
    int delayedSamplesCount = 0; // Delayed sample count after playing

    av_opt_get_int(this->resampleContext, "out_sample_fmt", 0, &avResampleFormat); // Get output sample format

    this->avPacket = new AVPacket;
    this->avFrame = av_frame_alloc();

    av_init_packet(this->avPacket);

    int audioFrameStatus = -1;
    int readFrame = -1;
    int32_t pcmValue = 0;
    int pcmBufferSize = 0;
    int16_t* pcmData = nullptr;

    while(this->playTrack)
    {
        if(this->playTrack && !this->pauseTrack)
        {
            av_init_packet(this->avPacket);

            // Read each audio frame until there aren't any left
            while((readFrame = av_read_frame(this->formatContext, this->avPacket)) >= 0)
            {
                if(this->setPosition && this->formatContext)
                {
                    av_seek_frame(this->formatContext, -1, this->calculatedPosition, AVSEEK_FLAG_ANY); // Set position of audio track
                    this->setPosition = false;
                }

                audioFrameStatus = avcodec_send_packet(this->codecContext, this->avPacket);

                if (audioFrameStatus == AVERROR(EAGAIN) || audioFrameStatus == AVERROR_EOF)
                {
                    break;
                }

                if (this->avPacket->stream_index == this->streamID && audioFrameStatus == 0)
                {
                    audioFrameStatus = avcodec_receive_frame(this->codecContext, this->avFrame);

                    if (audioFrameStatus == AVERROR(EAGAIN) || audioFrameStatus == AVERROR_EOF) // Error reading audio frame or end of file reached
                    {
                        break;
                    }

                    if (audioFrameStatus == 0)
                    {
                        avResampleCount = swr_get_out_samples(this->resampleContext,
                                                              this->avFrame->nb_samples); // Get number of samples required after resampling
                        av_samples_alloc(&this->avBuffer,
                                         &avBufferSize,
                                         this->aoFormat.channels,
                                         avResampleCount,
                                         static_cast<AVSampleFormat>(avResampleFormat),
                                         0); // Allocate the resample output buffer

                        // Resample the audio data and store it in output buffer
                        avResampleCount = swr_convert(this->resampleContext,
                                                      &this->avBuffer,
                                                      avBufferSize,
                                                      const_cast<const uint8_t**>(this->avFrame->extended_data),
                                                      this->avFrame->nb_samples);

                        // Set volume for resample data
                        pcmData = reinterpret_cast<int16_t*>(this->avBuffer);
                        pcmBufferSize = avBufferSize / 2;

                        for (int i = 0; i < pcmBufferSize; i++) // 16 bit, divide by 2 (PCM - 8 bit data)
                        {
                            pcmValue = static_cast<int32_t>(pcmData[i] * this->volume);
                            pcmData[i] = static_cast<int16_t>(pcmValue);
                        }

                        ao_play(this->aoDevice, reinterpret_cast<char*>(this->avBuffer), static_cast<uint_32>(avResampleCount * 4)); // Use ao to play resample buffer
                        ClearAttributes();
                    }
                }

                if((!this->playTrack && !this->pauseTrack) || (this->playTrack && this->pauseTrack))
                {
                    break;
                }
            }
        }

        ClearAttributes();

        if(this->playTrack || this->pauseTrack)
        {
            sleep(1);
        }

        if(readFrame < 0)
        {
            break;
        }
    }

    // Clean delayed samples
    delayedSamplesCount = static_cast<int>(swr_get_delay(this->resampleContext, static_cast<int64_t>(this->codecContext->sample_rate)));

    while (delayedSamplesCount > 0)
    {
        avResampleCount = swr_get_out_samples(this->resampleContext, delayedSamplesCount);
        av_samples_alloc(&this->avBuffer, &avBufferSize, this->aoFormat.channels, delayedSamplesCount, static_cast<AVSampleFormat>(avResampleFormat), 0);
        delayedSamplesCount = swr_convert(this->resampleContext, &this->avBuffer, avResampleCount, nullptr, 0); // Concert each delayed sample to NULL
        ClearAttributes();
    }

    StopTrack();

    // Clean up player resources
    ClearAttributes();
    CleanResources();
}
