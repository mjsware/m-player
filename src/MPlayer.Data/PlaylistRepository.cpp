// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "PlaylistRepository.hpp"

QVector <PlayerTrack> MPlayer::Data::PlaylistRepository::GetPlaylistTracks(const QStringList &fileList)
{
    tm *currentTime = nullptr;
    char *tagContent = nullptr;
    TagLib_Tag *tags = nullptr;
    time_t parsedSeconds;
    TagLib_File* mediaFile = nullptr;
    QString currentDuration, currentShortDuration;
    const TagLib_AudioProperties *properties = nullptr;
    QVector <PlayerTrack> playlistTracks;
    std::unique_ptr<QFile> musicFile;

    taglib_set_strings_unicode(1);

    for(int i = 0; i < fileList.length(); i++)
    {
        mediaFile = taglib_file_new(fileList[i].toStdString().c_str());
        musicFile = std::unique_ptr<QFile>(new QFile(fileList[i]));

        if(mediaFile != nullptr && musicFile != nullptr)
        {
            tags = taglib_file_tag(mediaFile);
            properties = taglib_file_audioproperties(mediaFile);

            if(tags != nullptr)
            {
                PlayerTrack currentTrack;
                currentTrack.SetTrackNumber(taglib_tag_track(tags));

                tagContent = taglib_tag_title(tags);
                currentTrack.SetTrackTitle(strlen(tagContent) > 0 ? tagContent : "Unknown");

                tagContent = taglib_tag_artist(tags);
                currentTrack.SetTrackArtist(strlen(tagContent) > 0 ? tagContent : "Unknown");

                tagContent = taglib_tag_album(tags);
                currentTrack.SetTrackAlbum(strlen(tagContent) > 0 ? tagContent : "Unknown");

                tagContent = taglib_tag_genre(tags);
                currentTrack.SetTrackGenre(strlen(tagContent) > 0 ? tagContent : "Unknown");

                currentTrack.SetTrackYear(taglib_tag_year(tags));
                currentTrack.SetTrackFilepath(fileList[i]);
                currentTrack.SetTrackFilesize(musicFile->size());

                if(properties != nullptr)
                {
                    parsedSeconds = taglib_audioproperties_length(properties);
                    currentTime = gmtime(&parsedSeconds); // Convert to broken down time

                    currentDuration.sprintf("%02i:%02i:%02i", currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec);

                    if(currentTime->tm_hour > 0)
                    {
                        currentShortDuration.sprintf("%02i:%02i:%02i", currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec);
                    }
                    else
                    {
                        currentShortDuration.sprintf("%02i:%02i", currentTime->tm_min, currentTime->tm_sec);
                    }

                    currentTrack.SetTrackDuration(currentDuration);
                    currentTrack.SetTrackShortDuration(currentShortDuration);
                    currentTrack.SetTrackBitrate(taglib_audioproperties_bitrate(properties));
                    currentTrack.SetTrackDuration(currentDuration);

                    playlistTracks.append(currentTrack);
                }
            }

            musicFile->close();
        }
        taglib_tag_free_strings();
        taglib_file_free(mediaFile);
    }
    return playlistTracks;
}

void MPlayer::Data::PlaylistRepository::SavePlaylistTrack(PlayerTrack &playlistTrack)
{
    TagLib_File* mediaFile = taglib_file_new(playlistTrack.GetTrackFilepath().toStdString().c_str());
    TagLib_Tag *tags = taglib_file_tag(mediaFile);

    if(tags != nullptr)
    {
        taglib_tag_set_track(tags, playlistTrack.GetTrackNumber());
        taglib_tag_set_artist(tags, playlistTrack.GetTrackArtist().toLatin1());
        taglib_tag_set_album(tags, playlistTrack.GetTrackAlbum().toLatin1());
        taglib_tag_set_title(tags, playlistTrack.GetTrackTitle().toLatin1());
        taglib_tag_set_genre(tags, playlistTrack.GetTrackGenre().toLatin1());
        taglib_tag_set_year(tags, playlistTrack.GetTrackYear());
        taglib_file_save(mediaFile);
    }

    taglib_tag_free_strings();
    taglib_file_free(mediaFile);
}

void MPlayer::Data::PlaylistRepository::SavePlaylist(const QString &playlistPath, const QStringList &playlistFilePaths)
{
    QFile existingPlaylistFile(playlistPath);

    if (existingPlaylistFile.exists())
    {
        existingPlaylistFile.remove();
    }

    existingPlaylistFile.close();
    QFile playlistFile(playlistPath);

    if(playlistFile.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter writer(&playlistFile);
        writer.setAutoFormatting(true);
        writer.writeStartDocument();
        writer.writeStartElement("Playlists");
        writer.writeStartElement("Playlist");

        for(int i = 0; i < playlistFilePaths.count(); i++)
        {
            if(playlistFilePaths.at(i).length() > 0)
            {
                writer.writeStartElement("Track");
                writer.writeTextElement("TrackFile", playlistFilePaths[i]);
                writer.writeEndElement();
            }
        }

        writer.writeEndElement();
        writer.writeEndDocument();
        playlistFile.flush();
        playlistFile.close();
    }
}

QStringList MPlayer::Data::PlaylistRepository::GetFilelistOfPlaylistDirectory(const QString &playlistPath)
{
    QStringList foundPlaylistFiles;
    QFile existingPlaylistFile(playlistPath);

    if (existingPlaylistFile.exists())
    {
        if(existingPlaylistFile.open(QIODevice::ReadOnly)) // Open the xml file with read only permissions
        {
            QXmlStreamReader reader(&existingPlaylistFile);
            QXmlStreamReader::TokenType token;

            while(!reader.atEnd()) // Read until the end
            {
                token = reader.readNext();

                if(reader.name() == "TrackFile" && token == QXmlStreamReader::StartElement)
                {
                    foundPlaylistFiles.append(reader.readElementText());
                }
            }
            existingPlaylistFile.close(); // Close the reader

            if (reader.hasError())
            {
                qDebug() << reader.errorString();
                qDebug() << "at line " << reader.lineNumber() << ", column " << reader.columnNumber();
                qDebug() << "Error type " << reader.error();
            }
        }
    }
    return foundPlaylistFiles;
}
