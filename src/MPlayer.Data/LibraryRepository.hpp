// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef LIBRARY_REPOSITORY_HPP
#define LIBRARY_REPOSITORY_HPP

#include "Interfaces/ILibraryRepository.hpp"

namespace MPlayer
{
    namespace Data
    {
        class LibraryRepository : public ILibraryRepository
        {
            public:
                LibraryRepository();
                void AddAllLibraryTracks(QVector<PlayerTrack> selectedLibraryTracks) override;
                void AddSelectedLibraryTracks(QVector<PlayerTrack> selectedLibraryTracks) override;
                QVector<PlayerTrack> GetGeneratedLibraryTracks() override;
                double GetLibraryPercentageGenerated() override;
                QVector<PlayerTrack> GetLibraryTracksByQuery(const QString &query) override;
                bool IsGenerated() override;
                void RemoveLibraryTrack(const QString &trackFilepath) override;
                void run() override;
                void UpdateLibraryTrack(PlayerTrack &selectedLibraryTrack) override;

            private:
                QStringList GetLibraryTrackFilepathsForDirectory(const QString &currentLibraryAlbumPath);
                void UpdateLibraryTrackAttribute(QDomDocument libraryXMLDoc, const int &index, const QString &trackAttributeName, const QString &trackAttributeValue);

                std::shared_ptr<IApplicationSettingsProvider> applicationSettingsProvider;
                QVector<PlayerTrack> generatedLibraryTracks;
                double percentageGenerated;
                bool isGenerated;
        };
    }
}

#endif // LIBRARY_REPOSITORY_HPP
