// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#include "SettingsRepository.hpp"

bool MPlayer::Data::SettingsRepository::CreateDirectories(ApplicationSettings &applicationSettings)
{
    QDir topDirectory(applicationSettings.GetTopDirectoryPath());

    if(!topDirectory.exists())
    {
        topDirectory.mkdir(applicationSettings.GetTopDirectoryPath());
    }

    QDir settingsDirectory(applicationSettings.GetSettingsDirectoryPath());

    if(!settingsDirectory.exists())
    {
        settingsDirectory.mkdir(applicationSettings.GetSettingsDirectoryPath());
    }

    QDir playlistsDirectory(applicationSettings.GetPlaylistsDirectory());

    if(!playlistsDirectory.exists())
    {
        playlistsDirectory.mkdir(applicationSettings.GetPlaylistsDirectory());
    }

    QFile settingsFile(applicationSettings.GetSettingsFilePath());

    return settingsFile.exists() && settingsFile.size() != 0;
}

QStringList MPlayer::Data::SettingsRepository::GetProgramThemes(const QString &themePath)
{
    QFile themeFile(themePath);
    QStringList themeList;

    themeList.append("None");

    if(themeFile.open(QIODevice::ReadOnly)) // Open the xml file with read only permissions
    {
        QXmlStreamReader reader(&themeFile);
        QXmlStreamReader::TokenType token;
        QString themeName;

        while(!reader.atEnd()) // Read until the end
        {
            token = reader.readNext();

            if(token == QXmlStreamReader::StartElement && reader.isStartElement())
            {
                if(reader.name() == "ThemeName")
                {
                    themeName = reader.readElementText();

                    if(!themeList.contains(themeName, Qt::CaseSensitive))
                    {
                        themeList.append(themeName);
                    }
                }
                reader.readNextStartElement();
            }
        }
        themeFile.close(); // Close the reader

        if (reader.hasError())
        {
            qDebug() << reader.errorString();
            qDebug() << "at line " << reader.lineNumber() << ", column " << reader.columnNumber();
            qDebug() << "Error type " << reader.error();
            return QStringList();
        }
    }

    return themeList;
}

bool MPlayer::Data::SettingsRepository::SaveApplicationSettings(ApplicationSettings &applicationSettings)
{
    QFile settingsFile(applicationSettings.GetSettingsFilePath());

    if (settingsFile.exists())
    {
        settingsFile.remove();
    }

    if(settingsFile.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter writer(&settingsFile);

        writer.setAutoFormatting(true);
        writer.writeStartDocument();
        writer.writeStartElement("Settings");
        writer.writeTextElement("HideOnStartup", applicationSettings.GetHideOnStartup() ? "1" : "0");
        writer.writeTextElement("AutoLoadPlaylist", applicationSettings.GetAutoLoadPlaylist() ? "1" : "0");
        writer.writeTextElement("PlaylistPath", applicationSettings.GetAutoLoadPlaylist() ? applicationSettings.GetPlaylistPath() : "");
        writer.writeTextElement("DefaultVolume", QString::number(static_cast<double>(applicationSettings.GetDefaultVolume())));
        writer.writeTextElement("ThemePath", applicationSettings.GetProgramThemePath());
        writer.writeTextElement("DefaultThemeName", applicationSettings.GetDefaultThemeName());
        writer.writeTextElement("DefaultAudioDriverName", applicationSettings.GetDefaultAudioDriverName());
        writer.writeTextElement("MusicLibraryPath", applicationSettings.GetLibraryMusicPath());
        writer.writeEndElement();
        writer.writeEndDocument();

        settingsFile.flush();
        settingsFile.close();

        if (writer.hasError())
        {
            return false;
        }
    }
    return true;
}

ApplicationSettings MPlayer::Data::SettingsRepository::GetApplicationSettings(const QString &settingsPath)
{
    QFile settingsFile(settingsPath);
    ApplicationSettings applicationSettings = ApplicationSettings();

    if(settingsFile.open(QIODevice::ReadOnly)) // Open the xml file with read only permissions
    {
        QXmlStreamReader reader(&settingsFile);
        QXmlStreamReader::TokenType token;

        while(!reader.atEnd()) // Read until the end
        {
            token = reader.readNext();

            if(token == QXmlStreamReader::StartElement)
            {
                if(reader.name() == "HideOnStartup")
                {
                    applicationSettings.SetHideOnStartup(reader.readElementText() == "1" ? true : false);
                }
                else if(reader.name() == "AutoLoadPlaylist")
                {
                    applicationSettings.SetAutoLoadPlaylist(reader.readElementText() == "1" ? true : false);
                }
                else if(reader.name() == "PlaylistPath")
                {
                    applicationSettings.SetPlaylistPath(reader.readElementText());
                }
                else if(reader.name() == "DefaultVolume")
                {
                    applicationSettings.SetDefaultVolume(reader.readElementText().toFloat());
                }
                else if(reader.name() == "ThemePath")
                {
                    applicationSettings.SetProgramThemePath(reader.readElementText());
                }
                else if(reader.name() == "DefaultThemeName")
                {
                    applicationSettings.SetDefaultTheme(reader.readElementText());
                }
                else if(reader.name() == "DefaultAudioDriverName")
                {
                    applicationSettings.SetDefaultAudioDriverName(reader.readElementText());
                }
                else if(reader.name() == "MusicLibraryPath")
                {
                    applicationSettings.SetLibraryMusicPath(reader.readElementText());
                }
            }
        }
        settingsFile.close(); // Close the reader

        if (reader.hasError())
        {
            return ApplicationSettings();
        }
    }
    return applicationSettings;
}
