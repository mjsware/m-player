// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef IPLAYER_REPOSITORY_HPP
#define IPLAYER_REPOSITORY_HPP

#include <QThread>
#include <QString>
#include <QDebug>

extern "C"
{
    #include <libavformat/avformat.h> // libavformat-dev, libavformat58
    #include <libavcodec/avcodec.h> // libavcodec-dev, libavcodec58
    #include <libavutil/avstring.h> // libavutil-dev, libavutil56
    #include <libswresample/swresample.h> //libswresample-dev, libswresample4
    #include <libavutil/opt.h>
    #include <libavfilter/avfilter.h> // libavfilter-dev, libavfilter7
    #include <ao/ao.h> // libao-dev, libao4
}

namespace MPlayer
{
    namespace Data
    {
        class IPlayerRepository : public QThread
        {
            public:
                virtual ~IPlayerRepository() = 0;
                virtual int64_t GetCurrentDuration() = 0;
                virtual QString GetFileFormat() = 0;
                virtual int64_t GetTotalDuration() = 0;
                virtual QString GetTrackFilepath() = 0;
                virtual bool IsPaused() = 0;
                virtual bool IsPlaying() = 0;
                virtual bool IsStopped() = 0;
                virtual void run() = 0;
                virtual void PauseTrack() = 0;
                virtual void PlayTrack() = 0;
                virtual void SetPosition(const int64_t &durationPos) = 0;
                virtual void SetTrackFilepath(const QString &trackFilepath) = 0;
                virtual void SetVolume(const float &volume) = 0;
                virtual void StopTrack() = 0;
        };
    }
}

#endif // IPLAYER_REPOSITORY_HPP
