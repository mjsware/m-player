// This application is under GNU GPLv3. Please read the COPYING.txt file for further terms and conditions of the license.
// Copyright © Matthew James
// "M++ Player" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// "M++ Player" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with "M++ Player". If not, see http://www.gnu.org/licenses/.

#ifndef ILIBRARY_REPOSITORY_HPP
#define ILIBRARY_REPOSITORY_HPP

#include <QThread>
#include <QDebug>
#include <QDirIterator>
#include <QDomDocument>
#include <taglib/tag_c.h> // libtagc0, libtagc0-dev

#include "MPlayer.Domain/Settings/ApplicationSettingsProvider.hpp"
#include "MPlayer.Entities/Track/PlayerTrack.hpp"

using namespace MPlayer::Domain::Settings;
using namespace MPlayer::Entities::Track;

namespace MPlayer
{
    namespace Data
    {
        class ILibraryRepository : public QThread
        {
           public:
                virtual ~ILibraryRepository() = 0;
                virtual void AddAllLibraryTracks(QVector<PlayerTrack> selectedLibraryTracks) = 0;
                virtual void AddSelectedLibraryTracks(QVector<PlayerTrack> selectedLibraryTracks) = 0;
                virtual QVector<PlayerTrack> GetGeneratedLibraryTracks() = 0;
                virtual double GetLibraryPercentageGenerated() = 0;
                virtual QVector<PlayerTrack> GetLibraryTracksByQuery(const QString &query) = 0;
                virtual bool IsGenerated() = 0;
                virtual void RemoveLibraryTrack(const QString &trackFilepath) = 0;
                virtual void run() = 0;
                virtual void UpdateLibraryTrack(PlayerTrack &selectedLibraryTrack) = 0;
        };
    }
}

#endif // ILIBRARY_REPOSITORY_HPP
