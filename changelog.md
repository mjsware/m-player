m++player (0.4.7-1) stable; urgency=low

  * Fix a bug where skipping tracks too quickly caused the audio player to stop playing the current track [Bug Fix]
  
  * Fix a bug where library generation wouldn't find the top level folder tracks [Bug Fix]
  
  * Fix a bug where playlist track calculation was shown on start-up on the library tab [Bug Fix]

  * Fix a bug where double loading of tracks occurred on shuffling and the Select Playing option in library mode [Bug Fix]
  
  * Fix a bug which caused a sorting issue upon switching between the options of Shuffle All Library Tracks to Shuffle Tracks [Bug Fix]
  
  * Fix a bug which caused the Select Playing option to select the wrong album if played from the "All Albums" selection [Bug Fix]
  
  * Update Themes.xml file to improve theming on the RPI [Feature]
  
  * Update audio player to store the library track list (like playlist), so it's not dependent on an index [Feature]
  
  * Update unit tests project to add/update unit tests (Qt Test)
  
  * Add debian package support for RPI

 -- Matthew James <admin@mjsware.co.uk>  Sun, 14 Jun 2020 10:16:11 +0100
